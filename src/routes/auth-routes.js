import React from 'react/lib/React';
import { Route } from 'react-router';

import MuiThemeWrapper from 'helpers/MuiThemeWrapper';
import Login from 'admin/containers/Login';

export default () => {
  return (
    <Route path="/" component={MuiThemeWrapper}>
      <Route path="login" component={Login} />
    </Route>
  );
};
