import React from 'react';
import {IndexRoute, Route } from 'react-router';
import MuiThemeWrapper from 'helpers/MuiThemeWrapper';
import ResponsiveWrapper from 'admin/components/ResponsiveWrapper';

import { editOff, editOn, editorContextOn, editorContextOff } from 'admin/redux/modules/editor';
import { getLanguageRouteData } from 'routesSet';

import {
    AnsaApp,
    HomePage,
    Ambulance,
    OneDaySurgery,
    NewsPage,
    PlasticSurgery,
    NotFound
  } from 'public/containers';
import { __test__, Media, PageBuilder, Pages, PagesVersions, Root, Setting } from 'admin/containers';


export default (store) => {
  const languageRouteData = getLanguageRouteData();
  return (
      <Route path="/" component={MuiThemeWrapper}>
        { /* Home (main) route */ }
        <Route path="admin">
          <IndexRoute component={Root} />
          <Route path="pages" component={PagesVersions}/>
          <Route path="pages/:id">
            <IndexRoute component={Pages} />
            <Route path="pageBuilder" component={PageBuilder} onEnter={() => { store.dispatch(editOn()); store.dispatch(editorContextOn()); }} onLeave={() => { store.dispatch(editOff()); store.dispatch(editorContextOff); }}>
              <Route component={ResponsiveWrapper}>
                <Route component={AnsaApp}>
                  <Route path="homePage" component={HomePage}/>
                  <Route path={languageRouteData.ambulance.cs} component={Ambulance}/>
                  <Route path={languageRouteData.oneDaySurgery.cs} component={OneDaySurgery}/>
                  <Route path={languageRouteData.plasticSurgery.cs} component={PlasticSurgery}/>
                  <Route path={languageRouteData.news.cs} component={NewsPage}/>
                </Route>
                <Route path="responsive-view" component={__test__.ResponsiveViewContainer} />
              </Route>
            </Route>
          </Route>
          <Route path="media" component={Media} />
          <Route path="setting" component={Setting} />
        </Route>
        { /* Catch all route */ }
        <Route path="*" component={NotFound} status={404} />
      </Route>
  );
};
