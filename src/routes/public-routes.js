import React from 'react/lib/React';
import {IndexRoute, Route } from 'react-router';
// import { isLoaded as isAuthLoaded, load as loadAuth } from 'redux/modules/auth';
import { setCS, setEN, setDE, setPL } from 'admin/redux/modules/language';
// import { loadLanguageData } from 'redux/modules/pageBuilder';
import { getLanguageRouteData } from 'routesSet';
import MuiThemeWrapper from 'helpers/MuiThemeWrapper';

import {
    AnsaApp,
    HomePage,
    Ambulance,
    OneDaySurgery,
    NewsPage,
    PlasticSurgery,
    NotFound
  } from 'public/containers';

export default (store) => {
  const languageRouteData = getLanguageRouteData();
  return (
      <Route path="/" component={MuiThemeWrapper}>
        { /* Home (main) route */ }
        <Route component={AnsaApp}>
          <Route>
            <IndexRoute component={HomePage}/>
            <Route path={languageRouteData.ambulance.cs} component={Ambulance}/>
            <Route path={languageRouteData.oneDaySurgery.cs} component={OneDaySurgery}/>
            <Route path={languageRouteData.plasticSurgery.cs} component={PlasticSurgery}/>
            <Route path={languageRouteData.news.cs} component={NewsPage}/>
          </Route>
          <Route path="cs" onEnter={() => { store.dispatch(setCS()); }}>
            <Route>
              <IndexRoute component={HomePage}/>
              <Route path={languageRouteData.ambulance.cs} component={Ambulance}/>
              <Route path={languageRouteData.oneDaySurgery.cs} component={OneDaySurgery}/>
              <Route path={languageRouteData.plasticSurgery.cs} component={PlasticSurgery}/>
              <Route path={languageRouteData.news.cs} component={NewsPage}/>
            </Route>
          </Route>
          <Route path="en" onEnter={() => { store.dispatch(setEN()); }}>
            <Route>
              <IndexRoute component={HomePage}/>
              <Route path={languageRouteData.ambulance.en} component={Ambulance}/>
              <Route path={languageRouteData.oneDaySurgery.en} component={OneDaySurgery}/>
              <Route path={languageRouteData.plasticSurgery.en} component={PlasticSurgery}/>
              <Route path={languageRouteData.news.en} component={NewsPage}/>
            </Route>
          </Route>
          <Route path="de" onEnter={() => { store.dispatch(setDE()); }}>
            <Route>
              <IndexRoute component={HomePage}/>
              <Route path={languageRouteData.ambulance.de} component={Ambulance}/>
              <Route path={languageRouteData.oneDaySurgery.de} component={OneDaySurgery}/>
              <Route path={languageRouteData.plasticSurgery.de} component={PlasticSurgery}/>
              <Route path={languageRouteData.news.de} component={NewsPage}/>
            </Route>
          </Route>
          <Route path="pl" onEnter={() => { store.dispatch(setPL()); }}>
            <Route>
              <IndexRoute component={HomePage}/>
              <Route path={languageRouteData.ambulance.pl} component={Ambulance}/>
              <Route path={languageRouteData.oneDaySurgery.pl} component={OneDaySurgery}/>
              <Route path={languageRouteData.plasticSurgery.pl} component={PlasticSurgery}/>
              <Route path={languageRouteData.news.pl} component={NewsPage}/>
            </Route>
          </Route>
        </Route>
        { /* Catch all route */ }
        <Route path="*" component={NotFound} status={404} />
      </Route>
  );
};
