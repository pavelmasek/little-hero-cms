export function deepFind(obj, path) {
  const splitPath = path.split('.');
  let i = 0;
  while (i < splitPath.length) {
    if (Array.isArray(obj)) {
      const indexInArray = splitPath[i].split(/[\[\]]/)[1];
      obj = obj[indexInArray];
    } else {
      obj = obj[splitPath[i]];
    }
    i++;
  }
  return obj;
}

export function deepAssign(obj, path, value) {
  const keys = path.split('.');
  return goDeep(obj, keys, value);
}

function goDeep(obj, keys, value) {
  if (typeof obj !== "object") {
    return value;
  }
  let returnObj = {};
  const key = keys.shift();
  let returnValue = goDeep(obj[key], keys, value);
  if (Array.isArray(obj)) {
    returnObj = obj.slice();
    if (key)
      returnObj[key] = value;
    else
      returnObj = value;
  } else {
    returnObj[key] = returnValue;
  }
  return returnObj;
}
