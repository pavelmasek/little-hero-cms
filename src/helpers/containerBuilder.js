import React, { Component } from 'react';
import PropTypes from 'prop-types';



const componentObjectParser = (childrens) => {
  return childrens.map((comp, index) => {
    if (comp.content) {
      return comp.content;
    }
    return (
        <comp.component key={index} {...comp.props}>
          { comp.children && componentObjectParser(comp.children) || comp.content }
        </comp.component>
    );
  });
};

class Builder extends Component {
  static propTypes = {
    components: PropTypes.array
  }
  render() {
    return (
      <div>
        {this.props.components && componentObjectParser(this.props.components)}
      </div>
    );
  }
}

const containerBuilder = (components) => {
  return <Builder components={components} />;
};

export default containerBuilder;
