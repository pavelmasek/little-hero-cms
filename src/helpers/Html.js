import React, { Component } from 'react';
import PropTypes from 'prop-types';


import ReactDOM from 'react-dom/server';
import serialize from 'serialize-javascript';
import Helmet from 'react-helmet';

/**
 * Wrapper component containing HTML metadata and boilerplate tags.
 * Used in server-side code only to wrap the string output of the
 * rendered route component.
 *
 * The only thing this component doesn't (and can't) include is the
 * HTML doctype declaration, which is added to the rendered output
 * by the server.js file.
 */
export default class Html extends Component {
  static propTypes = {
    assets: PropTypes.object,
    component: PropTypes.node,
    store: PropTypes.object,
    templateType: PropTypes.string
  };

  render() {
    const {assets, component, store, templateType} = this.props;
    const content = component ? ReactDOM.renderToString(component) : '';
    const head = Helmet.rewind();
    const htmlAttrs = head.htmlAttributes.toComponent();
    return (
      <html {...htmlAttrs}>
        <head>
          {head.base.toComponent()}
          {head.title.toComponent()}
          {head.meta.toComponent()}
          {head.link.toComponent()}
          {head.script.toComponent()}

          <link rel="shortcut icon" href="/favicon.ico" />
          {
            templateType === 'admin' &&
            [
              <link key="treeview" rel="stylesheet" href="/assets/react-treeview.css" />,
              <link key="medium-editor" rel="stylesheet" href="/assets/medium/css/medium-editor.css" />,
              <link key="medium-theme" rel="stylesheet" href="/assets/medium/css/themes/default.css" />
            ]
          }
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_V1CVB-_bL1zL85LoutvBwFgRMEg4S4A" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          {/* styles (will be present only in production with webpack extract text plugin) */}
          {Object.keys(assets.styles).map((style, key) =>
            <link href={assets.styles[style]} key={key} media="screen"
                  rel="stylesheet" type="text/css" charSet="UTF-8"/>
          )}

          {/* (will be present only in development mode) */}
          {/* outputs a <style/> tag with all bootstrap styles + App.scss + it could be CurrentPage.scss. */}
          {/* can smoothen the initial style flash (flicker) on page load in development mode. */}
          {/* ideally one could also include here the style for the current page (Home.scss, About.scss, etc) */}
        </head>
        <body>
          <div id="content" dangerouslySetInnerHTML={{__html: content[0]}}/>
          <script dangerouslySetInnerHTML={{__html: `window.__data=${serialize(store.getState())};`}} charSet="UTF-8"/>
          <script src={assets.javascript[templateType]} charSet="UTF-8"/>
        </body>
      </html>
    );
  }
}
