export AnsaApp from './AnsaApp/AnsaApp';
export Ambulance from './Ambulance/Ambulance';
export OneDaySurgery from './OneDaySurgery/OneDaySurgery';
export PlasticSurgery from './PlasticSurgery/PlasticSurgery';
export HomePage from './HomePage/HomePage';
export NewsPage from './NewsPage/NewsPage';
export NotFound from './NotFound/NotFound';
