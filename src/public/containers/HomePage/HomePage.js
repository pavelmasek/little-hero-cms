import React, { Component } from 'react';
import PropTypes from 'prop-types';

import HeadWorkers, { PatrakDoctor, JanataDoctor, SchmoranzovaDoctor, ParkanovaDoctor } from 'public/components/HeadWorkers/HeadWorkers.js';
import BackgroundComponent from 'public/components/BackgroundComponent/BackgroundComponent.js';
import OfficeHours from 'public/components/OfficeHours/OfficeHours.js';
import { Link } from 'react-router';
import {connect} from 'react-redux';

import * as languageActions from 'admin/redux/modules/language';

import ContentItem from 'public/components/ContentItem/ContentItem';
import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import References from 'public/components/References/References';
import NewsLink from 'public/components/NewsLink/NewsLink';
import SEO from 'admin/components/SEO';
import CMSImage from 'admin/components/CMSImage';
import ResponsiveView, { RULES } from 'admin/components/ResponsiveView';

import { getLanguageRoute } from 'routesSet';
import { Aligner, HorizontalContainer, HorizontalItem, Center } from 'public/components/LayoutComponents/LayoutComponents';

const surgeryIcon = require('public/images/ikona_chirurgie.svg');
const ambulanceIcon = require('public/images/ikona_ambulance.svg');
const plasticIcon = require('public/images/ikona_plastika.svg');

const styles = require('public/styles/styles.scss');
const smallMenuStyles = require('public/components/SmallMenu/SmallMenu.scss');

// import CommonLangData from 'languageData/CommonLanguageData';
// import { getCommonLangData } from 'helpers/LanguageApi';

@connect(state => ({
  language: state.language.language
}), {...languageActions})

export default class HomePage extends Component {

  displayName = 'HomePage';

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    language: PropTypes.string,
    route: PropTypes.object
  };

  state = {
    selectedClientReference: 0
  };
  render() {
    const { language } = this.props;
    return (
      <div>
        <SEO dataKey="homepage"/>
        <ContentItem className={styles.contentItemHeader}>
          <BackgroundComponent className={styles.contentItemHeader}>
            <div style={{height: '100%'}}>
              <div style={{height: '25%'}}>
                <div></div>
              </div>
              <div style={{textAlign: 'center', height: '50%'}}>
                <Aligner verticalAlign="middle">
                  <Link to="/">
                    <ResponsiveView showOn={[RULES.SHOW_ON_DESKTOP]}>
                      <CMSImage dataKey="ansalogo" className={styles.ansaLogo} />
                    </ResponsiveView>
                    <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE]}>
                      <CMSImage dataKey="ansalogo" className={styles.ansaLogo_mobile} />
                    </ResponsiveView>
                    <ResponsiveView showOn={[RULES.SHOW_ON_TABLET]}>
                      <CMSImage dataKey="ansalogo" className={styles.ansaLogo_tablet} />
                    </ResponsiveView>
                  </Link>
                </Aligner>
              </div>
              <div style={{height: '25%'}}>
                <Aligner verticalAlign="middle">
                  <ResponsiveView showOn={[RULES.SHOW_ON_DESKTOP]}>
                      <div className={styles.headerText}><MultiLanguageContent dataKey="homepageHeader" /></div>
                    </ResponsiveView>
                    <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE]}>
                      <div className={[styles.headerText, styles.headerText_mobile].join(' ')}><MultiLanguageContent dataKey="homepageHeader" /></div>
                    </ResponsiveView>
                    <ResponsiveView showOn={[RULES.SHOW_ON_TABLET]}>
                      <div className={[styles.headerText, styles.headerText_tablet].join(' ')}><MultiLanguageContent dataKey="homepageHeader" /></div>
                    </ResponsiveView>
                </Aligner>
              </div>
            </div>
          </BackgroundComponent>
        </ContentItem>
        <ContentItem className={styles.contentitem + ' ' + styles.second}>
          <Center style={{height: '50vh'}}>
            <div className={styles.whiteColor} style={{'height': '80%'}}>
              <ResponsiveView showOn={[RULES.SHOW_ON_DESKTOP, RULES.SHOW_ON_TABLET_LANDSCAPE]}>
                <div className={styles.servicesWide} style={{padding: '1rem 0', width: '100%', height: '100%'}}>
                  <HorizontalContainer height="100%" width="auto" style={{marginLeft: 'auto', marginRight: 'auto'}}>
                    <HorizontalItem verticalAlign="middle" className={styles.services + ' ' + smallMenuStyles.menuItem}>
                      <Link to={'/' + language + '/' + getLanguageRoute('oneDaySurgery')[language]}>
                        <div>
                          <div className={styles.icon} style={{textAlign: 'center'}}>
                            <div className={smallMenuStyles.oneDaySurgery}></div>
                          </div>
                          <div className={styles.header} style={{textAlign: 'center'}}>
                            <MultiLanguageContent dataKey="oneDaySurgeryh2" />
                          </div>
                          <div className={styles.text + ' ' + styles.menuText} style={{textAlign: 'center'}}>
                            <MultiLanguageContent dataKey="homepageMenuOneDaySurgery" style={{textAlign: 'center'}} />
                          </div>
                          <div className={styles.moreButton} style={{textAlign: 'center'}}>
                            <div>
                              <br />
                              <div className={styles.button}><MultiLanguageContent dataKey="more" /></div>
                            </div>
                          </div>
                        </div>
                      </Link>
                    </HorizontalItem>
                    <HorizontalItem verticalAlign="middle" className={styles.services + ' ' + smallMenuStyles.menuItem}>
                      <Link to={'/' + language + '/' + getLanguageRoute('ambulance')[language]}>
                        <div>
                          <div className={styles.icon} style={{textAlign: 'center'}}>
                            <div className={smallMenuStyles.ambulance}></div>
                          </div>
                          <div className={styles.header} style={{textAlign: 'center'}}>
                            <MultiLanguageContent dataKey="ambulanceh2" />
                          </div>
                          <div className={styles.text + ' ' + styles.menuText} style={{textAlign: 'center'}}>
                            <MultiLanguageContent dataKey="homepageMenuAmbulance" style={{textAlign: 'center'}} />
                          </div>
                          <div className={styles.moreButton} style={{textAlign: 'center'}}>
                            <div>
                              <br />
                              <div className={styles.button}><MultiLanguageContent dataKey="more" /></div>
                            </div>
                          </div>
                        </div>
                      </Link>
                    </HorizontalItem>
                    <HorizontalItem verticalAlign="middle" className={styles.services + ' ' + smallMenuStyles.menuItem}>
                      <Link to={'/' + language + '/' + getLanguageRoute('plasticSurgery')[language]}>
                        <div>
                          <div className={styles.icon} style={{textAlign: 'center'}}>
                            <div className={smallMenuStyles.plasticSurgery}></div>
                          </div>
                          <div className={styles.header} style={{textAlign: 'center'}}>
                            <MultiLanguageContent dataKey="plasticSurgeryh2" />
                          </div>
                          <div className={styles.text + ' ' + styles.menuText} style={{textAlign: 'center'}}>
                            <MultiLanguageContent dataKey="homepageMenuPlasticSurgery" style={{textAlign: 'center'}} />
                          </div>
                          <div className={styles.moreButton} style={{textAlign: 'center'}}>
                            <div>
                              <br />
                              <div className={styles.button}><MultiLanguageContent dataKey="more" /></div>
                            </div>
                          </div>
                        </div>
                      </Link>
                    </HorizontalItem>
                  </HorizontalContainer>
                </div>
              </ResponsiveView>
              <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE, RULES.SHOW_ON_TABLET_PORTRAIT]}>
                <div className={styles.servicesMobile}>
                  <div style={{width: '80%', marginLeft: 'auto', marginRight: 'auto'}}>
                    <div>
                      <Link style={{color: 'inherit', textDecoration: 'none'}} to={'/' + language + '/' + getLanguageRoute('oneDaySurgery')[language]}>
                        <table>
                          <tbody>
                            <tr>
                              <td>
                                <img src={surgeryIcon}/>
                              </td>
                              <td>
                                <MultiLanguageContent dataKey="oneDaySurgeryspan" display="inline" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </Link>
                    </div>
                    <div>
                      <Link style={{color: 'inherit', textDecoration: 'none'}} to={'/' + language + '/' + getLanguageRoute('ambulance')[language]}>
                        <table>
                          <tbody>
                            <tr>
                              <td>
                                <img src={ambulanceIcon}/>
                              </td>
                              <td>
                                <MultiLanguageContent dataKey="ambulancespan" display="inline" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </Link>
                    </div>
                    <div>
                      <Link style={{color: 'inherit', textDecoration: 'none'}} to={'/' + language + '/' + getLanguageRoute('plasticSurgery')[language]}>
                        <table>
                          <tbody>
                            <tr>
                              <td>
                                <img src={plasticIcon}/>
                              </td>
                              <td>
                                <MultiLanguageContent dataKey="plasticSurgeryspan" display="inline" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </Link>
                    </div>
                  </div>
                </div>
              </ResponsiveView>
            </div>
          </Center>
        </ContentItem>
        <ContentItem alignItems="middle" className={styles.contentitem + ' ' + styles.third}>
          <NewsLink language={language} />
        </ContentItem>
        <ContentItem className={styles.contentitem + ' ' + styles.whiteColor + ' ' + styles.darkBlueBackground}>
          <OfficeHours />
        </ContentItem>
        <ContentItem alignItems="middle" className={styles.contentitem + ' ' + styles.darkBlueColor}>
          <HeadWorkers prefixDataKey="homepage">
            <PatrakDoctor />
            <JanataDoctor />
            <SchmoranzovaDoctor />
            <ParkanovaDoctor />
          </HeadWorkers>
        </ContentItem>
        <ContentItem style={{padding: '2em', backgroundColor: '#5CC2D0', color: '#004E63'}}>
          <References />
        </ContentItem>
      </div>
    );
  }
}
