import React, { Component } from 'react';

import ContentItem from 'public/components/ContentItem/ContentItem';
import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import HeadWorkers, { PatrakDoctor, JanataDoctor, ParkanovaDoctor } from 'public/components/HeadWorkers/HeadWorkers.js';
import SmallMenu from 'public/components/SmallMenu/SmallMenu.js';
import BackgroundComponent from 'public/components/BackgroundComponent/BackgroundComponent.js';
import { Link } from 'react-router';
import References from 'public/components/References/References';
import { Aligner } from 'public/components/LayoutComponents/LayoutComponents';
import SEO from 'admin/components/SEO';
import CMSImage from 'admin/components/CMSImage';

const styles = require('public/styles/styles.scss');

export default class OneDaySurgery extends Component {

  render() {
    return (
      <div>
        <SEO dataKey="onedaysurgerypage"/>
        <ContentItem percentHeight={50} flexGrow={1} className={styles.contentItemHeader}>
          <BackgroundComponent className={styles.contentItemHeader}>
            <div style={{height: '100%'}}>
              <div style={{height: '25%'}}>
                <div></div>
              </div>
              <div style={{textAlign: 'center', height: '50%'}}>
                <Aligner verticalAlign="middle">
                  <Link to="/">
                    <CMSImage dataKey="ansalogo" className={styles.ansaLogo} />
                  </Link>
                </Aligner>
              </div>
              <div style={{height: '25%'}}>
                <Aligner verticalAlign="middle">
                  <div className={styles.headerText}><MultiLanguageContent dataKey="headerOneDaySurgery" /></div>
                </Aligner>
              </div>
            </div>
          </BackgroundComponent>
        </ContentItem>
        <ContentItem alignItems="middle" style={{padding: '2em', backgroundColor: '#004E63'}}>
          <SmallMenu selected={0} />
        </ContentItem>
        <ContentItem className={styles.contentitem} alignItems="middle" style={{padding: '2em', backgroundColor: '#5CC2D0'}}>
          <div>
            <div className={styles.text} style={{ marginLeft: 'auto', marginRight: 'auto', maxWidth: '1024px'}}>
              <MultiLanguageContent dataKey="infoOneDaySurgery" />
              <MultiLanguageContent style={{fontSize: '1.2em'}} dataKey="operationInfoOneDaySurgery" />
            </div>
            <div style={{textAlign: 'center'}}>
              <br />
              <Link to="/" style={{color: 'inherit', textDecoration: 'none'}}>
                <div className={styles.buttonDarkerHover}>
                  <MultiLanguageContent dataKey="back" />
                </div>
              </Link>
            </div>
          </div>
        </ContentItem>
        <ContentItem className={styles.contentitem} style={{ color: '#004E63'}}>
          <HeadWorkers prefixDataKey="onedaysurgery">
            <PatrakDoctor />
            <JanataDoctor />
            <ParkanovaDoctor />
          </HeadWorkers>
        </ContentItem>
        <ContentItem className={styles.contentitem} style={{backgroundColor: '#5CC2D0', color: '#004E63'}}>
          <References />
        </ContentItem>
      </div>
    );
  }
}
