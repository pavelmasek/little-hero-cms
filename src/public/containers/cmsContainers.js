import HomePage from './HomePage/HomePage';
import OneDaySurgery from './OneDaySurgery/OneDaySurgery';
import Ambulance from './Ambulance/Ambulance';
import PlasticSurgery from './PlasticSurgery/PlasticSurgery';
import NewsPage from './NewsPage/NewsPage';


export default [
  {
    name: 'Homepage',
    component: HomePage,
    path: 'homepage'
  },
  {
    name: 'Jednodenní chirurgie',
    component: OneDaySurgery,
    path: 'jednodenni-chirurgie'
  },
  {
    name: 'Ambulance',
    component: Ambulance,
    path: 'ambulance'
  },
  {
    name: 'Plastická chirurgie',
    component: PlasticSurgery,
    path: 'plasticka-chirurgie'
  },
  {
    name: 'Aktuality',
    component: NewsPage,
    path: 'aktuality'
  }
];
