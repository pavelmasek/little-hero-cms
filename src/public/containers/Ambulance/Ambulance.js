import React, { Component } from 'react';

import ContentItem from 'public/components/ContentItem/ContentItem';
import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import HeadWorkers, { PatrakDoctor, JanataDoctor, ParkanovaDoctor } from 'public/components/HeadWorkers/HeadWorkers.js';
import SmallMenu from 'public/components/SmallMenu/SmallMenu.js';
import BackgroundComponent from 'public/components/BackgroundComponent/BackgroundComponent.js';
import OfficeHours from 'public/components/OfficeHours/OfficeHours.js';
import { Link } from 'react-router';
import { Aligner } from 'public/components/LayoutComponents/LayoutComponents';
import References from 'public/components/References/References';
import SEO from 'admin/components/SEO';
import OfficeHoursTable from 'public/components/OfficeHoursTable';
import CMSImage from 'admin/components/CMSImage';
import Form from 'public/components/Form/Form';

const styles = require('public/styles/styles.scss');
const less = require('public/styles/style.less');

export default class Ambulance extends Component {

  render() {
    return (
      <div>
        <SEO dataKey="ambulancepage"/>
        <ContentItem percentHeight={50} className={styles.contentItemHeader}>
          <BackgroundComponent className={styles.contentItemHeader}>
            <div style={{height: '100%'}}>
              <div style={{height: '25%'}}>
                <div></div>
              </div>
              <div style={{textAlign: 'center', height: '50%'}}>
                <Aligner verticalAlign="middle">
                  <Link to="/">
                    <CMSImage dataKey="ansalogo" className={styles.ansaLogo} />
                  </Link>
                </Aligner>
              </div>
              <div style={{height: '25%'}}>
                <Aligner verticalAlign="middle">
                  <div className={styles.headerText}><MultiLanguageContent dataKey="headerAmbulance" /></div>
                </Aligner>
              </div>
            </div>
          </BackgroundComponent>
        </ContentItem>
        <ContentItem alignItems="middle" style={{padding: '2em', backgroundColor: '#004E63'}} className={styles.contentitem}>
          <SmallMenu selected={1} />
        </ContentItem>
        <ContentItem className={styles.contentitem} alignItems="middle" style={{padding: '2em', backgroundColor: '#5CC2D0'}}>
          <div>
            <div className={styles.text} style={{ marginLeft: 'auto', marginRight: 'auto', maxWidth: '1024px'}}>
              <MultiLanguageContent dataKey="infoAmbulance" />
              <div style={{textAlign: 'center'}}>
                <Form
                  openButtonText={
                    <MultiLanguageContent dataKey="infoAndPriceButton" display="inline-block" style={{minHeight: '1em'}} />
                  }
                  sendButtonText={
                    <MultiLanguageContent dataKey="formSendButtonText" display="inline-block" style={{minHeight: '1em'}} />
                  }
                  formHeaderText={
                    <MultiLanguageContent dataKey="formHeaderText" display="inline-block" style={{minHeight: '1em'}} />
                  }
                  formStyleClassName={less.formOnLight}
                  buttonStyleClassName={less.contactUsButton} />
              </div>
            </div>
          </div>
        </ContentItem>
        <ContentItem style={{padding: '2em'}} className={[styles.contentitem, styles.darkBlueColor].join(' ')}>
          <OfficeHoursTable prefixDataKey="ambulance" days={[1, 2, 3, 4, 5]} />
        </ContentItem>
        <ContentItem className={styles.whiteColor + ' ' + styles.darkBlueBackground + ' ' + styles.contentitem}>
          <OfficeHours />
          <div style={{textAlign: 'center'}}>
            <Link to="/" style={{color: 'inherit', textDecoration: 'none'}}>
              <div className={styles.button}>
                <MultiLanguageContent dataKey="back" />
              </div>
            </Link>
          </div>
        </ContentItem>
        {
          /*
          <ContentItem alignItems="middle" className={styles.contentitem + ' ' + styles.third}>
            <NewsLink />
          </ContentItem>
          */
        }
        <ContentItem style={{color: '#004E63'}} className={styles.contentitem}>
          <HeadWorkers prefixDataKey="ambulance">
            <PatrakDoctor />
            <JanataDoctor />
            <ParkanovaDoctor />
          </HeadWorkers>
        </ContentItem>
        <ContentItem style={{padding: '2em', backgroundColor: '#5CC2D0', color: '#004E63'}}>
          <References />
        </ContentItem>
      </div>
    );
  }
}
