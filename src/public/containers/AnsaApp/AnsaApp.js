import React, { Component } from 'react';
import PropTypes from 'prop-types';


import Helmet from 'react-helmet';
import config from 'config';
import * as languageActions from 'admin/redux/modules/language';
import { connect } from 'react-redux';
// import { routeActions } from 'react-router-redux';
import { Aligner } from 'public/components/LayoutComponents/LayoutComponents';
import { asyncConnect } from 'redux-connect';
import { Link } from 'react-router';
import { getRouteByData } from 'routesSet';
import ContentItem from 'public/components/ContentItem/ContentItem';
import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import CMSImage from 'admin/components/CMSImage';
import Map from 'public/components/Map/Map';
import ResponsiveView, { RULES } from 'admin/components/ResponsiveView';

const styles = require('./AnsaApp.scss');
// const ansaLogoSvg = 'https://s3.eu-central-1.amazonaws.com/calm-beach-40263-storage/images/logo.svg';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];
    console.log(dispatch, getState);
    return Promise.all(promises);
  }
}])

@connect(state => ({
  ...state,
  language: state.language.language
}), {...languageActions})

export default class AnsaApp extends Component {

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    setCS: PropTypes.func,
    setEN: PropTypes.func,
    setDE: PropTypes.func,
    setPL: PropTypes.func,
    language: PropTypes.string,
    location: PropTypes.object
  };

  state = {
    backgroundIndex: 0,
    windowWidth: 0
  };
  replaceLanguage() {
    return this.props.location.pathname.replace(/\/cs\/|\/en\/|\/de\/|\/pl\/|\/cs|\/en|\/de|\/pl/g, '');
  }
  render() {
    const { setCS, setEN, setDE, setPL, language } = this.props;
    const locationWithoutLanguage = this.replaceLanguage();
    const route = getRouteByData(locationWithoutLanguage, this.props.language);
    // console.log('route ', route);

    return (
      <div ref="container" className={styles.ansaapp}>
        <Helmet htmlAttributes={{'lang': language}} {...config.app.head} />
        <div>
          <div>
            <div className={styles.languageWrapper}>
              <div className={(language === 'cs') ? styles.selectedLanguage : undefined} onClick={setCS}>
                <Link to={ '/cs/' + ((route && route.cs) ? route.cs : '')}>
                  <Aligner verticalAlign="middle" horizontalAlign="center">
                    cs
                  </Aligner>
                </Link>
              </div>
              <div className={(language === 'en') ? styles.selectedLanguage : undefined} onClick={setEN}>
                <Link to={ '/en/' + ((route && route.en) ? route.en : '')}>
                  <Aligner verticalAlign="middle" horizontalAlign="center">
                    en
                  </Aligner>
                </Link>
              </div>
              <div className={(language === 'de') ? styles.selectedLanguage : undefined} onClick={setDE}>
                <Link to={ '/de/' + ((route && route.de) ? route.de : '')}>
                  <Aligner verticalAlign="middle" horizontalAlign="center">
                    de
                  </Aligner>
                </Link>
              </div>
              <div className={(language === 'pl') ? styles.selectedLanguage : undefined} onClick={setPL}>
                <Link to={ '/pl/' + ((route && route.pl) ? route.pl : '')}>
                  <Aligner verticalAlign="middle" horizontalAlign="center">
                    pl
                  </Aligner>
                </Link>
              </div>
            </div>
          </div>
          {this.props.children}
          <ContentItem className={styles.darkBlueColor} style={{padding: '2em', textAlign: 'center'}}>
            <MultiLanguageContent dataKey="parking" />
          </ContentItem>
          <ContentItem>
            <a href="https://www.google.cz/maps/place/ANSA+Sanatorium+s.r.o.+-+MUDr.+Ji%C5%99%C3%AD+Patr%C3%A1k/@50.6258833,15.6125056,17z/data=!3m1!4b1!4m2!3m1!1s0x470e954ea8755e13:0xb44cdc8c2be0cd66?hl=cs">
              <div style={{height: '600px'}}>
                <Map />
              </div>
            </a>
          </ContentItem>
          <div className={styles.darkBlueBackground}>
            <ResponsiveView showOn={[RULES.SHOW_ON_DESKTOP, RULES.SHOW_ON_TABLET_LANDSCAPE]}>
            <div style={{padding: '2em 0', textAlign: 'center'}}>
              <table style={{marginLeft: 'auto', marginRight: 'auto', height: '100%'}}>
                <tbody>
                  <tr>
                    <td>
                      <CMSImage dataKey="footerlogo" style={{ width: 300 }} />
                    </td>
                    <td style={{height: '100%', padding: '0 1em'}}>
                      <div className={styles.whiteColor}>
                        <MultiLanguageContent className={styles.lightBlueColor} dataKey="footertextdarkbluefirstline" display="inline-block" />
                        <MultiLanguageContent dataKey="footertextfirstline" display="inline-block" />
                        <MultiLanguageContent dataKey="footertextsecondline" />
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            </ ResponsiveView>
            <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE, RULES.SHOW_ON_TABLET_PORTRAIT]}>
              <div className={styles.footerMobile}>
                <div>
                  <CMSImage dataKey="footerlogo" />
                </div>
                <div><div><span className={styles.lightBlueColor}>ANSA Sanatorium, s.r.o.</span> <br /> Krkonošská 153 | 543 01 Vrchlabí | IČO: 25950924</div></div>
                <div><div>telefon: +420 499 429 439 | mobil: +420 777 168 037 <br /> email: info@chirurgie-ansa.cz</div></div>
              </div>
            </ResponsiveView>
          </div>
        </div>
      </div>
    );
  }
}
