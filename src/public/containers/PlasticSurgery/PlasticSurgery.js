import React, { Component } from 'react';

import ContentItem from 'public/components/ContentItem/ContentItem';
import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import HeadWorkers, { SchmoranzovaDoctor } from 'public/components/HeadWorkers/HeadWorkers.js';
import SmallMenu from 'public/components/SmallMenu/SmallMenu.js';
import BackgroundComponent from 'public/components/BackgroundComponent/BackgroundComponent.js';
import { Link } from 'react-router';
import References from 'public/components/References/References';
import { Aligner } from 'public/components/LayoutComponents/LayoutComponents';
import OfficeHoursTable from 'public/components/OfficeHoursTable';
import SEO from 'admin/components/SEO';
import CMSImage from 'admin/components/CMSImage';
import Form from 'public/components/Form/Form';

const styles = require('public/styles/styles.scss');
const less = require('public/styles/style.less');

export default class PlasticSurgery extends Component {

  render() {
    return (
      <div>
        <SEO dataKey="plasticsurgerypage"/>
        <ContentItem percentHeight={50} className={styles.contentItemHeader}>
          <BackgroundComponent className={styles.contentItemHeader}>
            <div style={{height: '100%'}}>
              <div style={{height: '25%'}}>
                <div></div>
              </div>
              <div style={{textAlign: 'center', height: '50%'}}>
                <Aligner verticalAlign="middle">
                  <Link to="/">
                    <CMSImage dataKey="ansalogo" className={styles.ansaLogo} />
                  </Link>
                </Aligner>
              </div>
              <div style={{height: '25%'}}>
                <Aligner verticalAlign="middle">
                  <div className={styles.headerText}><MultiLanguageContent dataKey="headerPlastic" /></div>
                </Aligner>
              </div>
            </div>
          </BackgroundComponent>
        </ContentItem>
        <ContentItem alignItems="middle" style={{padding: '2em', backgroundColor: '#004E63'}}>
          <SmallMenu selected={2} />
        </ContentItem>
        <ContentItem alignItems="middle" percentHeight={75} style={{padding: '2em', backgroundColor: '#5CC2D0'}}>
          <div>
            <div className={styles.text} style={{ marginLeft: 'auto', marginRight: 'auto', maxWidth: '1024px'}}>
              <MultiLanguageContent dataKey="infoPlastic" />
              <MultiLanguageContent style={{fontSize: '1.2em'}} dataKey="operationInfoPlastic" />
              <div style={{textAlign: 'center'}}>
                <Form
                  openButtonText={
                    <MultiLanguageContent dataKey="infoAndPriceButton" display="inline-block" style={{minHeight: '1em'}} />
                  }
                  sendButtonText={
                    <MultiLanguageContent dataKey="formSendButtonText" display="inline-block" style={{minHeight: '1em'}} />
                  }
                  formHeaderText={
                    <MultiLanguageContent dataKey="formHeaderText" display="inline-block" style={{minHeight: '1em'}} />
                  }
                  formStyleClassName={less.formOnLight}
                  buttonStyleClassName={less.contactUsButton} />
              </div>
            </div>
            <div style={{textAlign: 'center'}}>
              <br />
              <Link to="/" style={{color: 'inherit', textDecoration: 'none'}}>
                <div className={styles.buttonDarkerHover}>
                  <MultiLanguageContent dataKey="back" />
                </div>
              </Link>
            </div>
          </div>
        </ContentItem>
        <ContentItem style={{padding: '2em', color: '#004E63'}}>
          <HeadWorkers prefixDataKey="plasticsurgery">
            <SchmoranzovaDoctor />
          </HeadWorkers>
        </ContentItem>
        <ContentItem style={{padding: '2em'}} className={[styles.contentitem, styles.darkBlueColor].join(' ')}>
          <OfficeHoursTable prefixDataKey="plastic" days={[1]} info={
              <div>
                <a href="tel:777165037">tel.: 777 165 037</a>
                <br />
                <a href="tel:499429439">tel.: 499 429 439</a>
                <br />
                <a href="mailto:info@ansasanatorium.cz">info@ansasanatorium.cz</a>
              </div>
            } />
          </ContentItem>
        <ContentItem style={{padding: '2em', backgroundColor: '#5CC2D0', color: '#004E63'}}>
          <References />
        </ContentItem>
      </div>
    );
  }
}
