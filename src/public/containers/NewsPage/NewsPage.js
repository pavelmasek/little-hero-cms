import React, { Component } from 'react';

import HeadWorkers, { PatrakDoctor, JanataDoctor, SchmoranzovaDoctor, ParkanovaDoctor } from 'public/components/HeadWorkers/HeadWorkers.js';
import BackgroundComponent from 'public/components/BackgroundComponent/BackgroundComponent.js';
import OfficeHours from 'public/components/OfficeHours/OfficeHours.js';
import ContentItem from 'public/components/ContentItem/ContentItem';
import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import References from 'public/components/References/References';
import { Link } from 'react-router';
import { Aligner } from 'public/components/LayoutComponents/LayoutComponents';
import SEO from 'admin/components/SEO';
import CMSImage from 'admin/components/CMSImage';

const styles = require('public/styles/styles.scss');
const alertIcon = 'https://s3.eu-central-1.amazonaws.com/calm-beach-40263-storage/images/alert.svg';

export default class NewsPage extends Component {
  render() {
    return (
      <div>
        <SEO dataKey="newspage"/>
        <ContentItem percentHeight={50} className={styles.contentItemHeader}>
          <BackgroundComponent className={styles.contentItemHeader}>
            <div style={{height: '100%'}}>
              <div style={{height: '25%'}}>
                <div></div>
              </div>
              <div style={{textAlign: 'center', height: '50%'}}>
                <Aligner verticalAlign="middle">
                  <Link to="/">
                    <CMSImage dataKey="ansalogo" className={styles.ansaLogo} />
                  </Link>
                </Aligner>
              </div>
              <div style={{height: '25%'}}>
                <Aligner verticalAlign="middle">
                  <div className={styles.headerText}><MultiLanguageContent dataKey="news" /></div>
                </Aligner>
              </div>
            </div>
          </BackgroundComponent>
        </ContentItem>
        <ContentItem alignItems="middle" className={styles.contentitem + ' ' + styles.whiteColor + ' ' + styles.third}>
          <div style={{maxWidth: '800px', marginLeft: 'auto', marginRight: 'auto'}}>
            <div className={styles.horizontalContainer + ' ' + styles.everythingCenter} style={{height: '100%'}}>
              <div className={[styles.info, styles.first].join(' ')} >
                <img src={alertIcon} />
              </div>
              <div className={[styles.info, styles.second].join(' ')}>
                <MultiLanguageContent dataKey="newsText" dataKey="newsText" />
              </div>
            </div>
            <div style={{textAlign: 'center'}}>
              <Link to="/" style={{color: 'inherit', textDecoration: 'none'}}>
                <div className={styles.buttonDarkerHover}>
                  <MultiLanguageContent dataKey="back" />
                </div>
              </Link>
            </div>
          </div>
        </ContentItem>
        <ContentItem alignItems="middle" className={styles.contentitem + ' ' + styles.darkBlueColor}>
          <HeadWorkers>
            <PatrakDoctor />
            <JanataDoctor />
            <SchmoranzovaDoctor />
            <ParkanovaDoctor />
          </HeadWorkers>
        </ContentItem>
        <ContentItem className={styles.contentitem + ' ' + styles.darkBlueColor + ' ' + styles.third}>
          <References />
        </ContentItem>
        <ContentItem className={styles.contentitem + ' ' + styles.whiteColor + ' ' + styles.darkBlueBackground}>
          <OfficeHours />
        </ContentItem>
      </div>
    );
  }
}
