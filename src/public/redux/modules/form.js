const SEND_CONTACT_FORM = 'SEND_CONTACT_FORM';
const SEND_CONTACT_FORM_SUCCESS = 'SEND_CONTACT_FORM_SUCCESS';
const SEND_CONTACT_FORM_FAILURE = 'SEND_CONTACT_FORM_FAILURE';

const initialState = {
  sended: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SEND_CONTACT_FORM:
      return {
        ...state,
        sended: false
      };
    case SEND_CONTACT_FORM_SUCCESS:
      return {
        ...state,
        sended: true
      };
    case SEND_CONTACT_FORM_FAILURE:
      return {
        ...state,
        sended: false
      };
    default:
      return state;
  }
}

export function sendContactForm(data) {
  return {
    types: [SEND_CONTACT_FORM, SEND_CONTACT_FORM_SUCCESS, SEND_CONTACT_FORM_FAILURE],
    promise: (client) => client.post('/sendContactForm', { data: { ...data } })
  };
}
