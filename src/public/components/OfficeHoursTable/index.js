import React, { Component } from 'react';
import PropTypes from 'prop-types';


import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';

const style = require('./style.scss');
const glStyles = require('public/styles/styles.scss');

export default class OfficeHoursTable extends Component {
  static propTypes = {
    days: PropTypes.array.isRequired,
    info: PropTypes.node,
    prefixDataKey: PropTypes.string.isRequired,
    style: PropTypes.object
  }
  render() {
    const { days, info, prefixDataKey } = this.props;
    return (
      <div>
        <div style={{fontSize: '2em', textAlign: 'center', padding: '.5em'}}>
          <MultiLanguageContent dataKey={`${prefixDataKey}OfficeHoursTableHeader`} />
        </div>
        <table style={Object.assign({fontSize: '1.35em', marginLeft: 'auto', marginRight: 'auto', width: '100%', maxWidth: '1024px'}, this.props.style)} className={[style.tableOfficeHours, glStyles.showOnDesktop].join(' ')}>
          <tbody>
              { days.map((day, index) => {
                return (
                  <tr className={style.tableRow} key={index} style={{padding: '0.5em 0'}}>
                    <td style={{padding: '.5em 0.5em', textAlign: 'center'}}>
                      <MultiLanguageContent dataKey={`${prefixDataKey}days${day}.name`} />
                    </td>
                    <td style={{padding: '.5em 0.25em', textAlign: 'right'}}>
                      <MultiLanguageContent dataKey={`${prefixDataKey}days${day}.time`} />
                    </td>
                    <td style={{padding: '.5em 0.25em'}}>
                      <MultiLanguageContent dataKey={`${prefixDataKey}days${day}.timeInfo`} />
                    </td>
                    <td style={{padding: '.5em 0.25em'}}>
                      { <MultiLanguageContent dataKey={`${prefixDataKey}days${day}.doctor`} /> }
                    </td>
                    <td style={{padding: '.5em 0.25em', textAlign: 'center'}}>
                      { info }
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        <div className={[style.tableOfficeHours, glStyles.showOnMobile].join(' ')}>
          { days.map((day, index) => {
            return (
              <div className={style.tableRow} key={index} style={{paddingTop: '.25em', paddingBottom: '.25em'}}>
                <div style={{fontSize: '2em', textAlign: 'center'}}><MultiLanguageContent multiLangData={day.name} /></div>
                <table style={{marginLeft: 'auto', marginRight: 'auto', fontSize: '1.15em'}}>
                  <tbody>
                    <tr>
                      <td style={{textAlign: 'right', paddingRight: '5px'}}>
                        <MultiLanguageContent dataKey={`${prefixDataKey}daymobile${day}.time`} />
                      </td>
                      <td>
                        <MultiLanguageContent dataKey={`${prefixDataKey}daymobile${day}.timeInfo`} />
                      </td>
                    </tr>
                  </tbody>
                </table>
                <div style={{fontSize: '1.15em', textAlign: 'center'}}><MultiLanguageContent dataKey={`${prefixDataKey}daymobile${day}.doctor`} /></div>
              </div>
            );
          })}
          <div style={{fontSize: '1.75em', textAlign: 'center', color: 'white', backgroundColor: '#004E63', padding: '0.75em 0', marginTop: '1em'}}>
            <a href="tel:777165037">OBJEDNAT TELEFONICKY</a>
          </div>
        </div>
      </div>
    );
  }
}
