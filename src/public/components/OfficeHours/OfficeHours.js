import React, { Component } from 'react';
import PropTypes from 'prop-types';


import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import { HorizontalContainer, HorizontalItem } from 'public/components/LayoutComponents/LayoutComponents';

const styles = require('public/styles/styles.scss');

const verticalAlign = {
  position: 'absolute',
  top: '50%',
  transform: 'translateY(-50%)',
  left: 0,
  right: 0
};

export default class OfficeHours extends Component {
  static propTypes = {
    languageData: PropTypes.object
  }
  state = {
    selectedRoundButton: (new Date().getDay() === 0) ? 6 : new Date().getDay() - 1
  };
  onWeekNameClick(index) {
    this.setState({ selectedRoundButton: index });
  }
  render() {
    const selectedRoundButton = [styles.roundButton, styles.selectedButton].join(' ');
    return (
      <div>
        <div style={{fontSize: '2em', padding: '1em 0', textAlign: 'center'}}>
          <MultiLanguageContent dataKey="contact"/>
        </div>
        <div>
          <HorizontalContainer justifyItems="center" alignItems="middle" className="week-names" style={{maxWidth: '1024px', width: 'auto', marginLeft: 'auto', marginRight: 'auto'}}>
            <HorizontalItem style={{width: '1px'}} className="day-of-week">
              <div onClick={this.onWeekNameClick.bind(this, 0)} className={(this.state.selectedRoundButton === 0) ? selectedRoundButton : styles.roundButton}>
                <MultiLanguageContent style={verticalAlign} dataKey="monday"/>
              </div>
            </HorizontalItem>
            <HorizontalItem style={{width: '1px'}} className="day-of-week">
              <div onClick={this.onWeekNameClick.bind(this, 1)} className={(this.state.selectedRoundButton === 1) ? selectedRoundButton : styles.roundButton}>
                <MultiLanguageContent style={verticalAlign} dataKey="thuesday" />
              </div>
            </HorizontalItem>
            <HorizontalItem style={{width: '1px'}} className="day-of-week">
              <div onClick={this.onWeekNameClick.bind(this, 2)} className={(this.state.selectedRoundButton === 2) ? selectedRoundButton : styles.roundButton}>
                <MultiLanguageContent style={verticalAlign} dataKey="wednesday" />
              </div>
            </HorizontalItem>
            <HorizontalItem style={{width: '1px'}} className="day-of-week">
              <div onClick={this.onWeekNameClick.bind(this, 3)} className={(this.state.selectedRoundButton === 3) ? selectedRoundButton : styles.roundButton}>
                <MultiLanguageContent style={verticalAlign} dataKey="thirsday" />
              </div>
            </HorizontalItem>
            <HorizontalItem style={{width: '1px'}} className="day-of-week">
              <div onClick={this.onWeekNameClick.bind(this, 4)} className={(this.state.selectedRoundButton === 4) ? selectedRoundButton : styles.roundButton}>
                <MultiLanguageContent style={verticalAlign} dataKey="friday" />
              </div>
            </HorizontalItem>
            <HorizontalItem style={{width: '1px'}} className="day-of-week">
              <div onClick={this.onWeekNameClick.bind(this, 5)} className={(this.state.selectedRoundButton === 5) ? selectedRoundButton : styles.roundButton}>
                <MultiLanguageContent style={verticalAlign} dataKey="saturday" />
              </div>
            </HorizontalItem>
            <HorizontalItem style={{width: '1px'}} className="day-of-week">
              <div onClick={this.onWeekNameClick.bind(this, 6)} className={(this.state.selectedRoundButton === 6) ? selectedRoundButton : styles.roundButton}>
                <MultiLanguageContent style={verticalAlign} dataKey="sunday" />
              </div>
            </HorizontalItem>
          </HorizontalContainer>
        </div>
        <div>
          <div style={{position: 'relative', textAlign: 'center'}}>
            <div className={ this.state.selectedRoundButton === 0 ? [styles.timetable, styles.timetableSelected].join(' ') : styles.timetable } style={{fontSize: '1.5em', padding: '1.5em 0'}}>
              <MultiLanguageContent dataKey="mondayTime" />
            </div>
            <div className={ this.state.selectedRoundButton === 1 ? [styles.timetable, styles.timetableSelected].join(' ') : styles.timetable } style={{fontSize: '1.5em', padding: '1.5em 0'}}>
              <MultiLanguageContent dataKey="thuesdayTime" />
            </div>
            <div className={ this.state.selectedRoundButton === 2 ? [styles.timetable, styles.timetableSelected].join(' ') : styles.timetable } style={{fontSize: '1.5em', padding: '1.5em 0'}}>
              <MultiLanguageContent dataKey="wednesdayTime" />
            </div>
            <div className={ this.state.selectedRoundButton === 3 ? [styles.timetable, styles.timetableSelected].join(' ') : styles.timetable } style={{fontSize: '1.5em', padding: '1.5em 0'}}>
              <MultiLanguageContent dataKey="thirsdayTime" />
            </div>
            <div className={ this.state.selectedRoundButton === 4 ? [styles.timetable, styles.timetableSelected].join(' ') : styles.timetable } style={{fontSize: '1.5em', padding: '1.5em 0'}}>
              <MultiLanguageContent dataKey="fridayTime" />
            </div>
            <div className={ this.state.selectedRoundButton === 5 ? [styles.timetable, styles.timetableSelected].join(' ') : styles.timetable } style={{fontSize: '1.5em', padding: '1.5em 0'}}>
              <MultiLanguageContent dataKey="saturdayTime" />
            </div>
            <div className={ this.state.selectedRoundButton === 6 ? [styles.timetable, styles.timetableSelected].join(' ') : styles.timetable } style={{fontSize: '1.5em', padding: '1.5em 0'}}>
              <MultiLanguageContent dataKey="sundayTime" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
