import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from 'material-ui/svg-icons/navigation/close';

import { connect } from 'react-redux';
import * as formActions from 'public/redux/modules/form';

const less = require('public/styles/style.less');
const translations = require('./translations');

const defaultState = {
  formHidden: true,
  nameAndSurname: '',
  mail: '',
  phoneNumber: '',
  messageContent: '',
  nameAndSurnameIsNotEmpty: true,
  mailIsValid: true,
  phoneNumberIsValid: true,
  messageIsNotEmpty: true
};

@connect(state => ({
  ...state,
  ...state.editor,
  ...state.language,
  sended: state.form.sended
}), {...formActions})


export default class Form extends Component {

  static propTypes = {
    editing: PropTypes.bool,
    formHeaderText: PropTypes.node,
    sendButtonText: PropTypes.node,
    openButtonText: PropTypes.node,
    openButtonIconClassName: PropTypes.string,
    sendButtonIconClassName: PropTypes.string,
    language: PropTypes.string,
    buttonStyleClassName: PropTypes.string,
    formStyleClassName: PropTypes.string,
    sendContactForm: PropTypes.func,
    sendWatterForChildrenReservationForm: PropTypes.func,
    sendSpecialOffersReservationForm: PropTypes.func,
    formType: PropTypes.string,
    additionalData: PropTypes.object,
    style: PropTypes.object
  };

  state = {
    ...defaultState
  }

  onSendMail() {
    if (this.validateForm() && !this.props.editing) {
      this.props.sendContactForm(this.state);
      this.switchVisibility();
      this.setState({...defaultState});
    }
  }
  onChange(event) {
    const val = event.target.value;
    switch (event.target.name) {
      case 'nameAndSurname':
        if (this.checkContentIsNotEmpty(val)) {
          this.setState({
            nameAndSurname: val,
            nameAndSurnameIsNotEmpty: true
          });
        } else {
          this.setState({
            nameAndSurname: val
          });
        }
        break;
      case 'mail':
        if (this.checkMail(val)) {
          this.setState({
            mail: val,
            mailIsValid: true
          });
        } else {
          this.setState({
            mail: val
          });
        }
        break;
      case 'phoneNumber':
        if (this.checkPhoneNumber(val)) {
          this.setState({
            phoneNumber: val,
            phoneNumberIsValid: true
          });
        } else {
          this.setState({
            phoneNumber: val
          });
        }
        break;
      case 'messageContent':
        if (this.checkContentIsNotEmpty(val)) {
          this.setState({
            messageIsNotEmpty: true,
            messageContent: val
          });
        } else {
          this.setState({
            messageIsNotEmpty: val
          });
        }
        break;
      default:
        break;
    }
  }
  switchVisibility() {
    this.setState({
      formHidden: !this.state.formHidden
    });
  }
  validateForm() {
    const isValid = this.checkMail(this.state.mail) && this.checkPhoneNumber(this.state.phoneNumber) && this.checkContentIsNotEmpty(this.state.messageContent) && this.checkContentIsNotEmpty(this.state.nameAndSurname);
    if (!isValid) {
      console.log('Something is not ok');
      if (!this.checkMail(this.state.mail)) {
        console.log('Mail is not ok');
        this.setState({ mailIsValid: false });
      }
      if (!this.checkPhoneNumber(this.state.phoneNumber)) {
        this.setState({ phoneNumberIsValid: false });
        console.log('Phone is not ok');
      }
      if (!this.checkContentIsNotEmpty(this.state.messageContent)) {
        this.setState({ messageIsNotEmpty: false });
        console.log('Message is not ok');
      }
      if (!this.checkContentIsNotEmpty(this.state.nameAndSurname)) {
        this.setState({ nameAndSurnameIsNotEmpty: false });
        console.log('nameAndSurnameIsNotEmpty is not ok');
      }
    }
    return isValid;
  }
  checkMail(value) {
    return /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(value);
  }
  checkPhoneNumber(value) {
    const escapedValue = value.replace(/[\s\+]/g, '');
    return +escapedValue > 1000000;
  }
  checkContentIsNotEmpty(value) {
    return !!value;
  }
  render() {
    const { editing, language } = this.props;
    return (
      <div style={this.props.style}>
        <div onClick={this.switchVisibility.bind(this)} style={{ display: ((!this.state.formHidden && !editing) ? 'none' : undefined)}} className={this.props.buttonStyleClassName}>
          <div style={{display: 'table'}}>
            <div style={{display: 'table-row'}}>
              <div style={{display: 'table-cell', minHeight: '100%', verticalAlign: 'middle'}}>
                <span style={{verticalAlign: 'middle'}}>{this.props.openButtonText}</span>
              </div>
              <div style={{display: 'table-cell', verticalAlign: 'middle'}}>
                <div className={this.props.openButtonIconClassName}></div>
              </div>
            </div>
          </div>
        </div>
        <div style={{ display: ((this.state.formHidden && !editing) ? 'none' : undefined)}}>
          <div className={this.props.formStyleClassName} style={{position: 'relative'}}>
            <CloseIcon
              onClick={() => { this.setState({formHidden: true}); }}
              style={{ width: 48, height: 48, position: 'absolute', top: 0, right: 0, cursor: 'pointer', padding: '1em' }}
              color="#004E63"
              hoverColor="#fff" />
            <h3 className={less.colorOrange}>{this.props.formHeaderText}</h3>
            <input className={(!this.state.nameAndSurnameIsNotEmpty) ? less.invalid : undefined} name="nameAndSurname" onChange={this.onChange.bind(this)} type="text" placeholder={translations.nameAndSurname[language]} value={this.state.nameAndSurname} />
            <br />
            <br />
            <input className={(!this.state.mailIsValid) ? less.invalid : undefined} name="mail" onChange={this.onChange.bind(this)} type="text" placeholder="mail" value={this.state.mail} />
            <br />
            <br />
            <input className={(!this.state.phoneNumberIsValid) ? less.invalid : undefined} name="phoneNumber" onChange={this.onChange.bind(this)} type="text" placeholder={translations.phoneNumber[language]} value={this.state.phoneNumber} />
            <br />
            <br />
            <textarea className={(!this.state.messageIsNotEmpty) ? less.invalid : undefined} name="messageContent" onChange={this.onChange.bind(this)} placeholder={translations.messageContent[language]} rows="10" value={this.state.messageContent}/>
            <br />
            <br />
            <div onClick={this.onSendMail.bind(this)} className={this.props.buttonStyleClassName}>
              <div style={{display: 'table'}}>
                <div style={{display: 'table-row'}}>
                  <div style={{display: 'table-cell', minHeight: '100%', verticalAlign: 'middle'}}>
                    <span style={{verticalAlign: 'middle'}}>{this.props.sendButtonText}</span>
                  </div>
                  <div style={{display: 'table-cell', verticalAlign: 'middle'}}>
                    <div className={this.props.sendButtonIconClassName}></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
