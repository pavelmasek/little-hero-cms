import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = require('./ContentItem.scss');

export default class ContentItem extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    percentHeight: PropTypes.number,
    style: PropTypes.object,
    alignItems: PropTypes.string,
    className: PropTypes.string
  };
  state = {
    windowHeight: 0
  };
  componentDidMount() {
    this.onResize();
    window.addEventListener('resize', this.onResize.bind(this));
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize.bind(this));
  }
  onResize() {
    this.setState({ windowHeight: window.innerHeight });
  }
  render() {
    const percentHeight = this.props.percentHeight || undefined;
    const style = {
      ...this.props.style,
      minHeight: percentHeight + 'vh'
    };
    return (
      <div ref="ContentItem" className={this.props.className + ' ' + styles.contentItem} style={style}>
        {this.props.children}
      </div>
    );
  }
}
