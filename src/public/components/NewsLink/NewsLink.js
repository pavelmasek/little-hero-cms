import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { HorizontalContainer, HorizontalItem } from 'public/components/LayoutComponents/LayoutComponents';
import { Link } from 'react-router';
import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import { getLanguageRoute } from 'routesSet';
import {connect} from 'react-redux';
import * as languageActions from 'admin/redux/modules/language';
import ShowAndHide from 'admin/components/ShowAndHide';

const styles = require('public/styles/styles.scss');
const alertIcon = 'https://s3.eu-central-1.amazonaws.com/calm-beach-40263-storage/images/alert.svg';

@connect(state => ({
  language: state.language.language
}), {...languageActions})

export default class NewsLink extends Component {

  static propTypes = {
    language: PropTypes.string
  };

  render() {
    return (
        <div style={{height: '100%'}}>
          <HorizontalContainer width="auto" style={{marginLeft: 'auto', marginRight: 'auto'}}>
            <ShowAndHide dataKey="showNews">
              <HorizontalItem>
                <div className={[styles.info, styles.first].join(' ')} >
                  <img src={alertIcon} />
                </div>
              </HorizontalItem>
              <HorizontalItem verticalAlign="middle">
                <div style={{padding: '0 1em'}}>
                  <div style={{margin: 0, fontSize: '1.75em'}}>
                    <MultiLanguageContent style={{fontSize: '1.5em'}} dataKey="newslinktext" />
                  </div>
                </div>
              </HorizontalItem>
              <HorizontalItem verticalAlign="middle">
                <div style={{margin: '3px'}} className={[styles.info, styles.third].join(' ')}>
                  <Link to={'/' + this.props.language + '/' + getLanguageRoute('news')[this.props.language]}>
                    <div className={styles.buttonToDarkBlue}>
                      <MultiLanguageContent dataKey="more" />
                    </div>
                  </Link>
                </div>
              </HorizontalItem>
            </ShowAndHide>
          </HorizontalContainer>
        </div>
    );
  }
}
