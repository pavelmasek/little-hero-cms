import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class Aligner extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    horizontalAlign: PropTypes.string,
    verticalAlign: PropTypes.string,
    style: PropTypes.object
  };
  render() {
    const style = {
      display: 'table',
      width: '100%',
      height: '100%',
      tableLayout: 'fixed',
      textAlign: this.props.horizontalAlign
    };
    Object.assign(style, this.props.style);
    return (
      <div style={style}>
        <div style={{display: 'table-cell', verticalAlign: this.props.verticalAlign}}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export class HorizontalContainer extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    className: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    style: PropTypes.object
  };
  render() {
    const style = {
      display: 'table',
      width: (this.props.width) ? this.props.width : '100%',
      height: this.props.height
    };
    Object.assign(style, this.props.style);
    return (
      <div className={this.props.className} style={style}>
        <div style={{display: 'table-row'}}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export class HorizontalItem extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    height: PropTypes.string,
    width: PropTypes.string,
    verticalAlign: PropTypes.string,
    style: PropTypes.object,
    className: PropTypes.string
  };
  render() {
    const style = {
      display: 'table-cell',
      height: this.props.height,
      width: this.props.width,
      verticalAlign: this.props.verticalAlign
    };
    Object.assign(style, this.props.style);
    return (
      <div style={style} className={this.props.className}>{this.props.children}</div>
    );
  }
}
const verticalAlign = {
  position: 'absolute',
  top: '50%',
  transform: 'translateY(-50%)',
  left: 0,
  right: 0
};

export class Center extends Component {

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    style: PropTypes.object
  };

  render() {
    return (
      <div style={Object.assign({position: 'relative'}, this.props.style)}>
        <div style={verticalAlign}>
          {this.props.children}
        </div>
      </div>
    );
  }
}
