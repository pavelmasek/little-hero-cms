import React, { Component } from 'react';

import {GoogleMapLoader, GoogleMap, OverlayView } from 'react-google-maps';

const ansaLogo = 'https://s3.eu-central-1.amazonaws.com/calm-beach-40263-storage/images/overlay.png';

export default class Map extends Component {
  render() {
    return (
      <section style={{height: '100%'}}>
        <GoogleMapLoader
          containerElement={
            <div
              style={{
                height: '100%',
              }}
            />
          }
          googleMapElement={
            <GoogleMap
              defaultZoom={17}
              defaultCenter={{ lat: 50.62577, lng: 15.612 }}
              defaultOptions={{
                draggable: false,
                zoomControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: true,
                disableDefaultUI: true
              }}
            >

              <OverlayView position={{ lat: 50.625758, lng: 15.612013 }} mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}>
                <img src={ansaLogo} style={{width: '85px', height: '100px', marginLeft: '-50px', marginTop: '-135px'}}/>
              </OverlayView>
            </GoogleMap>
          }
        />
      </section>
    );
  }
}
