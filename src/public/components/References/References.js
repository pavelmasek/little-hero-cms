import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { connect } from 'react-redux';

import Paper from 'material-ui/Paper';
import AddIcon from 'material-ui/svg-icons/content/add';
import RemoveIcon from 'material-ui/svg-icons/content/remove-circle-outline';
import * as Colors from 'material-ui/styles/colors';
import timestamp from 'public/utils/timestamp';

import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import SwitchContent from 'public/components/SwitchContent/SwitchContent.js';
import * as pageBuilderActions from 'admin/redux/modules/pageBuilder';

@connect(state => ({
  ...state,
  ...state.pageBuilder,
  editing: state.editor.editing
}), {...pageBuilderActions})
export default class References extends Component {

  static propTypes = {
    editing: PropTypes.bool,
    forceUpdateLanguageData: PropTypes.func,
    languageData: PropTypes.object
  }

  handleAddNewReference() {
    const { forceUpdateLanguageData, languageData } = this.props;
    const timeStamp = timestamp();
    const references = languageData.references;
    references.push({
      name: `refName${timeStamp}`,
      text: `refText${timeStamp}`
    });
    forceUpdateLanguageData('references', references);
  }

  render() {
    const { editing, languageData } = this.props;
    return (
      <div style={{height: '100%', padding: '1em'}}>
        <div style={{fontSize: '2em', padding: '0.5em 0', textAlign: 'center'}}>
          <MultiLanguageContent dataKey="refHeader" />
        </div>
        <div>
          <SwitchContent children={
            languageData.references && languageData.references.map((reference, key) => {
              return (
                <Reference
                  key={key}
                  objectKey={key}
                  nameDataKey={reference.name}
                  textDataKey={reference.text} />
              );
            })
          } />
          {
            editing &&
            <Paper
              zDepth={1}
              onClick={::this.handleAddNewReference}
              style={{
                width: 50,
                height: 50,
                padding: '.5em',
                margin: '.5em',
                cursor: 'pointer',
                marginLeft: 'auto',
                marginRight: 'auto',
                textAlign: 'center'
              }}>
              <table style={{ width: '100%', height: '100%'}}>
                <tbody>
                  <tr>
                    <td>
                      <AddIcon
                        style={{width: 32, height: 32}}
                        color={Colors.grey600} />
                    </td>
                  </tr>
                </tbody>
              </table>
            </Paper>
          }
        </div>
      </div>
    );
  }
}

const editingStyle = {
  padding: '1em 0',
  position: 'relative',
  maxWidth: '340px',
  marginLeft: 'auto',
  marginRight: 'auto'
};

@connect(state => ({
  ...state,
  ...state.pageBuilderActions,
  editing: state.editor.editing
}), {...pageBuilderActions})
export class Reference extends Component {

  static propTypes = {
    editing: PropTypes.bool,
    objectKey: PropTypes.number,
    nameDataKey: PropTypes.string,
    removeDataFromArray: PropTypes.func,
    textDataKey: PropTypes.string
  }

  handleRemoveReference() {
    const { objectKey, removeDataFromArray } = this.props;
    removeDataFromArray(objectKey, 'references');
  }

  render() {
    const { editing, nameDataKey, textDataKey } = this.props;
    return (
      <div style={(editing) ? editingStyle : { maxWidth: '340px', marginLeft: 'auto', marginRight: 'auto'}}>
        {
          editing &&
          <RemoveIcon
            color={Colors.red500}
            style={{ position: 'absolute', right: 0, cursor: 'pointer' }}
            onClick={::this.handleRemoveReference}/>
        }
        <MultiLanguageContent
          style={{fontSize: '24px', lineHeight: '1.1', fontWeight: 500, marginTop: '24px', marginBottom: '24px'}}
          placeholder="Vložte jméno klienta"
          dataKey={nameDataKey} />
        <MultiLanguageContent style={{fontSize: '16px'}} dataKey={textDataKey} />
      </div>
    );
  }
}
