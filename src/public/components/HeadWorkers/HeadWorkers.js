import React, { Component } from 'react';
import PropTypes from 'prop-types';


import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import { HorizontalContainer, HorizontalItem } from 'public/components/LayoutComponents/LayoutComponents';
import CMSImage from 'admin/components/CMSImage';
import ResponsiveView, { RULES } from 'admin/components/ResponsiveView';

const styles = require('./HeadWorkers.scss');

class HeadWorker extends Component {
  static propTypes = {
    className: PropTypes.string,
    dataKey: PropTypes.string,
    doctorName: PropTypes.string,
    children: PropTypes.node
  }
  render() {
    return (
      <div className={this.props.className + ' ' + styles.headWorker}>
        <CMSImage dataKey={this.props.dataKey} className={styles.photo} />
        <h3>{this.props.doctorName}</h3>
        {this.props.children}
      </div>
    );
  }
}

export const PatrakDoctor = (props) => {
  return (
    <HeadWorker dataKey="patrak" doctorName="MUDr. Jiří Patrák" {...props}>
      <MultiLanguageContent dataKey="headChief" />
    </HeadWorker>
  );
};

export const JanataDoctor = (props) => {
  return (
    <HeadWorker dataKey="janata" doctorName="MUDr. Petr Janata" {...props}>
      <MultiLanguageContent dataKey="doctorAmbulance" />
    </HeadWorker>
  );
};

export const SchmoranzovaDoctor = (props) => {
  return (
    <HeadWorker dataKey="schmoranzova" doctorName="MUDr. Alena Schmoranzová" {...props}>
      <MultiLanguageContent dataKey="doctorPlasticSurgery" />
    </HeadWorker>
  );
};

export const ParkanovaDoctor = (props) => {
  return (
    <HeadWorker dataKey="parkanova" doctorName="MUDr. Simona Parkánová" {...props}>
      <MultiLanguageContent dataKey="doctorAmbulance" />
    </HeadWorker>
  );
};

export default class HeadWorkers extends Component {

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    prefixDataKey: PropTypes.string
  }

  state = {
    windowWidth: 0,
    selectedHeadWorker: -1
  }
  componentDidMount() {
    this.setWindowWidth();
    window.addEventListener('resize', this.setWindowWidth.bind(this));
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.setWindowWidth.bind(this));
  }
  setWindowWidth() {
    this.setState({ windowWidth: window.innerWidth});
  }
  setActiveHeadWorker(index) {
    if (this.state.selectedHeadWorker === index) {
      this.setState({selectedHeadWorker: -1});
      return;
    }
    this.setState({selectedHeadWorker: index});
  }
  render() {
    const { prefixDataKey } = this.props;
    return (
      <div style={{height: '100%'}}>
        <div style={{fontSize: '2em', padding: '1rem'}}>
          <div style={{textAlign: 'center'}}><MultiLanguageContent dataKey="header" /></div>
        </div>
        <div style={{padding: '1em 0'}}>
         <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE, RULES.SHOW_ON_TABLET]}>
            <div>
              <HorizontalContainer>
                <HorizontalItem width="50%">
                  <div data-index={0} onClick={this.setActiveHeadWorker.bind(this, 0)} style={{textAlign: 'center'}}>
                    {
                      (this.props.children && this.props.children[0]) || this.props.children
                    }
                  </div>
                </HorizontalItem>
                { this.props.children && this.props.children[1] &&
                  <HorizontalItem width="50%">
                    <div data-index={1} onClick={this.setActiveHeadWorker.bind(this, 1)} style={{textAlign: 'center'}}>
                      { this.props.children[1] }
                    </div>
                  </HorizontalItem>
                }
              </HorizontalContainer>
              <div className={styles.doctorComment}>
                <div className={(this.state.selectedHeadWorker !== 0) ? styles.displayNone : ''}>
                  <MultiLanguageContent dataKey={`${prefixDataKey}doctor1`} />
                </div>
                <div className={(this.state.selectedHeadWorker !== 1) ? styles.displayNone : ''}>
                  <MultiLanguageContent dataKey={`${prefixDataKey}doctor2`} />
                </div>
              </div>
              <HorizontalContainer>
                <HorizontalItem width="50%">
                  <div data-index={2} onClick={this.setActiveHeadWorker.bind(this, 2)} style={{textAlign: 'center'}}>
                    {
                      this.props.children && this.props.children[2]
                    }
                  </div>
                </HorizontalItem>
                { this.props.children && this.props.children[3] &&
                  <HorizontalItem width="50%">
                    <div data-index={3} onClick={this.setActiveHeadWorker.bind(this, 3)} style={{textAlign: 'center'}}>
                      { this.props.children[3] }
                    </div>
                  </HorizontalItem>
                }
              </HorizontalContainer>
              <div className={styles.doctorComment}>
                <div className={(this.state.selectedHeadWorker !== 2) ? styles.displayNone : ''}>
                  <MultiLanguageContent dataKey={`${prefixDataKey}doctor3`} />
                </div>
                <div className={(this.state.selectedHeadWorker !== 3) ? styles.displayNone : ''}>
                  <MultiLanguageContent dataKey={`${prefixDataKey}doctor4`} />
                </div>
              </div>
            </div>
          </ResponsiveView>
          <ResponsiveView showOn={[RULES.SHOW_ON_DESKTOP]}>
            <div style={{maxWidth: 800, marginLeft: 'auto', marginRight: 'auto'}}>
              <HorizontalContainer>
                { ((this.props.children && this.props.children[0]) || this.props.children) &&
                  <HorizontalItem width="20%">
                    <div data-index={0} onClick={this.setActiveHeadWorker.bind(this, 0)} style={{textAlign: 'center'}}>
                      { this.props.children[0] || this.props.children }
                    </div>
                  </HorizontalItem>
                }
                { this.props.children && this.props.children[1] &&
                  <HorizontalItem width="20%">
                    <div data-index={1} onClick={this.setActiveHeadWorker.bind(this, 1)} style={{textAlign: 'center'}}>
                      { this.props.children[1] }
                    </div>
                  </HorizontalItem>
                }
                { this.props.children && this.props.children[2] &&
                  <HorizontalItem width="20%">
                    <div data-index={2} onClick={this.setActiveHeadWorker.bind(this, 2)} style={{textAlign: 'center'}}>
                      { this.props.children[2] }
                    </div>
                  </HorizontalItem>
                }
                { this.props.children && this.props.children[3] &&
                  <HorizontalItem width="20%">
                    <div data-index={3} onClick={this.setActiveHeadWorker.bind(this, 3)} style={{textAlign: 'center'}}>
                      { this.props.children[3] }
                    </div>
                  </HorizontalItem>
                }
              </HorizontalContainer>
              <div className={styles.doctorComment}>
                <div className={(this.state.selectedHeadWorker !== 0) ? styles.displayNone : ''}>
                  <MultiLanguageContent dataKey={`${prefixDataKey}doctor1`} />
                </div>
                <div className={(this.state.selectedHeadWorker !== 1) ? styles.displayNone : ''}>
                  <MultiLanguageContent dataKey={`${prefixDataKey}doctor2`} />
                </div>
                <div className={(this.state.selectedHeadWorker !== 2) ? styles.displayNone : ''}>
                  <MultiLanguageContent dataKey={`${prefixDataKey}doctor3`} />
                </div>
                <div className={(this.state.selectedHeadWorker !== 3) ? styles.displayNone : ''}>
                  <MultiLanguageContent dataKey={`${prefixDataKey}doctor4`} />
                </div>
              </div>
            </div>
          </ResponsiveView>
        </div>
      </div>
    );
  }
}
