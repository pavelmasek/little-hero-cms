import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { HorizontalContainer, HorizontalItem } from 'public/components/LayoutComponents/LayoutComponents';

import { Link } from 'react-router';
import MultiLanguageContent from 'admin/components/MultiLanguageContent/MultiLanguageContent';
import { getLanguageRoute } from 'routesSet';
import * as languageActions from 'admin/redux/modules/language';
import { connect } from 'react-redux';

import ResponsiveView, { RULES } from 'admin/components/ResponsiveView';

const style = require('./SmallMenu.scss');

@connect(state => ({
  ...state,
  language: state.language.language
}), {...languageActions})

export default class SmallMenu extends Component {
  static propTypes = {
    selected: PropTypes.number,
    language: PropTypes.string
  };
  render() {
    const { selected, language } = this.props;
    return (
      <div>
        <ResponsiveView showOn={[RULES.SHOW_ON_TABLET_LANDSCAPE, RULES.SHOW_ON_DESKTOP]}>
          <HorizontalContainer style={{maxWidth: '1024px', marginLeft: 'auto', marginRight: 'auto'}}>
            <HorizontalItem className={style.menuItem + ' ' + ((selected === 0) ? style.selected : '')}>
              <Link to={'/' + language + '/' + getLanguageRoute('oneDaySurgery')[language]}>
                <div>
                  <div className={(selected === 0) ? [style.oneDaySurgery, style.selected].join(' ') : style.oneDaySurgery}></div>
                  <MultiLanguageContent dataKey="oneDaySurgeryh2" />
                </div>
              </Link>
            </HorizontalItem>
            <HorizontalItem className={style.menuItem + ' ' + ((selected === 1) ? style.selected : '')}>
              <Link to={'/' + language + '/' + getLanguageRoute('ambulance')[language]}>
                <div>
                  <div className={(selected === 1) ? [style.ambulance, style.selected].join(' ') : style.ambulance}></div>
                  <MultiLanguageContent dataKey="ambulanceh2" />
                </div>
              </Link>
            </HorizontalItem>
            <HorizontalItem className={style.menuItem + ' ' + ((selected === 2) ? style.selected : '')}>
              <Link to={'/' + language + '/' + getLanguageRoute('plasticSurgery')[language]}>
                <div>
                  <div className={(selected === 2) ? [style.plasticSurgery, style.selected].join(' ') : style.plasticSurgery}></div>
                  <MultiLanguageContent dataKey="plasticSurgeryh2" />
                </div>
              </Link>
            </HorizontalItem>
          </HorizontalContainer>
        </ ResponsiveView>
        <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE, RULES.SHOW_ON_TABLET_PORTRAIT]}>
          <div>
            <div className={style.menuItem + ' ' + ((selected === 0) ? style.selected : '')}>
              <Link to={'/' + language + '/' + getLanguageRoute('oneDaySurgery')[language]}>
                <div>
                  <div className={(selected === 0) ? [style.oneDaySurgery, style.selected].join(' ') : style.oneDaySurgery}></div>
                  <MultiLanguageContent dataKey="oneDaySurgeryh2" />
                </div>
              </Link>
            </div>
            <div className={style.menuItem + ' ' + ((selected === 1) ? style.selected : '')}>
              <Link to={'/' + language + '/' + getLanguageRoute('ambulance')[language]}>
                <div>
                  <div className={(selected === 1) ? [style.ambulance, style.selected].join(' ') : style.ambulance}></div>
                  <MultiLanguageContent dataKey="ambulanceh2" />
                </div>
              </Link>
            </div>
            <div className={style.menuItem + ' ' + ((selected === 2) ? style.selected : '')}>
              <Link to={'/' + language + '/' + getLanguageRoute('plasticSurgery')[language]}>
                <div>
                  <div className={(selected === 2) ? [style.plasticSurgery, style.selected].join(' ') : style.plasticSurgery}></div>
                  <MultiLanguageContent dataKey="plasticSurgeryh2" />
                </div>
              </Link>
            </div>
          </div>
        </ResponsiveView>
      </div>
    );
  }
}
