import React, { Component } from 'react';
import PropTypes from 'prop-types';

const styles = require('./BackgroundComponent.scss');

export default class BackgroundComponent extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    className: PropTypes.string
  };
  render() {
    return (
      <div className={this.props.className + ' ' + styles.backgroundImage}>
        {this.props.children}
      </div>
    );
  }
}
