import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { HorizontalContainer, HorizontalItem } from 'public/components/LayoutComponents/LayoutComponents';

import { connect } from 'react-redux';
import ResponsiveView, { RULES } from 'admin/components/ResponsiveView';

const styles = require('./SwitchContent.scss');
const arrow = require('./arrow.svg');

const style = {
  maxWidth: '1024px',
  marginLeft: 'auto',
  marginRight: 'auto'
};

@connect(state => ({
  ...state,
  editing: state.editor.editing,
}), {})
export default class SwitchContent extends Component {

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    editing: PropTypes.bool
  };

  state = {
    windowWidth: 0,
    selectedContent: 0
  };

  componentDidMount() {
    this.onResize();
    this.timeoutId = setTimeout(this.increaseContentIndex.bind(this), 10000);
    window.addEventListener('resize', this.onResize.bind(this));
  }
  componentWillUnmount() {
    clearTimeout(this.timeoutId);
    window.removeEventListener('resize', this.onResize.bind(this));
  }
  onResize() {
    this.setState({ windowWidth: window.innerWidth });
  }
  onDashClick(index) {
    this.setState({ selectedContent: index});
  }
  increaseContentIndex() {
    clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(this.increaseContentIndex.bind(this), 10000);
    const divider = (this.state.windowWidth < 980) ? this.props.children.length : parseInt(this.props.children.length / 3, 10);
    this.setState({ selectedContent: (this.state.selectedContent + 1) % divider});
  }
  decreaseContentIndex() {
    clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(this.increaseContentIndex.bind(this), 10000);
    const divider = (this.state.windowWidth < 980) ? this.props.children.length : parseInt(this.props.children.length / 3, 10);
    const selectedContent = (this.state.selectedContent === 0) ? divider : this.state.selectedContent;
    this.setState({ selectedContent: (selectedContent - 1) % divider});
  }
  render() {
    const children = this.props.children;
    const childrenCopy = [];
    Object.assign(childrenCopy, children);
    const splicedChildren = [];
    const chunkSize = 3;
    while (childrenCopy.length > 0) {
      splicedChildren.push(childrenCopy.splice(0, chunkSize));
    }
    const maxMobileWidth = 980;
    return (
      <div>
        <HorizontalContainer justifyItems="center">
          <HorizontalItem align="middle">
            {this.state.windowWidth < maxMobileWidth
              && <img onClick={this.decreaseContentIndex.bind(this)} className={[styles.rotate180, styles.arrowButtons].join(' ')} style={{width: '55px'}} src={arrow} width="55" />
            }
          </HorizontalItem>
          <HorizontalItem style={(this.state.windowWidth < maxMobileWidth) ? {width: '70%'} : {}}>
            <div>
              <ResponsiveView showOn={[RULES.SHOW_ON_DESKTOP]}>
                {
                  splicedChildren.map((splice, index) => {
                    return (
                      <HorizontalContainer
                        style={{
                          tableLayout: 'fixed',
                          ...style
                        }}
                        key={index}
                        className={((!this.props.editing) ? styles.contentItem : '') + ' ' + ((index === this.state.selectedContent) ? styles.visible : '')}>
                        {
                          splice.map((child, spliceIndex) => {
                            return <HorizontalItem key={spliceIndex} style={{textAlign: 'center'}}>{child}</HorizontalItem>;
                          })
                        }
                      </HorizontalContainer>
                    );
                  })
                }
              </ResponsiveView>
              <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE, RULES.SHOW_ON_TABLET]}>
                {
                  children.map((child, index) => {
                    return (
                      <HorizontalContainer
                        style={{marginLeft: 'auto', marginRight: 'auto'}}
                        width="auto"
                        key={index}
                        className={((!this.props.editing) ? styles.contentItem : '') + ' ' + ((index === this.state.selectedContent) ? styles.visible : '')}>
                        <HorizontalItem style={{textAlign: 'center'}}>{child}</HorizontalItem>
                      </HorizontalContainer>
                    );
                  })
                }
              </ResponsiveView>
              <HorizontalContainer style={style}>
                <HorizontalItem />
                {
                  !this.props.editing &&
                  <HorizontalItem style={{textAlign: 'center'}}>
                    <div>
                      <ResponsiveView showOn={[RULES.SHOW_ON_DESKTOP]}>
                        {
                          splicedChildren.map((child, index) => {
                            return <div key={index} onClick={this.onDashClick.bind(this, index)} className={styles.dash + ' ' + ((index === this.state.selectedContent) ? styles.selected : '')}></div>;
                          })
                        }
                      </ResponsiveView>
                      <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE, RULES.SHOW_ON_TABLET]}>
                        {
                          children.map((child, index) => {
                            return <div key={index} onClick={this.onDashClick.bind(this, index)} className={[styles.dash, styles.small].join(' ') + ' ' + ((index === this.state.selectedContent) ? styles.selected : '')}></div>;
                          })
                        }
                      </ResponsiveView>
                    </div>
                  </HorizontalItem>
                }
                <HorizontalItem>
                  <div></div>
                </HorizontalItem>
              </HorizontalContainer>
            </div>
          </HorizontalItem>
          <HorizontalItem align="middle">
            {
              (() => {
                if (this.state.windowWidth < maxMobileWidth) {
                  return <img onClick={this.increaseContentIndex.bind(this)} className={styles.arrowButtons} style={{width: '55px'}} src={arrow} width="55" />;
                }
              })()
            }
          </HorizontalItem>
        </HorizontalContainer>
      </div>
    );
  }

}
