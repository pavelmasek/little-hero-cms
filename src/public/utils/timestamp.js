export default (date) => {
  if (!date) {
    const nDate = new Date();
    return `${nDate.toISOString().replace(/[-:.T]/g, '').substring(2, 12)}`;
  }
  return `${date.toISOString().replace(/[-:.T]/g, '').substring(2, 12)}`;
};
