import React, { Component } from 'react';

import { Link } from 'react-router';
import Paper from 'material-ui/Paper';
import DescriptionIcon from 'material-ui/svg-icons/action/description';
import WallpaperIcon from 'material-ui/svg-icons/device/wallpaper';
import SettingIcon from 'material-ui/svg-icons/action/settings';
import * as Colors from 'material-ui/styles/colors';
import Divider from 'material-ui/Divider';
import style from 'admin/styles/admin.less';
import LoginStatus from 'admin/components/LoginStatus';

import translate, { setTranslation } from 'admin/utils/translate';

const menuItemStyle = {
  height: 100,
  width: 100,
  margin: 20,
  padding: 10,
  textAlign: 'center',
  display: 'inline-block',
};

export default class Root extends Component {

  constructor() {
    super();
    setTranslation();
  }

  render() {
    return (
      <div>
        <div className={style.toolbar}>
          <h3 style={{padding: '0 1em'}}>HERO CMS</h3>
          <div className={style.toolbarLoginStatus}>
            <LoginStatus />
          </div>
        </div>
        <div style={{maxWidth: '80%', marginLeft: 'auto', marginRight: 'auto', textAlign: 'center'}}>
          <h4 style={{padding: '1em'}}>HERO {translate('modules')}</h4>
          <Divider />
          <Link to="/admin/pages">
            <Paper style={menuItemStyle} zDepth={1}>
              <DescriptionIcon style={{width: 36, height: 36}} color={Colors.grey600} hoverColor={Colors.grey800} />
              <br />
              <span style={{color: Colors.grey600, fontSize: '.75em'}}>{translate('pages')}</span>
            </Paper>
          </Link>
          <Link to="/admin/media">
            <Paper style={menuItemStyle} zDepth={1}>
              <WallpaperIcon style={{width: 36, height: 36}} color={Colors.grey600} hoverColor={Colors.grey800} />
              <br />
              <span style={{color: Colors.grey600, fontSize: '.75em'}}>{translate('media')}</span>
            </Paper>
          </Link>
          <Link to="/admin/setting">
            <Paper style={menuItemStyle} zDepth={1}>
              <SettingIcon style={{width: 36, height: 36}} color={Colors.grey600} hoverColor={Colors.grey800} />
              <br />
              <span style={{color: Colors.grey600, fontSize: '.75em'}}>{translate('setting')}</span>
            </Paper>
          </Link>
          <h4 style={{padding: '1em'}}>{translate('custom')} {translate('modules')}</h4>
          <Divider />
        </div>
      </div>
    );
  }
}
