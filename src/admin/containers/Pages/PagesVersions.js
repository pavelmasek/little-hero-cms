import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { Link } from 'react-router';
import { connect } from 'react-redux';
import CircularProgress from 'material-ui/CircularProgress';
import RaisedButton from 'material-ui/RaisedButton';
import moment from 'moment';
import AddIcon from 'material-ui/svg-icons/content/add';
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh';
import BuildIcon from 'material-ui/svg-icons/action/build';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import EditIcon from 'material-ui/svg-icons/image/edit';
import Snackbar from 'material-ui/Snackbar';
import * as Colors from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import LoginStatus from 'admin/components/LoginStatus';

import translate, { getCurrentLanguage } from 'admin/utils/translate';

// import Dialog from 'material-ui/Dialog';
import * as pageBuilderActions from 'admin/redux/modules/pageBuilder';
import * as adminActions from 'admin/redux/modules/admin';
// import FlatButton from 'material-ui/FlatButton';
import style from 'admin/styles/admin.less';

import RenameBox from 'admin/components/RenameBox';

@connect(state => ({
  ...state,
  languageVersions: state.pageBuilder.languageVersions,
  gettingLanguageData: state.pageBuilder.gettingLanguageData,
  settingNewLanguage: state.pageBuilder.settingNewLanguage,
  creatingNewLanguageVersion: state.pageBuilder.creatingNewLanguageVersion,
  deletingLanguageVersion: state.pageBuilder.deletingLanguageVersion,
  renameLanguageVersion: state.pageBuilder.renameLanguageVersion,
  builded: state.admin.builded,
  building: state.admin.building
}), { ...pageBuilderActions, ...adminActions })

export default class PagesVersions extends Component {

  static propTypes = {
    getLanguageVersions: PropTypes.func,
    languageVersions: PropTypes.array,
    getLanguageVersionData: PropTypes.func,
    setNewLanguageData: PropTypes.func,
    runBuild: PropTypes.func,
    setBuildedToFalse: PropTypes.func,
    languageData: PropTypes.object,
    admin: PropTypes.object,
    building: PropTypes.bool,
    builded: PropTypes.bool,
    gettingLanguageData: PropTypes.bool,
    settingNewLanguage: PropTypes.bool,
    createNewLanguageVersion: PropTypes.func,
    deleteLanguageVersion: PropTypes.func,
    creatingNewLanguageVersion: PropTypes.bool,
    deletingLanguageVersion: PropTypes.bool,
    renameLanguageVersion: PropTypes.func
  }

  state = {
    basedOnSelectValue: '',
    versionToDelete: '',
    deleteVersionDialogOpen: false
  }

  componentWillMount() {
    moment.locale(getCurrentLanguage());
  }

  componentDidMount() {
    this.props.getLanguageVersions();
  }

  onSelectChangeHandler(ev) {
    this.props.getLanguageVersionData(ev.target.value);
  }

  onBuildVersionHandler(ev) {
    const { version } = ev.currentTarget.dataset;
    Promise.all([this.props.setNewLanguageData(version)])
      .then(() => this.props.runBuild());
  }

  onCreateNewVersionHandler() {
    Promise.all([this.props.createNewLanguageVersion(this.state.basedOnSelectValue)])
      .then(() => this.props.getLanguageVersions())
      .then(() => {
        this.setState({
          basedOnSelectValue: ''
        });
      });
  }

  onSelectChangeBaseOnHandler(ev) {
    this.setState({
      basedOnSelectValue: ev.currentTarget.value
    });
  }

  onVersionButttonDeleteClick(ev) {
    const { version } = ev.currentTarget.dataset;
    this.setState({
      versionToDelete: version,
      deleteVersionDialogOpen: true
    });
  }

  onDeleteVersionHandler() {
    Promise.all([this.props.deleteLanguageVersion(this.state.versionToDelete)])
      .then(() => {
        this.props.getLanguageVersions();
        this.setState({
          versionToDelete: '',
          deleteVersionDialogOpen: false
        });
      });
  }

  onCloseDialog() {
    this.setState({
      deleteVersionDialogOpen: false
    });
  }

  openNewTab() {
    window.open('/', '_blank');
  }

  render() {
    const { languageVersions } = this.props;
    const langVers = languageVersions.slice();
    langVers.reverse();
    return (
      <div>
        <div className={style.toolbar}>
          <h3 style={{padding: '0 1em', margin: 0}}>
            <Link style={{color: 'rgba(0,0,0,0.5)'}} to="/admin">HERO CMS</Link> / {translate('pages').toUpperCase()}
          </h3>
          <div className={style.toolbarButtons}>
            <RaisedButton
              style={{margin: '0 .5em'}}
              onClick={this.props.getLanguageVersions}
              label={translate('refresh')}
              icon={<RefreshIcon />} />
            <RaisedButton
              style={{margin: '0 .5em'}}
              onClick={this.onCreateNewVersionHandler.bind(this)}
              label={translate('createNewVersion')}
              disabled={this.state.basedOnSelectValue === ''}
              icon={<AddIcon />} />
            <span style={{margin: '0 .5em'}}>
              {translate('basedOn')}
            </span>
            <select
              value={this.state.basedOnSelectValue}
              onChange={this.onSelectChangeBaseOnHandler.bind(this)}
              className={style.select}
              defaultValue={`-- ${translate('selectVersion')} --`}>
              <option disabled value>{`-- ${translate('selectVersion')} --`}</option>
              <option value="CommonLanguageData.json">{translate('currentVersion')}</option>
              {
                langVers.map((item, index) => {
                  const splittedFileName = item.Key.replace('versions/', '').replace('.json', '').split('@');
                  const versionId = splittedFileName[0];
                  return (<option key={index} value={item.Key}>{versionId}</option>);
                })
              }
            </select>
          </div>
          <div className={style.toolbarLoginStatus}>
            <LoginStatus />
          </div>
        </div>
        <div style={{width: '80%', marginLeft: 'auto', marginRight: 'auto'}}>
          <h4 style={{marginLeft: 50, padding: '1em'}}>{translate('versions').toUpperCase()}</h4>
        </div>
        {
          this.props.gettingLanguageData &&
          <div style={{textAlign: 'center', paddingTop: '10vmin'}}>
            <CircularProgress size={2} color={Colors.grey600} />
            <br />
            <span>{translate('loadingVersions')}...</span>
          </div>
        }
        {
          !this.props.gettingLanguageData &&
          <table style={{ width: '80%', marginLeft: 'auto', marginRight: 'auto', textAlign: 'center'}}>
            <thead>
              <tr style={{textTransform: 'capitalize'}}>
                <th>{translate('created')}</th>
                <th>{translate('lastModified')}</th>
                <th>{translate('name')}</th>
              </tr>
            </thead>
            <tbody>
              {
                langVers.map((item, index) => {
                  const splitted = item.Key.split('@');
                  const versionId = splitted[0].replace('versions/', '');
                  const timeStamp = splitted[splitted.length - 1].replace('.json', '');
                  return (
                    <tr data-value={item.key} key={index}>
                      <td style={{padding: '.5em .5em'}}>{moment(timeStamp, 'YYYYMMDDhhmmss').fromNow()}</td>
                      <td style={{padding: '.5em .5em'}}>{moment.utc(item.LastModified).fromNow()}</td>
                      <td style={{padding: '.5em .5em'}}>
                        <RenameBox
                          onConfirm={this.props.renameLanguageVersion}
                          afterConfirm={this.props.getLanguageVersions}
                          text={item.Key} />
                      </td>
                      <td
                        data-version={item.Key}
                        onClick={this.onBuildVersionHandler.bind(this)}
                        style={{padding: '.5em .5em'}}>
                        <RaisedButton
                          label={translate('build')}
                          disabled={this.props.building}
                          icon={<BuildIcon />} />
                      </td>
                      <td style={{padding: '.5em .5em'}}>
                        <Link to={`/admin/pages/${versionId}@${timeStamp}`}>
                          <RaisedButton
                            label={translate('edit')}
                            icon={<EditIcon />} />
                        </Link>
                      </td>
                      <td
                        data-version={item.Key}
                        onClick={this.onVersionButttonDeleteClick.bind(this)}
                        style={{padding: '.5em .5em'}}>
                        <RaisedButton
                          label={translate('remove')}
                          icon={<DeleteIcon />} />
                      </td>
                    </tr>);
                })
              }
            </tbody>
          </table>
        }
        <Snackbar
          open={this.props.building || this.props.settingNewLanguage}
          message={translate('websiteBuildIsRunning')}
          onRequestClose={() => {}}
        />
        <Snackbar
          open={!(this.props.building || this.props.settingNewLanguage) && this.props.builded}
          message={translate('showNewVersionOfWebsite')}
          action={translate('show')}
          onClick={this.openNewTab}
          autoHideDuration={10000}
          onRequestClose={() => { this.props.setBuildedToFalse(); }}
        />
        <Snackbar
          open={this.props.deletingLanguageVersion}
          message={translate('deletingLanguageVersion')}
          onClick={this.openNewTab}
          autoHideDuration={10000}
          onRequestClose={() => { this.props.setBuildedToFalse(); }}
        />
        <Snackbar
          open={this.props.creatingNewLanguageVersion}
          message={`${translate('creatingNewVersionBasedOn')} ${this.state.basedOnSelectValue}...`}
          onClick={this.openNewTab}
          autoHideDuration={10000}
          onRequestClose={() => { this.props.setBuildedToFalse(); }}
        />
        <Dialog
            title={translate('deleteVersion')}
            actions={[
              <FlatButton
                label={translate('cancel')}
                secondary
                onClick={this.onCloseDialog.bind(this)}
              />,
              <FlatButton
                label={translate('delete')}
                primary
                keyboardFocused
                onClick={this.onDeleteVersionHandler.bind(this)}
              />
            ]}
            modal
            open={this.state.deleteVersionDialogOpen}
            onRequestClose={this.onCloseDialog.bind(this)}
          >
            {translate('areYouSureYouWantToDeleteVersion')}.
          </Dialog>
      </div>
    );
  }
}
