import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { Link } from 'react-router';
import { connect } from 'react-redux';

import containers from 'public/containers/cmsContainers';

import Paper from 'material-ui/Paper';
import DescriptionIcon from 'material-ui/svg-icons/action/description';
import * as Colors from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import BuildIcon from 'material-ui/svg-icons/action/build';
import Snackbar from 'material-ui/Snackbar';
import LoginStatus from 'admin/components/LoginStatus';
import Divider from 'material-ui/Divider';

import translate from 'admin/utils/translate';

import * as pageBuilderActions from 'admin/redux/modules/pageBuilder';
import * as adminActions from 'admin/redux/modules/admin';

import style from 'admin/styles/admin.less';

const menuItemStyle = {
  height: 100,
  width: 100,
  margin: 20,
  padding: 10,
  textAlign: 'center',
  display: 'inline-block',
};

const floatLeft = {
  float: 'left'
};

@connect(state => ({
  ...state,
  settingNewLanguage: state.pageBuilder.settingNewLanguage,
  building: state.admin.building,
  builded: state.admin.builded,
  languageVersions: state.pageBuilder.languageVersions
}), { ...pageBuilderActions, ...adminActions })

export default class Pages extends Component {

  static propTypes = {
    getLanguageVersions: PropTypes.func,
    languageVersions: PropTypes.array,
    getLanguageVersionData: PropTypes.func,
    setNewLanguageData: PropTypes.func,
    runBuild: PropTypes.func,
    setBuildedToFalse: PropTypes.func,
    params: PropTypes.object,
    building: PropTypes.bool,
    builded: PropTypes.bool,
    settingNewLanguage: PropTypes.bool
  }

  componentDidMount() {
    const { id } = this.props.params;
    this.props.getLanguageVersionData(`versions/${id}.json`);
  }

  onSelectChangeHandler(ev) {
    console.log(ev.target.value);
    this.props.getLanguageVersionData(ev.target.value);
  }

  onBuildHandler() {
    const { id } = this.props.params;
    Promise.all([this.props.setNewLanguageData(`versions/${id}.json`)])
      .then(() => this.props.runBuild());
  }

  openNewTab() {
    window.open('/', '_blank');
  }

  render() {
    const { id } = this.props.params;
    return (
      <div>
        <div className={style.toolbar}>
          <h3 style={{padding: '0 1em'}}>
            <Link style={{color: 'rgba(0,0,0,0.5)'}} to="/admin">HERO CMS</Link>
            &nbsp;/&nbsp;
            <Link style={{color: 'rgba(0,0,0,0.5)'}} to="/admin/pages">{translate('pages').toUpperCase()}</Link>
            &nbsp;/&nbsp;
            {id}
          </h3>
          <div className={style.toolbarButtons}>
            <RaisedButton
              label={translate('build')}
              disabled={this.props.building || this.props.settingNewLanguage}
              onClick={this.onBuildHandler.bind(this)}
              icon={<BuildIcon style={{width: 20, height: 20}} />} />
          </div>
          <div className={style.toolbarLoginStatus}>
            <LoginStatus />
          </div>
        </div>
        <div style={{maxWidth: '80%', marginLeft: 'auto', marginRight: 'auto', textAlign: 'center'}}>
          <h4 style={{ padding: '1em' }}>{translate('pages')}</h4>
          <Divider />
          {
            containers.map((item, key) => {
              return (
                <Link key={key} style={floatLeft} to={`/admin/pages/${id}/pageBuilder/${item.path}`}>
                  <Paper style={menuItemStyle} zDepth={1}>
                    <DescriptionIcon style={{width: 48, height: 48}} color={Colors.grey500} hoverColor={Colors.grey600} />
                    <br />
                    <span style={{color: Colors.grey600, fontSize: '.75em'}}>{item.name}</span>
                  </Paper>
                </Link>
              );
            })
          }
          <div style={{clear: 'both'}} />
          <h4 style={{ padding: '1em' }}>{`${translate('custom')} ${translate('components')}`}</h4>
          <Divider />
          <Snackbar
            open={this.props.building || this.props.settingNewLanguage}
            message={translate('websiteBuildIsRunning')}
            onRequestClose={() => {}}
          />
          <Snackbar
            open={!(this.props.building || this.props.settingNewLanguage) && this.props.builded}
            message={translate('showNewVersionOfWebsite')}
            action={translate('show')}
            onClick={this.openNewTab}
            autoHideDuration={10000}
            onRequestClose={() => { this.props.setBuildedToFalse(); }}
          />
        </div>
      </div>
    );
  }
}
