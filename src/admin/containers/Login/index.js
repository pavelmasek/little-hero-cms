import React, { Component } from 'react';
import PropTypes from 'prop-types';


// import Router from 'react-router';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import * as Colors from 'material-ui/styles/colors';
import style from 'admin/styles/admin.less';
import connect from 'react-redux/lib/components/connect';
import * as authActions from 'admin/redux/modules/auth';
import translate from 'admin/utils/translate';

import IdentityIcon from 'material-ui/svg-icons/action/perm-identity';
import LockIcon from 'material-ui/svg-icons/action/lock-outline';

const underlineFocusStyle = {
  borderColor: Colors.grey800
};

const floatingLabelStyle = {
  color: Colors.grey500
};

@connect(state => ({
  ...state,
  user: state.auth.user
}), {...authActions})
export default class LoginPage extends Component {

  static propTypes = {
    login: PropTypes.func,
    user: PropTypes.object
  }

  state = {
    login: '',
    password: ''
  }

  onChangeHandler(ev) {
    const newState = {};
    newState[ev.currentTarget.getAttribute('name')] = ev.currentTarget.value;
    this.setState(newState);
  }

  onSubmitHandler() {
    Promise.all([this.props.login(this.state.login, this.state.password)])
      .then((result) => {
        console.log(result);
        if (localStorage) {
          localStorage.setItem('jwtToken', this.props.user.token);
          const date = new Date();
          date.setDate(date.getDate() + 7);
          document.cookie = `jwtToken=${this.props.user.token}; Expires=${date.toGMTString()}`;
        }
        if (window) {
          window.location.href = '/admin';
        }
      });
  }

  render() {
    return (
      <div className={style.coolFont}>
        <div style={{marginLeft: 'auto', marginRight: 'auto', marginTop: '20vmin', width: '300px'}}>
          <Paper style={{textAlign: 'center'}} zDepth={1}>
            <div style={{fontFamily: 'coolFont', padding: '0.75em'}}>
              <h3>HERO CMS</h3>
              <h5>website administration</h5>
            </div>
            <Divider />
            <div style={{padding: '.75em'}}>
              <TextField
                id="login"
                name="login"
                onChange={this.onChangeHandler.bind(this)}
                value={this.state.login}
                floatingLabelText={
                  <div>
                    <IdentityIcon color={Colors.grey400} style={{float: 'left', width: 20, height: 20}} />
                    <span style={{float: 'left'}}>Login</span>
                  </div>
                }
                floatingLabelStyle={floatingLabelStyle}
                underlineFocusStyle={underlineFocusStyle}
                />
              <TextField
                id="password"
                name="password"
                onChange={this.onChangeHandler.bind(this)}
                value={this.state.password}
                floatingLabelText={
                  <div>
                    <LockIcon color={Colors.grey400} style={{float: 'left', width: 20, height: 20}} />
                    <span style={{float: 'left'}}>{translate('password')}</span>
                  </div>
                }
                floatingLabelStyle={floatingLabelStyle}
                underlineFocusStyle={underlineFocusStyle}
                type="password"
                />
              <RaisedButton
                onClick={this.onSubmitHandler.bind(this)}
                label="LOGIN" />
            </div>
          </Paper>
        </div>
      </div>
    );
  }
}
