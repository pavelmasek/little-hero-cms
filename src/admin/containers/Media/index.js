import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { Link } from 'react-router';
import { connect } from 'react-redux';
import CircularProgress from 'material-ui/CircularProgress';
import * as Colors from 'material-ui/styles/colors';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import FileUploadIcon from 'material-ui/svg-icons/file/file-upload';
import AddIcon from 'material-ui/svg-icons/content/add';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh';
import SearchIcon from 'material-ui/svg-icons/action/search';
import translate from 'admin/utils/translate';
import LoginStatus from 'admin/components/LoginStatus';
import TextField from 'material-ui/TextField';
import Highlight from 'react-highlighter';


import * as mediaActions from 'admin/redux/modules/media';

import style from 'admin/styles/admin.less';


@connect(state => ({
  ...state,
  ...state.media
}), { ...mediaActions })
export default class MediaContainer extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.node)
    ]),
    deleteMediaFile: PropTypes.func,
    deletingMediaFile: PropTypes.bool,
    getAllMediaFiles: PropTypes.func,
    gettingMediaFiles: PropTypes.bool,
    mediaFiles: PropTypes.array,
    uploadMediaFile: PropTypes.func,
    uploadingMediaFile: PropTypes.bool
  }

  state = {
    fileName: '',
    keyReuploaded: '',
    searchValue: '',
    imageSizes: {}
  }

  componentDidMount() {
    this.props.getAllMediaFiles();
  }

  handleFileInputChange(ev) {
    const { files } = ev.target;
    const file = files[0];
    if (file) {
      const fileReader = new FileReader();
      fileReader.onload = () => {
        Promise.all([this.props.uploadMediaFile({
          fileName: this.state.fileName || file.name,
          data: fileReader.result
        })])
        .then(() => this.props.getAllMediaFiles())
        .then(() => this.setState({fileName: ''}));
      };
      fileReader.readAsBinaryString(file);
    }
    console.log(ev);
  }

  handleDeleteMediaFile(fileName) {
    Promise.all([this.props.deleteMediaFile(fileName)])
      .then(() => this.props.getAllMediaFiles());
  }

  handleFileUpload() {
    this.setState({ fileName: ''});
    this.fileInput.click();
  }

  handleSearchValueChange(ev) {
    const { value } = ev.currentTarget;
    this.setState({searchValue: value});
  }

  handleImageOnLoad(ev) {
    const { naturalHeight, naturalWidth } = ev.currentTarget;
    const { imageName } = ev.currentTarget.dataset;
    const image = {};
    image[imageName] = {
      naturalHeight,
      naturalWidth
    };
    this.setState({
      imageSizes: {
        ...this.state.imageSizes,
        ...image
      }
    });
  }

  render() {
    const { searchValue } = this.state;
    const { mediaFiles } = this.props;
    const filteredMediaFiles = mediaFiles && mediaFiles.filter((item) => {
      if (item.Key === 'images/') {
        return false;
      }
      if (item.Key.replace('images/', '').indexOf(searchValue) > -1) {
        return true;
      }
    });
    return (
      <div>
        <div className={style.toolbar}>
          <h3 style={{padding: '0 1em'}}><Link style={{color: 'rgba(0,0,0,0.5)'}} to="/admin">HERO CMS</Link> / {translate('media').toUpperCase()}</h3>
          <div style={{display: 'inline-block', position: 'absolute', left: '0', right: '0', top: 0, marginLeft: 'auto', marginRight: 'auto', width: 256, padding: '4px 0'}}>
            <TextField
              value={searchValue}
              onChange={::this.handleSearchValueChange}
              hintText={
                <div>
                  <SearchIcon color={Colors.grey400} style={{float: 'left'}}/>
                  <span style={{float: 'left'}}>{translate('search')}</span>
                </div>
              }
              underlineStyle={{borderColor: Colors.grey400}} />
          </div>
          <div className={style.toolbarButtons}>
            <RaisedButton
              style={{margin: '0 .5em'}}
              onClick={this.props.getAllMediaFiles}
              label={translate('refresh')}
              icon={<RefreshIcon />} />
          </div>
          <div className={style.toolbarLoginStatus}>
            <LoginStatus />
          </div>
        </div>
        {
          this.props.gettingMediaFiles &&
          <div style={{ textAlign: 'center', paddingTop: '10vmin' }}>
            <CircularProgress size={2} color={Colors.grey600} />
            <br />
            <span>{translate('loadingMediaFiles')}...</span>
          </div>
        }
        <div style={{ width: '80%', marginLeft: 'auto', marginRight: 'auto' }}>
          {
            !this.props.gettingMediaFiles && !this.props.deletingMediaFile && !this.props.uploadingMediaFile &&
            <Paper
              onClick={::this.handleFileUpload}
              zDepth={1}
              style={{
                width: 150,
                height: 200,
                textAlign: 'center',
                float: 'left',
                margin: '1em',
                position: 'relative',
                fontSize: '75%',
                cursor: 'pointer',
                backgroundColor: 'rgba(222, 222, 222, 0.75)'}}>
                <table style={{width: '100%', height: '100%'}}>
                  <tbody>
                    <tr>
                      <td>
                        <AddIcon
                          style={{ width: 100, height: 100 }}
                          color={Colors.grey500} />
                        <div
                          style={{color: Colors.grey500}}>
                          {translate('upload')} {translate('files')}
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <input
                  ref={(fileInput) => this.fileInput = fileInput}
                  type="file"
                  onChange={::this.handleFileInputChange}
                  style={{visibility: 'hidden'}} />
            </Paper>
          }
          {
            filteredMediaFiles && filteredMediaFiles.map((item, key) => {
              if (item.Key === 'images/') {
                return '';
              }
              const fileName = item.Key.replace('images/', '');
              const imageSize = this.state.imageSizes[item.Key];
              return (
                <Paper
                  zDepth={1}
                  key={key}
                  style={{
                    width: 150,
                    height: 200,
                    textAlign: 'center',
                    float: 'left',
                    margin: '1em',
                    position: 'relative',
                    backgroundColor: 'rgba(222, 222, 222, 0.75)'}}>
                  {
                    imageSize &&
                    <div style={{ position: 'absolute', top: 0, right: 0, padding: 5, fontSize: '65%', color: 'rgba(0,0,0, 0.5)'}}>
                      {imageSize.naturalWidth} x {imageSize.naturalHeight}
                    </div>
                  }
                  <div style={{position: 'absolute', top: 0, left: 0, right: 0}}>
                    <img data-image-name={item.Key} onLoad={::this.handleImageOnLoad} src={(item.Key === this.state.keyReuploaded) ? `${item.Link}?${new Date().getTime()}` : item.Link} style={{padding: '1em', maxWidth: '100px', maxHeight: '100px'}} />
                  </div>
                  <div style={{position: 'absolute', bottom: 0, left: 0, right: 0, fontSize: '75%', marginBottom: '48px'}}>
                    <Highlight
                      search={searchValue}
                      matchStyle={{backgroundColor: Colors.yellow300}}>
                      {fileName}
                    </Highlight>
                  </div>
                  <div style={{position: 'absolute', bottom: 0, left: 0, right: 0}}>
                    <IconButton
                      tooltip={translate('reupload')}
                      tooltipPosition="top-center"
                      onClick={() => {
                        this.setState({fileName: fileName, keyReuploaded: item.Key}, () => {
                          this.fileInput.click();
                        });
                      }}>
                      <FileUploadIcon color={Colors.grey600} hoverColor={Colors.grey800} />
                    </IconButton>
                    <IconButton
                      tooltip={translate('remove')}
                      tooltipPosition="top-center"
                      onClick={this.handleDeleteMediaFile.bind(this, fileName)}>
                      <DeleteIcon color={Colors.grey600} hoverColor={Colors.grey800} />
                    </IconButton>
                  </div>
                </Paper>
              );
            })
          }
          {
            !this.props.gettingMediaFiles && !this.props.mediaFiles && !this.props.deletingMediaFile && !this.props.uploadingMediaFile &&
            <h1 style={{ textAlign: 'center', paddingTop: '10vmin', color: Colors.grey400 }}>Nothing found here...</h1>
          }
          <div style={{clear: 'both'}} />
        </div>
      </div>
    );
  }
}
