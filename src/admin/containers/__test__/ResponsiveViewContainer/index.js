import React, { Component } from 'react';
import ResponsiveView, { RULES } from 'admin/components/ResponsiveView';

export default class ResponsiveViewContainer extends Component {
  render() {
    return (
      <div>
        <ResponsiveView showOn={[RULES.SHOW_ON_DESKTOP, RULES.SHOW_ON_MOBILE]}>Desktop and Mobile</ResponsiveView>
        <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE_PORTRAIT]}>mobile portrait</ResponsiveView>
        <ResponsiveView showOn={[RULES.SHOW_ON_MOBILE_LANDSCAPE]}>mobile landscape</ResponsiveView>
        <ResponsiveView showOn={[RULES.SHOW_ON_TABLET_PORTRAIT]}>tablet portrait</ResponsiveView>
        <ResponsiveView showOn={[RULES.SHOW_ON_TABLET_LANDSCAPE]}>tablet landscape</ResponsiveView>
      </div>
    );
  }
}
