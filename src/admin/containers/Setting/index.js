import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { Link } from 'react-router';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import * as Colors from 'material-ui/styles/colors';
import { connect } from 'react-redux';
import * as authActions from 'admin/redux/modules/auth';
import LoginStatus from 'admin/components/LoginStatus';
import Version from 'admin/components/Version';

import translate, { getLanguages, getCurrentLanguage, setLanguageOfTranslation } from 'admin/utils/translate';

import style from 'admin/styles/admin.less';
@connect(state => ({
  ...state,
  ...state.auth
}), { ...authActions })
export default class Setting extends Component {

  static propTypes = {
    changePassword: PropTypes.func,
    user: PropTypes.object
  }

  state = {
    error: false,
    successfullyChanged: false
  }

  handleLanguageChange(ev) {
    const { value } = ev.currentTarget;
    setLanguageOfTranslation(value);
    this.forceUpdate();
  }

  handleChangePassword() {
    if (this.passwordInput.getValue() === this.verifyPasswordInput.getValue()) {
      this.setState({error: false, successfullyChanged: true});
      this.props.changePassword(this.passwordInput.getValue());
      this.passwordInput.input.value = '';
      this.verifyPasswordInput.input.value = '';
      setTimeout(() => this.setState({successfullyChanged: false}), 10000);
    } else {
      this.setState({error: true, successfullyChanged: false});
    }
  }

  render() {
    return (
      <div>
        <Version />
        <div className={style.toolbar}>
          <h3 style={{padding: '0 1em'}}><Link style={{color: 'rgba(0,0,0,0.5)'}} to="/admin">HERO CMS</Link> / {translate('setting').toUpperCase()}</h3>
          <div className={style.toolbarButtons}>
          </div>
          <div className={style.toolbarLoginStatus}>
            <LoginStatus />
          </div>
        </div>
        <div style={{ width: '80%', marginLeft: 'auto', marginRight: 'auto', paddingTop: '1em' }}>
          {translate('languageSetting')}
          <Divider style={{marginBottom: '1em'}} />
          <div>
            {translate('language').toUpperCase()}
            <select
              className={style.select}
              defaultValue={getCurrentLanguage()}
              onChange={::this.handleLanguageChange}>
              {
                getLanguages().map((item, key) => {
                  return (
                    <option key={key} value={item}>{item}</option>
                  );
                })
              }
            </select>
          </div>
          <br/>
          <br/>
          <br/>
          {translate('changePassword')}
          <Divider />
          <TextField
            ref={(input) => this.passwordInput = input}
            hintText={translate('password')}
            floatingLabelText={translate('password')}
            type="password"
          />
          <br/>
          <TextField
            ref={(input) => this.verifyPasswordInput = input}
            hintText={translate('verifyPassword')}
            floatingLabelText={translate('verifyPassword')}
            type="password"
          />
          <br/>
          {
            this.state.error &&
            <span style={{color: Colors.red500}}>{translate('passwordsDoNotMatch')}</span>
          }
          {
            this.state.successfullyChanged &&
            <span style={{color: Colors.green500}}>Successfully changed password!</span>
          }
          <br />
          <RaisedButton label={translate('changePassword')} onClick={::this.handleChangePassword}/>
        </div>
      </div>
    );
  }
}
