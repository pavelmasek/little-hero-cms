import React, { Component } from 'react';
import PropTypes from 'prop-types';


import TreeView from './TreeView';
import { connect } from 'react-redux';

import * as editorActions from 'admin/redux/modules/editor';
import * as pageBuilderActions from 'admin/redux/modules/pageBuilder';
import * as languageActions from 'admin/redux/modules/language';
import * as adminActions from 'admin/redux/modules/admin';
import * as seoActions from 'admin/redux/modules/seo';

import { Link } from 'react-router';
import Dialog from 'material-ui/Dialog';
import Toggle from 'material-ui/Toggle';
import SaveIcon from 'material-ui/svg-icons/content/save';
import TrendingUpIcon from 'material-ui/svg-icons/action/trending-up';
import * as Colors from 'material-ui/styles/colors';
import Snackbar from 'material-ui/Snackbar';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import CloseIcon from 'material-ui/svg-icons/navigation/close';

import translate from 'admin/utils/translate';

import ResponsivePanelView from 'admin/views/ResponsivePanel';

import style from 'admin/styles/admin.less';

function findChildren(child, index) {
  const id = this && this.parentIndex !== undefined ? this.parentIndex + '.' + index : index + '';
  return {
    ...child,
    id: id,
    name: child.component ? child.component.displayName : 'HTMLContent' + (index + 1),
    children: child.children ? child.children.map(findChildren.bind({ parentIndex: id })) : undefined
  };
}

function parseSettingToTreeViewData(arr) {
  return arr.map(findChildren);
}

const getEmptyMetaArray = () => {
  return [
    {
      name: 'title',
      content: ''
    }, {
      name: 'description',
      content: ''
    }, {
      name: 'keywords',
      content: ''
    }
  ];
};

@connect(state => ({
  ...state,
  properties: state.editor.properties,
  setting: state.pageBuilder.setting,
  languageData: state.pageBuilder.languageData,
  editor: state.editor,
  languages: state.language.languages,
  language: state.language.language,
  savingLanguageData: state.pageBuilder.savingLanguageData,
  seoDataKey: state.seo.seoDataKey
}), {...editorActions, ...pageBuilderActions, ...languageActions, ...adminActions, ...seoActions})

export default class PageBuilder extends Component {

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.node)
    ]),
    editor: PropTypes.object,
    editPropertiesProps: PropTypes.func,
    language: PropTypes.string,
    languageData: PropTypes.object,
    languages: PropTypes.array,
    params: PropTypes.object,
    properties: PropTypes.object,
    restoreLanguageData: PropTypes.func,
    route: PropTypes.object,
    routes: PropTypes.array,
    runBuild: PropTypes.func,
    savingLanguageData: PropTypes.bool,
    saveLanguageData: PropTypes.func,
    setLanguage: PropTypes.func,
    setLanguageData: PropTypes.func,
    seoDataKey: PropTypes.string,
    setNewLanguageData: PropTypes.func,
    setting: PropTypes.array,
    setPageBuilderSetting: PropTypes.func,
    switchEditing: PropTypes.func,
    updateLanguageData: PropTypes.func,
    updatePageBuilderSetting: PropTypes.func,
    updateSettingData: PropTypes.func
  }

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    savedLanguageData: this.props.languageData,
    showAlertUnfinishedWork: false,
    showSEODialog: false,
    showSaveDialog: false,
    leavingRoute: {},
    device: 'desktop',
    orientation: 'landscape',
    metaTags: {}
  }

  componentDidMount() {
    const { route } = this.props;
    const { router } = this.context;

    router.setRouteLeaveHook(route, (leavingRoute) => {
      // ##this disable routing inside editing document
      if (leavingRoute.pathname.indexOf('/admin')) {
        return false;
      }
      // ##
      if (this.props.languageData !== this.state.savedLanguageData) {
        if (this.state.showAlertUnfinishedWork) {
          console.log('true');
        } else {
          this.setState({
            showAlertUnfinishedWork: true,
            leavingRoute: leavingRoute
          });
        }
        return false;
      }
      return true;
    });
  }

  onInputChange(ev) {
    const newProps = {
      [ev.target.dataset.key]: ev.target.value
    };
    this.props.editPropertiesProps({
      ...this.props.properties.props,
      ...newProps
    });
    this.props.updatePageBuilderSetting({ ...this.props.properties, props: newProps });
  }

  onMetaInputChangeHandler(ev) {
    const { index, key } = ev.currentTarget.dataset;
    const { value } = ev.currentTarget;
    const { language } = this.props;

    const originValue = this.state.metaTags;
    const originLanguageValue = originValue[language];
    originLanguageValue[index][key] = value;
    const newLanguageValue = {};
    newLanguageValue[language] = originLanguageValue;
    this.setState({
      metaTags: {
        ...originValue,
        ...newLanguageValue
      }
    });
  }

  setupStateMetaData() {
    const { metaTags } = this.state;
    const { language } = this.props;

    if (!metaTags[language]) {
      const meta = metaTags;
      const newEmptyMeta = {};
      const copyEmtyMeta = getEmptyMetaArray();
      newEmptyMeta[language] = copyEmtyMeta;
      this.setState({
        metaTags: {
          ...meta,
          ...newEmptyMeta
        }
      });
    }
  }

  handleSaveLanguageData() {
    const { id } = this.props.params;
    return Promise.all([this.props.saveLanguageData(this.props.languageData, id)])
      .then(() => {
        this.setState({
          savedLanguageData: this.props.languageData
        });
      });
  }

  handleLeave() {
    const { router } = this.context;

    const p1 = new Promise((resolve) => {
      this.setState({
        showAlertUnfinishedWork: false,
        savedLanguageData: this.props.languageData
      });
      resolve();
    });

    Promise.all([p1]).then(() => {
      router.push(this.state.leavingRoute.pathname);
    });
  }

  handleRestoreLanguageData() {
    this.props.restoreLanguageData();
  }

  handleLanguageChange(ev) {
    Promise.all([this.props.setLanguage(ev.target.value)])
      .then(() => this.setupStateMetaData());
  }

  handleSaveAndLeave() {
    Promise.all([this.handleSaveLanguageData()])
      .then(() => this.handleLeave());
  }

  handlePublishAndLeave() {
    const { id } = this.props.params;
    Promise.all([this.handleSaveLanguageData()])
      .then(() => this.props.setNewLanguageData(`versions/${id}.json`))
      .then(() => this.props.runBuild())
      .then(() => this.handleLeave());
  }


  switchEditorState() {
    this.props.switchEditing();
  }

  handlePublishAndCloseSaveDialog() {
    const { id } = this.props.params;
    Promise.all([this.handleSaveLanguageData()])
      .then(() => this.props.setNewLanguageData(`versions/${id}.json`))
      .then(() => this.props.runBuild())
      .then(() => this.handleCloseSaveDialog());
  }

  handleSaveAndCloseSaveDialog() {
    Promise.all([this.handleSaveLanguageData()])
      .then(() => this.handleCloseSaveDialog());
  }

  handleCloseSaveDialog() {
    this.setState({showSaveDialog: false});
  }

  render() {
    const { id } = this.props.params;
    const { metaTags, showSEODialog } = this.state;
    const { language } = this.props;

    return (
      <div>
        <div ref="toolbar" className={style.toolbar} style={{position: 'fixed', zIndex: 9999, width: '100%'}}>
          <h3 style={{padding: '0 1em'}}>
            <Link style={{color: 'rgba(0,0,0,0.5)'}} to="/admin">HERO CMS</Link>
            &nbsp;/&nbsp;
            <Link style={{color: 'rgba(0,0,0,0.5)'}} to="/admin/pages">{translate('pages').toUpperCase()}</Link>
            &nbsp;/&nbsp;
            <Link style={{color: 'rgba(0,0,0,0.5)'}} to={`/admin/pages/${id}`}>{id}</Link>
            &nbsp;/&nbsp;
            PAGE BUILDER
          </h3>

          <div className={style.toolbarButtons}>
            <div style={{
              display: 'inline-block',
              textAlign: 'center',
              fontSize: '.85em',
              paddingLeft: '1.5em',
              paddingRight: '1.5em',
              float: 'left' }}>
              <div
                onClick={() => {
                  const { languageData, seoDataKey } = this.props;
                  const seoData = languageData[seoDataKey];
                  if (seoData) {
                    this.setState({
                      metaTags: seoData
                    }, () => {
                      this.setState({showSEODialog: true});
                    });
                  } else {
                    this.setupStateMetaData();
                    this.setState({showSEODialog: true});
                  }
                }}
                style={{display: 'inline-block', paddingLeft: '.5em', paddingRight: '.5em', borderRight: '1px solid black'}}>
                seo<br />
                <TrendingUpIcon
                  color={Colors.grey800}
                  hoverColor={Colors.grey900}
                  style={{width: 20, height: 20, cursor: 'pointer', paddingLeft: '.5em', paddingRight: '.5em'}} />
              </div>
              <ResponsivePanelView />
              <div
                onClick={() => this.setState({showSaveDialog: true})}
                style={{display: 'inline-block', paddingLeft: '.5em', paddingRight: '.5em', borderRight: '1px solid black', cursor: 'pointer'}}>
                {translate('save')}<br />
                <SaveIcon
                  color={Colors.grey800}
                  hoverColor={Colors.grey900}
                  style={{width: 20, height: 20, cursor: 'pointer', paddingLeft: '.5em', paddingRight: '.5em'}} />
              </div>

            </div>
            <div style={{display: 'inline-block', float: 'left'}}>
              {
                false &&
                <div>
                  <span>switch to page</span>
                  <select className={style.select}>
                    <option>HomePage</option>
                    <option>Jednodenni chirurgie</option>
                    <option>Ambulance</option>
                  </select>
                </div>
              }
              <select
                onChange={this.handleLanguageChange.bind(this)}
                className={style.select}
                value={this.props.language} >
                {this.props.languages.map((lang, index) => {
                  return (
                    <option key={index}>{lang}</option>
                  );
                })}
              </select>
              <div
                style={{display: 'inline-block', paddingLeft: '.5em', paddingRight: '.5em', float: 'right'}}>
                {translate('editing')} {translate('mode')}<br />
                <Toggle
                  toggled={this.props.editor.editing}
                  onToggle={this.switchEditorState.bind(this)} />
              </div>
              <div style={{clear: 'both'}} />
            </div>
          </div>
          <Snackbar
            open={this.props.savingLanguageData}
            message={`Version ${id} is saving...`}
            onRequestClose={() => {}} />
        </div>
        <div ref="toolbarSubstitude" style={(this.refs && this.refs.toolbar) ? { height: `${this.refs.toolbar.offsetHeight}px` } : {}} />
        <div>
        {
          false && <div style={{width: '15%', minHeight: '90vmin', backgroundColor: 'white', float: 'left'}}>
            <div style={{fontSize: '1.25em', minHeight: '45vmin', maxHeight: '45vmin'}}>
              <div style={{textTransform: 'uppercase', fontWeight: 'bold', textAlign: 'center', paddingTop: '10px', paddingBottom: '10px'}}>layout tree</div>
              <TreeView treeViewData={parseSettingToTreeViewData(this.props.setting)} />
            </div>
            <div style={{fontSize: '1.25em', minHeight: '45vmin', maxHeight: '45vmin'}}>
              <div style={{textTransform: 'uppercase', fontWeight: 'bold', textAlign: 'center'}}>properties</div>
              {
                this.props.properties && this.props.properties.props && Object.keys(this.props.properties.props).map((key, index) => {
                  return (
                    <div style={{fontSize: '0.85em'}} key={index}>
                      <div style={{float: 'left', width: '35%', padding: '10px'}}>{key}</div>
                      <div style={{float: 'left', width: '65%', padding: '10px'}}>{this.props.properties.props[key]}</div>
                      <div style={{float: 'left', width: '35%', padding: '10px'}}>{key}</div>
                      <div style={{float: 'left', width: '65%', padding: '10px'}}>
                        <input data-key={key} value={this.props.properties.props[key]} onChange={this.onInputChange.bind(this)} />
                      </div>
                      <div style={{clear: 'both'}}></div>
                    </div>
                  );
                })
               }
            </div>
          </div>
        }
          <div style={{}}>
          { this.props.children }
          </div>
          <div style={{clear: 'both'}}></div>
          <Dialog
            title={translate('youHaveUnSavedWork')}
            open={this.state.showAlertUnfinishedWork}
            actions={
              [
                <FlatButton
                  label={translate('discardAndLeave')}
                  primary
                  onClick={this.handleLeave.bind(this)}
                />,
                <FlatButton
                  label={translate('publishAndLeave')}
                  secondary
                  onClick={this.handlePublishAndLeave.bind(this)}
                />,
                <FlatButton
                  label={translate('saveAndLeave')}
                  secondary
                  onClick={this.handleSaveAndLeave.bind(this)}
                  keyboardFocused
                />
              ]
            }>
            <CloseIcon
              onClick={() => this.setState({showAlertUnfinishedWork: false})}
              style={{position: 'absolute', width: 32, height: 32, right: 0, top: 0, padding: '1em', cursor: 'pointer'}}
              color={Colors.grey800}
              hoverColor={Colors.grey900} />
            {translate('whatIsYourPreferWayToLeave')}...
          </Dialog>
          <Dialog
            title={translate('save').toUpperCase()}
            open={this.state.showSaveDialog}
            actions={
              [
                <FlatButton
                  label={translate('saveAndPublish')}
                  secondary
                  onClick={::this.handlePublishAndCloseSaveDialog}
                />,
                <FlatButton
                  label={translate('save')}
                  secondary
                  onClick={::this.handleSaveAndCloseSaveDialog}
                  keyboardFocused
                />
              ]
            }>
            <CloseIcon
              onClick={() => this.setState({showSaveDialog: false})}
              style={{position: 'absolute', width: 32, height: 32, right: 0, top: 0, padding: '1em', cursor: 'pointer'}}
              color={Colors.grey800}
              hoverColor={Colors.grey900} />
            {translate('whatIsYourPreferWayToSave')}...
          </Dialog>
          <Dialog
            title={translate('searchEngineOptimalization')}
            open={this.state.showSEODialog}
            actions={
              [
                <FlatButton
                  label={translate('discard')}
                  primary
                  onClick={() => this.setState({showSEODialog: false})}
                />,
                <FlatButton
                  label="ok"
                  secondary
                  onClick={() => {
                    this.props.updateSettingData(this.props.seoDataKey, this.state.metaTags);
                    this.setState({showSEODialog: false});
                  }}
                />
              ]
            }>
            {translate('language')}:
            <select
              onChange={this.handleLanguageChange.bind(this)}
              className={style.select}
              defaultValue={language}
              value={language} >
              {this.props.languages.map((lang, index) => {
                return (
                  <option value={lang} key={index}>{lang}</option>
                );
              })}
            </select>
            {
              showSEODialog && metaTags && metaTags[language] && metaTags[language].map((item, index) => {
                return (
                  <div key={index}>
                    <div style={{display: 'inline-block', float: 'left', paddingRight: '.25em', width: '25%'}}>
                      <TextField
                        floatingLabelText="Name"
                        data-index={index}
                        data-key="name"
                        style={{width: '100%'}}
                        value={item.name}
                        onChange={this.onMetaInputChangeHandler.bind(this)}
                      />
                    </div>
                    <div style={{display: 'inline-block', float: 'left', paddingLeft: '.25em', width: '70%'}}>
                      <TextField
                        floatingLabelText="Content"
                        style={{width: '100%'}}
                        multiLine
                        data-index={index}
                        data-key="content"
                        value={item.content}
                        onChange={this.onMetaInputChangeHandler.bind(this)}
                      />
                    </div>
                    <div style={{clear: 'both'}} />
                  </div>
                );
              })
            }
          </Dialog>
        </div>
      </div>
    );
  }
}
