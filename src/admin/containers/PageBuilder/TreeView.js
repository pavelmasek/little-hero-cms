import React from 'react';
import PropTypes from 'prop-types';

import TreeView from 'react-treeview';

import { connect } from 'react-redux';
import * as editorActions from 'admin/redux/modules/editor';

function fillWithTreeViews(node, index) {
  if (node.children) {
    return (
      <TreeViewWrapper key={index} node={node} nodeLabel={node.name} defaultCollapsed={false}>
        { node.children.map(fillWithTreeViews) }
      </TreeViewWrapper>
    );
  }
  return <TreeViewItemWithoutCollapse key={index} node={node} />;
}

function findChildById(child) {
  if (this.id === child.id) {
    return child;
  }
  if (child.children) {
    return child.children.map(findChildById.bind({ id: this.id }));
  }
}

@connect((state) => ({
  setting: state.pageBuilder.setting
}), {...editorActions})

class TreeViewItemWithoutCollapse extends React.Component {
  static propTypes = {
    node: PropTypes.object,
    index: PropTypes.number,
    setProperties: PropTypes.func,
    setting: PropTypes.array
  }

  clickHandler(ev) {
    console.log('click handler ', this.props.node);
    console.log(this.props.setting.map(findChildById.bind({ id: this.props.node.id })));

    this.props.setProperties(this.props.node);
    ev.stopPropagation();
  }

  render() {
    return (
      <div key={this.props.index} className="tree-view" onClick={this.clickHandler.bind(this)}>
        <div className="tree-view_children">
          {this.props.node.name}
        </div>
      </div>
    );
  }
}

@connect(() => ({
}), {...editorActions})

class TreeViewWrapper extends React.Component {

  static propTypes = {
    key: PropTypes.string,
    nodeLabel: PropTypes.node,
    node: PropTypes.object,
    defaultCollapsed: PropTypes.bool,
    setProperties: PropTypes.func,
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ])
  }

  clickHandler(ev) {
    this.props.setProperties(this.props.node);
    ev.stopPropagation();
  }

  render() {
    return (
      <div key={this.props.key} onClick={this.clickHandler.bind(this)}>
        <TreeView {...this.props}>
          { this.props.children }
        </TreeView>
      </div>
    );
  }
}

export default class LayoutTreeView extends React.Component {

  static propTypes = {
    treeViewData: PropTypes.array
  }

  render() {
    return (
      <div>
        <div style={{padding: '0.25em 0.5em'}}>
          { this.props.treeViewData && this.props.treeViewData.map(fillWithTreeViews) }
        </div>
      </div>
    );
  }
}
