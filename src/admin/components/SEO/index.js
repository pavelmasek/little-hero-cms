import React, { Component } from 'react';
import PropTypes from 'prop-types';


import Helmet from 'react-helmet';
import { connect } from 'react-redux';

import * as seoActions from 'admin/redux/modules/seo';
import * as pageBuilderActions from 'admin/redux/modules/pageBuilder';
import * as languageActions from 'admin/redux/modules/language';

@connect(state => ({
  ...state,
  languageData: state.pageBuilder.languageData,
  language: state.language.language
}), {...pageBuilderActions, ...languageActions, ...seoActions})

export default class SEO extends Component {

  static propTypes = {
    dataKey: PropTypes.string,
    languageData: PropTypes.object,
    language: PropTypes.string,
    setSeoDataKey: PropTypes.func
  }

  componentDidMount() {
    const { dataKey, setSeoDataKey } = this.props;
    setSeoDataKey(dataKey);
  }

  render() {
    const { dataKey, language, languageData } = this.props;
    const seoData = languageData[dataKey];
    const title = seoData && seoData[language] ? seoData[language][0] : {};
    return (
      <div>
        <Helmet title={title ? title.content : ''} meta={seoData ? seoData[language] : []} />
      </div>
    );
  }
}
