import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { DIMENSIONS, DEVICES, ORIENTATION } from 'admin/utils/constants';

import * as editorActions from 'admin/redux/modules/editor';

const mobilePortrait = require('./img/mobile-portrait.png');
const mobileLandscape = require('./img/mobile-landscape.png');
const ipadPortrait = require('./img/ipad-portrait.png');
const ipadLandscape = require('./img/ipad-landscape.png');

const centerStyle = { marginLeft: 'auto', marginRight: 'auto' };
const mobilePortraitDimension = { width: DIMENSIONS.mobile.width, height: DIMENSIONS.mobile.height };
const mobileLandscapeDimension = { width: DIMENSIONS.mobile.height, height: DIMENSIONS.mobile.width };
const tabletPortraitDimension = { width: DIMENSIONS.tablet.width, height: DIMENSIONS.tablet.height };
const tabletLandscapeDimension = { width: DIMENSIONS.tablet.height, height: DIMENSIONS.tablet.width };

const mobilePortraitBackground = {
  backgroundImage: `url('${mobilePortrait}')`,
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center top',
  backgroundSize: '390px',
  padding: '120px 0'
};

const mobileLandscapeBackground = {
  backgroundImage: `url('${mobileLandscape}')`,
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center top',
  backgroundSize: '810px',
  padding: '35px 0',
  marginTop: '25px'
};

const ipadPortraitBackground = {
  backgroundImage: `url('${ipadPortrait}')`,
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center top',
  backgroundSize: '860px',
  padding: '130px 0',
  marginTop: '25px'
};

const ipadLandscapeBackground = {
  backgroundImage: `url('${ipadLandscape}')`,
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center top',
  backgroundSize: '1285px',
  padding: '50px 0',
  marginTop: '25px'
};

@connect(state => ({
  device: state.editor.device,
  orientation: state.editor.orientation
}), {...editorActions})
export default class ResponsiveWrapper extends Component {
    static propTypes = {
      children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node)
      ]),
      device: PropTypes.string,
      orientation: PropTypes.string
    }

    render() {
      const { device, orientation } = this.props;

      let deviceWrapperDimension = {};
      let wrapperDeviceBackground = {};

      switch (device) {
        case DEVICES.mobile:
          deviceWrapperDimension = orientation === ORIENTATION.landscape ? mobileLandscapeDimension : mobilePortraitDimension;
          wrapperDeviceBackground = orientation === ORIENTATION.landscape ? mobileLandscapeBackground : mobilePortraitBackground;
          break;
        case DEVICES.tablet:
          deviceWrapperDimension = orientation === ORIENTATION.landscape ? tabletLandscapeDimension : tabletPortraitDimension;
          wrapperDeviceBackground = orientation === ORIENTATION.landscape ? ipadLandscapeBackground : ipadPortraitBackground;
          break;
        default:
          break;
      }

      return (
        <div style={wrapperDeviceBackground}>
          { device === DEVICES.desktop && this.props.children }
          { device !== DEVICES.desktop &&
            <div style={{
              ...centerStyle,
              position: 'relative',
              overflow: 'scroll',
              maxWidth: `${deviceWrapperDimension.width}px`,
              minHeight: `${deviceWrapperDimension.height}px`,
              maxHeight: `${deviceWrapperDimension.height}px`,
              border: '1px solid gray',
              backgroundColor: 'white'
            }}>
              {this.props.children}
            </div>
          }
        </div>
      );
    }
}
