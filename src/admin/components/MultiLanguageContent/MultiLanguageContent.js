import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { connect } from 'react-redux';
import Editor from 'react-medium-editor';

import * as languageActions from 'admin/redux/modules/language';
import * as pageBuilderActions from 'admin/redux/modules/pageBuilder';

@connect(state => ({
  ...state,
  language: state.language.language,
  languages: state.language.languages,
  editing: state.editor.editing,
  languageData: state.pageBuilder.languageData
}), {...languageActions, ...pageBuilderActions})

export default class MultiLanguageContent extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    className: PropTypes.string,
    editing: PropTypes.bool,
    dataKey: PropTypes.string,
    display: PropTypes.string,
    language: PropTypes.string,
    languages: PropTypes.array,
    languageData: PropTypes.object,
    multiLangData: PropTypes.object,
    placeholder: PropTypes.string,
    style: PropTypes.object,
    updateLanguageData: PropTypes.func
  };

  onChangeHandler(text) {
    this.props.updateLanguageData(this.props.dataKey, this.props.language, text);
  }

  render() {
    const { language, languages, languageData, dataKey, placeholder } = this.props;
    const style = Object.assign({}, this.props.style);
    return (
      <div ref="MultiLanguageContent" style={Object.assign(style, {display: this.props.display})}>
        {!this.props.editing && languages.map((lang, index) => {
          return (
            <div
              key={index}
              style={(language === lang) ? { display: (this.props.display || 'block') } : { display: 'none'}}
              className={[this.props.className, 'editable'].join(' ')}
              dangerouslySetInnerHTML={{__html: ((languageData && languageData[dataKey] && languageData[dataKey][lang]) ? languageData[dataKey][lang] : '')}} />
          );
        })}
        {this.props.editing && languages.map((lang, index) => {
          const text = (languageData && languageData[dataKey] && languageData[dataKey][lang]) ? languageData[dataKey][lang] : '';
          return (
            <Editor
              key={index}
              style={(language === lang) ? { display: (this.props.display || 'block'), minWidth: (!text) ? '8em' : '' } : { display: 'none'}}
              className={[this.props.className, 'editable'].join(' ')}
              onChange={this.onChangeHandler.bind(this)}
              placeholder={placeholder}
              text={text}
              options={{toolbar: { buttons: ['bold', 'italic', 'underline', {
                name: 'anchor',
                contentDefault: 'link'
              }, 'h1', 'h2', 'h3', 'fontsize']}}} />
          );
        })}
      </div>
    );
  }
}
