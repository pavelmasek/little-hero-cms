import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import HoverWrapper from 'admin/components/HoverWrapper';
import ChooseImageButton from 'admin/components/ChooseImageButton';

import * as pageBuilderActions from 'admin/redux/modules/pageBuilder';

@connect(state => ({
  ...state,
  ...state.pageBuilder,
  editing: state.editor.editing,
}), {...pageBuilderActions})
export default class CMSImage extends Component {

  static propTypes = {
    className: PropTypes.string,
    dataKey: PropTypes.string,
    editing: PropTypes.bool,
    languageData: PropTypes.object,
    style: PropTypes.object,
    updateImageData: PropTypes.func
  }

  static childContextTypes = {
    setSelectedImage: PropTypes.func
  }

  state = {
    selectedImage: ''
  }

  getChildContext() {
    return {
      setSelectedImage: ::this.setSelectedImage
    };
  }

  setSelectedImage(imageLink) {
    // this.setState({selectedImage: imageLink});
    const imageData = {};
    imageData[this.props.dataKey] = imageLink;
    this.props.updateImageData(imageData);
  }

  render() {
    const { selectedImage } = this.state;
    const { className, dataKey, editing, languageData, style } = this.props;

    const imageSrc = languageData[dataKey] || 'https://calm-beach-40263-storage.s3.amazonaws.com/images/default-image.png';
    let component = {};
    if (editing) {
      component = (
        <HoverWrapper
          buttons={<ChooseImageButton />}>
          <img className={className} style={{
            minWidth: '1em',
            minHeight: '1em',
            ...style
          }} src={selectedImage || imageSrc} />
        </HoverWrapper>
      );
    } else {
      component = (
        <img className={className} style={style} src={imageSrc} />
      );
    }
    return component;
  }
}
