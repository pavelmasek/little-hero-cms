import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import { connect } from 'react-redux';

import * as editorActions from 'admin/redux/modules/editor';

import { DIMENSIONS, DEVICES, ORIENTATION } from 'admin/utils/constants';

// const desktopQuery = '';
const mobilePortraitQuery = `(max-width: ${DIMENSIONS.mobile.width}px) and (orientation: portrait)`;
const mobileLandscapeQuery = `(max-width: ${DIMENSIONS.mobile.height}px) and (orientation: landscape)`;
const tabletPortraitQuery = `(max-width: ${DIMENSIONS.tablet.width}px) and (orientation: portrait)`;
const tabletLandscapeQuery = `(max-width: ${DIMENSIONS.tablet.height}px) and (orientation: landscape)`;
const desktopQuery = `(min-width: ${DIMENSIONS.tablet.height}px)`;

export const RULES = {
  SHOW_ON_MOBILE_LANDSCAPE: mobileLandscapeQuery,
  SHOW_ON_MOBILE_PORTRAIT: mobilePortraitQuery,
  SHOW_ON_MOBILE: `${mobileLandscapeQuery} , ${mobilePortraitQuery}`,
  SHOW_ON_TABLET_LANDSCAPE: tabletLandscapeQuery,
  SHOW_ON_TABLET_PORTRAIT: tabletPortraitQuery,
  SHOW_ON_TABLET: `${tabletLandscapeQuery} , ${tabletPortraitQuery}`,
  SHOW_ON_DESKTOP: `${desktopQuery}`,
  SHOW_ALWAYS: ''
};

function shouldBeShown(showOn, device, orientation) {
  for (let index = 0, endIndex = showOn && showOn.length || 0; index < endIndex; index++) {
    const rule = showOn[index];
    switch (rule) {
      case RULES.SHOW_ALWAYS:
        return true;
      case RULES.SHOW_ON_DESKTOP:
        if (device === DEVICES.desktop) {
          return true;
        }
        break;
      case RULES.SHOW_ON_MOBILE:
        if (device === DEVICES.mobile) {
          return true;
        }
        break;
      case RULES.SHOW_ON_TABLET:
        if (device === DEVICES.tablet) {
          return true;
        }
        break;
      case RULES.SHOW_ON_MOBILE_LANDSCAPE:
        if (device === DEVICES.mobile && orientation === ORIENTATION.landscape) {
          return true;
        }
        break;
      case RULES.SHOW_ON_MOBILE_PORTRAIT:
        if (device === DEVICES.mobile && orientation === ORIENTATION.portrait) {
          return true;
        }
        break;
      case RULES.SHOW_ON_TABLET_LANDSCAPE:
        if (device === DEVICES.tablet && orientation === ORIENTATION.landscape) {
          return true;
        }
        break;
      case RULES.SHOW_ON_TABLET_PORTRAIT:
        if (device === DEVICES.tablet && orientation === ORIENTATION.portrait) {
          return true;
        }
        break;
      default:
        break;
    }
  }
}

@connect(state => ({
  editorContext: state.editor.editorContext,
  device: state.editor.device,
  orientation: state.editor.orientation
}), { ...editorActions })
export default class ResponsiveView extends Component {

    static propTypes = {
      children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node)
      ]),
      editorContext: PropTypes.bool,
      device: PropTypes.string,
      orientation: PropTypes.string,
      showOn: PropTypes.arrayOf(PropTypes.string)
    }

    render() {
      const { editorContext, device, orientation, showOn } = this.props;
      return (
        <div>
          { editorContext && shouldBeShown(showOn, device, orientation) && this.props.children }
          { !editorContext && <MediaQuery query={showOn.join(' , ')}>{this.props.children}</MediaQuery> }
        </div>
      );
    }
}
