import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { connect } from 'react-redux';
import * as mediaActions from 'admin/redux/modules/media';

import Paper from 'material-ui/Paper';
import PhotoIcon from 'material-ui/svg-icons/image/photo';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import * as Colors from 'material-ui/styles/colors';
import SearchIcon from 'material-ui/svg-icons/action/search';
import AddIcon from 'material-ui/svg-icons/content/add';

import translate from 'admin/utils/translate';
import TextField from 'material-ui/TextField';
import Highlight from 'react-highlighter';

const imagePopUp = {
  position: 'fixed',
  zIndex: 9999,
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  backgroundColor: 'rgba(0,0,0,0.5)'
};

const innerPopUpRelativeWrapper = {
  position: 'relative',
  width: '100%',
  height: '100%'
};

const innerPopUpAbsoluteWrapper = {
  position: 'absolute',
  top: '20%',
  left: '20%',
  right: '20%',
  bottom: '20%',
  backgroundColor: Colors.grey200
};

const closeIconStyle = {
  position: 'absolute',
  top: 0,
  right: 0,
  padding: '1em',
  width: 48,
  height: 48
};

const hide = {
  display: 'none'
};

@connect(state => ({
  ...state,
  ...state.media
}), { ...mediaActions })
export default class ChooseImageButton extends Component {

  static propTypes = {
    deletingMediaFile: PropTypes.bool,
    mediaFiles: PropTypes.array,
    getAllMediaFiles: PropTypes.func,
    gettingMediaFiles: PropTypes.bool,
    uploadMediaFile: PropTypes.func,
    uploadingMediaFile: PropTypes.bool
  }

  static contextTypes = {
    setSelectedImage: PropTypes.func
  }

  state = {
    fileName: '',
    imageSizes: {},
    popUpOpen: false,
    searchValue: ''
  }

  componentDidMount() {
    if (this.props.mediaFiles && !this.props.mediaFiles.length) {
      this.props.getAllMediaFiles();
    }
  }

  handleButtonClick(ev) {
    this.setState({popUpOpen: true});
    ev.stopPropagation();
    ev.preventDefault();
  }

  handlePopUpClose(ev) {
    this.setState({popUpOpen: false});
    ev.stopPropagation();
    ev.preventDefault();
  }

  handleSelectImage(selectedImageLink) {
    this.context.setSelectedImage(selectedImageLink);
    this.setState({popUpOpen: false});
  }

  handleSearchValueChange(ev) {
    const { value } = ev.currentTarget;
    this.setState({searchValue: value});
  }

  handleStopPropagation(ev) {
    const { type } = ev.target;
    if (type !== 'file') {
      ev.stopPropagation();
      ev.preventDefault();
    }
  }

  handleImageOnLoad(ev) {
    const { naturalHeight, naturalWidth } = ev.currentTarget;
    const { imageName } = ev.currentTarget.dataset;
    const image = {};
    image[imageName] = {
      naturalHeight,
      naturalWidth
    };
    this.setState({
      imageSizes: {
        ...this.state.imageSizes,
        ...image
      }
    });
  }

  handleWheel(ev) {
    if (this.state.popUpOpen) {
      ev.stopPropagation();
      // ev.preventDefault();
    }
  }

  handleFileInputChange(ev) {
    const { files } = ev.target;
    const file = files[0];
    if (file) {
      const fileReader = new FileReader();
      fileReader.onload = () => {
        Promise.all([this.props.uploadMediaFile({
          fileName: this.state.fileName || file.name,
          data: fileReader.result
        })])
        .then(() => this.props.getAllMediaFiles())
        .then(() => this.setState({fileName: ''}));
      };
      fileReader.readAsBinaryString(file);
    }
  }

  handleFileUpload() {
    this.setState({ fileName: ''});
    this.fileInput.click();
  }

  render() {
    const { popUpOpen, searchValue } = this.state;
    const { mediaFiles } = this.props;
    const visibilityStyle = (popUpOpen) ? {} : hide;
    const filteredMediaFiles = mediaFiles && mediaFiles.filter((item) => {
      if (item.Key === 'images/') {
        return false;
      }
      if (item.Key.replace('images/', '').indexOf(searchValue) > -1) {
        return true;
      }
    });
    return (
      <div onClick={::this.handleStopPropagation}>
        <PhotoIcon
          color={Colors.grey600}
          hoverColor={Colors.grey800}
          onClick={::this.handleButtonClick}/>
        <div
          onWheel={::this.handleWheel}
          style={{
            ...imagePopUp,
            ...visibilityStyle
          }}>
          <div style={innerPopUpRelativeWrapper}>
            <div style={innerPopUpAbsoluteWrapper}>
              <CloseIcon
                style={closeIconStyle}
                onClick={::this.handlePopUpClose}
                color={Colors.grey600}
                hoverColor={Colors.grey800} />
              <h3 style={{textAlign: 'center', padding: '1em'}}>Choose image</h3>
              <div
                style={{position: 'absolute', left: 0, top: 0, padding: '1em 2em'}}>
                <TextField
                  value={searchValue}
                  onChange={::this.handleSearchValueChange}
                  hintText={
                    <div>
                      <SearchIcon color={Colors.grey400} style={{float: 'left'}}/>
                      <span style={{float: 'left'}}>{translate('search')}</span>
                    </div>
                  }
                  underlineStyle={{borderColor: Colors.grey400}} />
              </div>
              <div style={{position: 'absolute', top: '12%', bottom: 0, left: 0, right: 0, overflowY: 'scroll', padding: '1em'}}>
                {
                  !this.props.gettingMediaFiles && !this.props.deletingMediaFile && !this.props.uploadingMediaFile &&
                  <Paper
                    onClick={::this.handleFileUpload}
                    zDepth={1}
                    style={{
                      width: 165,
                      height: 165,
                      textAlign: 'center',
                      float: 'left',
                      margin: '1em',
                      position: 'relative',
                      fontSize: '75%',
                      cursor: 'pointer',
                      backgroundColor: 'rgba(222, 222, 222, 0.75)'}}>
                      <table style={{width: '100%', height: '100%'}}>
                        <tbody>
                          <tr>
                            <td>
                              <AddIcon
                                style={{ width: 100, height: 100 }}
                                color={Colors.grey500} />
                              <div
                                style={{color: Colors.grey500}}>
                                {translate('upload')} {translate('files')}
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <input
                        ref={(fileInput) => this.fileInput = fileInput}
                        type="file"
                        onChange={::this.handleFileInputChange}
                        style={{visibility: 'hidden'}} />
                  </Paper>
                }
                {
                  filteredMediaFiles && filteredMediaFiles.map((item, key) => {
                    if (item.Key === 'images/') {
                      return undefined;
                    }
                    const imageSize = this.state.imageSizes[item.Key];
                    return (
                      <Paper
                        zDepth={1}
                        key={key}
                        onClick={this.handleSelectImage.bind(this, item.Link)}
                        style={{
                          height: 165,
                          width: 165,
                          margin: 15,
                          float: 'left',
                          textAlign: 'center',
                          display: 'inline-block',
                          position: 'relative',
                          backgroundColor: Colors.grey300
                        }}>
                        {
                          imageSize &&
                          <div style={{ position: 'absolute', top: 0, right: 0, padding: 5, fontSize: '65%', color: 'rgba(0,0,0, 0.5)'}}>
                            {imageSize.naturalWidth} x {imageSize.naturalHeight}
                          </div>
                        }
                        <div style={{padding: '10px'}}>
                          <img onLoad={::this.handleImageOnLoad} data-image-name={item.Key} style={{maxWidth: '120px', maxHeight: '120px'}} src={item.Link} />
                          <br />
                          <span style={{color: Colors.grey600, fontSize: '70%'}}>
                            <Highlight
                              search={searchValue}
                              matchStyle={{backgroundColor: Colors.yellow300}}>
                              {item.Key.replace('images/', '')}
                            </Highlight>
                          </span>
                        </div>
                      </Paper>
                    );
                  })
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
