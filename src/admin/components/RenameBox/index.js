import React, { Component } from 'react';
import PropTypes from 'prop-types';


import DoneIcon from 'material-ui/svg-icons/action/done';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import EditIcon from 'material-ui/svg-icons/image/edit';
import * as Colors from 'material-ui/styles/colors';

function cropText(text) {
  const postfix = '@YYYYMMDDhhmmss.json';
  const prefix = 'versions/';
  if (text.indexOf('@') > -1) {
    return text.substring(prefix.length, text.length - postfix.length);
  }
  return text.replace(prefix, '').replace('.json', '');
}

export default class RenameBox extends Component {
  static propTypes = {
    text: PropTypes.string,
    onConfirm: PropTypes.func,
    afterConfirm: PropTypes.func
  }

  state = {
    editing: false,
    textValue: cropText(this.props.text)
  }

  onInputChangeHandler(ev) {
    const { value } = ev.currentTarget;
    if (value.indexOf('@') < 0) {
      this.setState({ textValue: value });
    }
  }

  turnEditingModeOn() {
    this.setState({ editing: true}, () => this.input.focus());
  }

  turnEditingModeOff() {
    this.setState({
      editing: false
    });
  }

  revertChanges() {
    this.setState({ textValue: cropText(this.props.text) });
    this.turnEditingModeOff();
  }

  handleNameChange() {
    const splittedText = this.props.text.split('@');
    const postfix = splittedText[splittedText.length - 1];
    Promise.all([this.props.onConfirm(this.props.text, `versions/${this.state.textValue}@${postfix}`)])
      .then(() => this.props.afterConfirm());
  }

  render() {
    const { editing, hovering } = this.state;
    return (
      <div
        style={{ position: 'relative' }}
        onMouseEnter={() => this.setState({hovering: true})}
        onMouseLeave={() => this.setState({hovering: false})}>
        {
          !editing &&
          <div style={{ fontSize: 'inherit', padding: '6px' }}>
            {cropText(this.props.text)}
          </div>
        }
        {
          hovering && !editing &&
          <EditIcon
            style={{ position: 'absolute', top: 0, right: 0, marginTop: '6px', marginRight: '12px', cursor: 'pointer' }}
            onClick={::this.turnEditingModeOn} />
        }
        {
          editing &&
          <div style={{position: 'relative'}}>
            <input
              ref={(input) => this.input = input }
              style={{ width: '70%', textAlign: 'center', fontSize: 'inherit', padding: '6px' }}
              value={this.state.textValue}
              onChange={::this.onInputChangeHandler} />
            <DoneIcon
              style={{position: 'absolute', top: 0, right: 0, marginTop: '6px', marginRight: '20px'}}
              color={Colors.greenA700}
              hoverColor={Colors.greenA400}
              onClick={::this.handleNameChange} />
            <ClearIcon
              style={{position: 'absolute', top: 0, right: 0, marginTop: '6px'}}
              color={Colors.deepOrangeA700}
              hoverColor={Colors.deepOrangeA700}
              onClick={::this.revertChanges} />
          </div>
        }
      </div>
    );
  }
}
