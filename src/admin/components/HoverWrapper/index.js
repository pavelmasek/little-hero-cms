import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { connect } from 'react-redux';
import * as pageBuilderActions from 'admin/redux/modules/pageBuilder';
import * as editorActions from 'admin/redux/modules/editor';

const wrapstyle = {
  position: 'relative',
  boxShadow: 'inset 0px 0px 0px 2px rgba(222, 222, 222, 0.75)',
  cursor: 'pointer'
};

const buttonsStyles = {
  position: 'absolute',
  top: 0,
  height: 24,
  width: '100%',
  backgroundColor: 'rgba(222, 222, 222, 0.75)'
};

@connect(state => ({
  ...state,
  editing: state.editor.editing,
  languageData: state.pageBuilder.languageData
}), { ...pageBuilderActions, ...editorActions })
export default class HoverWrapper extends Component {

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    buttons: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    editing: PropTypes.bool,
    style: PropTypes.object
  }

  state = {
    hovering: false
  }

  handleMouseEnter() {
    this.setState({hovering: true});
  }

  handleMouseLeave() {
    this.setState({hovering: false});
  }

  render() {
    const { editing, style } = this.props;
    const { hovering } = this.state;
    const show = editing && hovering;
    const hoverWrapperStyle = show ? wrapstyle : {positing: 'absolute'};
    return (
      <div
        style={{
          ...style,
          ...hoverWrapperStyle
        }}
        onMouseEnter={::this.handleMouseEnter}
        onMouseLeave={::this.handleMouseLeave}>
        {
          show &&
          <div style={buttonsStyles}>
            {this.props.buttons}
          </div>
        }
        {this.props.children}
      </div>
    );
  }
}
