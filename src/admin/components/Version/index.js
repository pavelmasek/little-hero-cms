import React, { Component } from 'react';

import pack from '../../../../package.json';

export default class Version extends Component {
  render() {
    return (
      <div style={{position: 'fixed', top: 0, right: 0}}>v. {pack.version}</div>
    );
  }
}
