import React, { Component } from 'react';
import PropTypes from 'prop-types';


import AccoutCircleIcon from 'material-ui/svg-icons/action/account-circle';
import SettingIcon from 'material-ui/svg-icons/action/settings';
import FlatButton from 'material-ui/FlatButton';
import Popover from 'material-ui/Popover';

import PopoverAnimationFromTop from 'material-ui/Popover/PopoverAnimationVertical';
import { connect } from 'react-redux';
import * as authActions from 'admin/redux/modules/auth';

@connect(state => ({
  ...state,
  ...state.auth
}), {...authActions})
export default class LoginStatus extends Component {

  static propTypes = {
    getUserInfo: PropTypes.func,
    user: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object
  }

  constructor(props) {
    super(props);
  }

  state = {
    buttonElement: undefined,
    open: false
  }

  componentDidMount() {
    const { getUserInfo, user } = this.props;
    if (Object.keys(user).length === 0) {
      getUserInfo();
    }
  }

  onLoginButtonHandler(ev) {
    this.setState({
      buttonElement: ev.currentTarget,
      open: !this.state.open
    });
  }

  render() {
    const { user } = this.props;
    return (
      <div style={{textAlign: 'center'}}>
        <FlatButton
          label={`${(user ? user.login : '...')}`}
          onClick={this.onLoginButtonHandler.bind(this)}
          icon={<AccoutCircleIcon />} />
        <Popover
          open={this.state.open}
          anchorEl={this.state.buttonElement}
          onRequestClose={() => this.setState({ open: false })}
          anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'middle', vertical: 'top'}}
          animation={PopoverAnimationFromTop}>
          <FlatButton
            style={{minWidth: 200}}
            label="setting"
            icon={<SettingIcon />}
            onClick={() => {
              this.context.router.push('/admin/setting');
            }} />
          <br />
          <FlatButton
            style={{minWidth: 200}}
            label="logout"
            icon={<AccoutCircleIcon />}
            onClick={() => {
              localStorage.setItem('jwtToken', '');
              document.cookie = 'jwtToken=""';
              window.location.href = '/admin';
            }} />
        </Popover>
      </div>
    );
  }
}
