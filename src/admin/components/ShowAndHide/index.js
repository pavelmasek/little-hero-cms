import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { connect } from 'react-redux';
import Checkbox from 'material-ui/Checkbox';
import ActionVisibility from 'material-ui/svg-icons/action/visibility';
import ActionVisibilityBorder from 'material-ui/svg-icons/action/visibility-off';

import * as pageBuilderActions from 'admin/redux/modules/pageBuilder';

const styles = {
  wrap: {
    position: 'relative',
    boxShadow: 'inset 0px 0px 0px 2px rgba(222, 222, 222, 0.75)',
    cursor: 'pointer'
  },
  backgroundDisabled: {
    position: 'absolute',
    backgroundImage: 'repeating-linear-gradient(135deg, rgba(0,0,0,.3), rgba(0,0,0,.3) 1px, transparent 2px, transparent 2px, rgba(0,0,0,.3) 3px)',
    backgroundSize: '4px 4px',
    zIndex: 1400
  }
};

@connect(state => ({
  ...state,
  editing: state.editor.editing,
  languageData: state.pageBuilder.languageData
}), { ...pageBuilderActions })

export default class ShowAndHide extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    editing: PropTypes.bool,
    languageData: PropTypes.object,
    dataKey: PropTypes.string.isRequired,
    updateSettingData: PropTypes.func
  }

  state = {
    hover: false
  }

  onCheckHandler() {
    const { dataKey, languageData } = this.props;
    const checked = languageData[dataKey];
    this.props.updateSettingData(this.props.dataKey, !checked);
  }

  handleMouseEnter() {
    this.setState({hover: true});
  }

  handleMouseLeave() {
    this.setState({hover: false});
  }

  render() {
    const { editing, children, dataKey, languageData } = this.props;
    const { hover } = this.state;
    const checked = languageData[dataKey];
    return (
      <div
        ref="showAndHideArea"
        style={editing && hover ? styles.wrap : {}}
        onMouseEnter={this.handleMouseEnter.bind(this)}
        onMouseLeave={this.handleMouseLeave.bind(this)}>
        {
          editing && hover &&
          <div style={{
            position: 'absolute',
            display: 'inline',
            left: 0,
            padding: '5px',
            top: '-34px',
            zIndex: 1450,
            backgroundColor: 'rgba(222, 222, 222, 0.75)'
          }}>
            <Checkbox
              unCheckedIcon={<ActionVisibility />}
              checkedIcon={<ActionVisibilityBorder />}
              checked={!checked}
              onCheck={this.onCheckHandler.bind(this)}
            />
          </div>
        }
        {
          editing &&
          <div style={ checked ? { visibility: 'hidden' } : { visibility: 'visible' } }>
            <div style={{
              ...styles.backgroundDisabled,
              minHeight: (this.refs.showAndHideArea) ? this.refs.showAndHideArea.offsetHeight + 'px' : '',
              minWidth: (this.refs.showAndHideArea) ? this.refs.showAndHideArea.offsetWidth + 'px' : ''
            }} />
          </div>
        }
        {(checked || editing) && children}
      </div>
    );
  }
}
