import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as Colors from 'material-ui/styles/colors';

import * as editorActions from 'admin/redux/modules/editor';

import translate from 'admin/utils/translate';

import PhoneIcon from 'material-ui/svg-icons/hardware/phone-iphone';
import TabletIcon from 'material-ui/svg-icons/hardware/tablet-mac';
import DesktopIcon from 'material-ui/svg-icons/hardware/desktop-mac';
import LandscapeIcon from 'material-ui/svg-icons/communication/stay-primary-landscape';
import PortraitIcon from 'material-ui/svg-icons/communication/stay-primary-portrait';

@connect(state => ({
  device: state.editor.device,
  orientation: state.editor.orientation
}), { ...editorActions })
export default class ResponsivePanelView extends Component {

  static propTypes = {
    device: PropTypes.string,
    orientation: PropTypes.string,
    setDevice: PropTypes.func,
    setOrientation: PropTypes.func
  }

  handleDeviceOrientationChange(ev) {
    const { orientation, device } = ev.currentTarget.dataset;
    if (device) {
      this.props.setDevice(device);
    }
    if (orientation) {
      this.props.setOrientation(orientation);
    }
  }

  render() {
    const { device, orientation } = this.props;
    return (
      <div style={{display: 'inline-block'}}>
        <div style={{display: 'inline-block', paddingLeft: '.5em', paddingRight: '.5em'}}>
          {translate('device')}<br />
          <DesktopIcon
            onClick={this.handleDeviceOrientationChange.bind(this)}
            data-device="desktop"
            style={{width: 20, height: 20, cursor: 'pointer', paddingLeft: '.5em', paddingRight: '.5em'}}
            color={device === 'desktop' ? Colors.grey800 : Colors.grey500} />
          <TabletIcon
            onClick={this.handleDeviceOrientationChange.bind(this)}
            data-device="tablet"
            style={{width: 20, height: 20, cursor: 'pointer', paddingLeft: '.5em', paddingRight: '.5em'}}
            color={device === 'tablet' ? Colors.grey800 : Colors.grey500} />
          <PhoneIcon
            onClick={this.handleDeviceOrientationChange.bind(this)}
            data-device="mobile"
            style={{width: 20, height: 20, cursor: 'pointer', paddingLeft: '.5em', paddingRight: '.5em'}}
            color={device === 'mobile' ? Colors.grey800 : Colors.grey500} />
        </div>
        <div style={{display: 'inline-block', paddingLeft: '.5em', paddingRight: '.5em', borderRight: '1px solid black'}}>
            {translate('orientation')}<br />
          <PortraitIcon
            onClick={this.handleDeviceOrientationChange.bind(this)}
            data-orientation="portrait"
            style={{width: 20, height: 20, cursor: 'pointer', paddingLeft: '.5em', paddingRight: '.5em'}}
            color={orientation === 'portrait' && device !== 'desktop' ? Colors.grey800 : Colors.grey500} />
          <LandscapeIcon
            onClick={this.handleDeviceOrientationChange.bind(this)}
            data-orientation="landscape"
            style={{width: 20, height: 20, cursor: 'pointer', paddingLeft: '.5em', paddingRight: '.5em'}}
            color={orientation === 'landscape' && device !== 'desktop' ? Colors.grey800 : Colors.grey500} />
        </div>
      </div>
    );
  }
}
