const SET_SEO_DATA_KEY = 'SET_SEO_DATA_KEY';

const initialState = {
  seoDataKey: ''
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_SEO_DATA_KEY:
      return {
        ...state,
        seoDataKey: action.dataKey
      };
    default:
      return state;
  }
}

export function setSeoDataKey(dataKey) {
  return {
    type: SET_SEO_DATA_KEY,
    dataKey
  };
}
