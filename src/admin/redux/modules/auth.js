const LOAD = 'redux-example/auth/LOAD';
const LOAD_SUCCESS = 'redux-example/auth/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/auth/LOAD_FAIL';
const LOGIN = 'redux-example/auth/LOGIN';
const LOGIN_SUCCESS = 'redux-example/auth/LOGIN_SUCCESS';
const LOGIN_FAIL = 'redux-example/auth/LOGIN_FAIL';
const LOGOUT = 'redux-example/auth/LOGOUT';
const LOGOUT_SUCCESS = 'redux-example/auth/LOGOUT_SUCCESS';
const LOGOUT_FAIL = 'redux-example/auth/LOGOUT_FAIL';

const CHANGE_PASSWORD = 'CHANGE_PASSWORD';
const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';
const CHANGE_PASSWORD_FAIL = 'CHANGE_PASSWORD_FAIL';

const GET_USER_INFO = 'GET_USER_INFO';
const GET_USER_INFO_SUCCESS = 'GET_USER_INFO_SUCCESS';
const GET_USER_INFO_FAIL = 'GET_USER_INFO_FAIL';

const initialState = {
  loaded: false,
  user: {},
  changingPassword: false,
  changingPasswordError: false,
  gettingUserInfo: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.result
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOGIN:
      return {
        ...state,
        loggingIn: true
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loggingIn: false,
        user: action.result
      };
    case LOGIN_FAIL:
      return {
        ...state,
        loggingIn: false,
        user: null,
        loginError: action.error
      };
    case CHANGE_PASSWORD:
      return {
        ...state,
        changingPassword: true,
        changingPasswordError: false
      };
    case CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        changingPassword: false
      };
    case CHANGE_PASSWORD_FAIL:
      return {
        ...state,
        changingPassword: false,
        changingPasswordError: true
      };
    case GET_USER_INFO:
      return {
        ...state,
        gettingUserInfo: true
      };
    case GET_USER_INFO_SUCCESS:
      return {
        ...state,
        user: action.result,
        gettingUserInfo: false
      };
    case GET_USER_INFO_FAIL:
      return {
        ...state,
        gettingUserInfo: false
      };
    case LOGOUT:
      return {
        ...state,
        loggingOut: true
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        loggingOut: false,
        user: null
      };
    case LOGOUT_FAIL:
      return {
        ...state,
        loggingOut: false,
        logoutError: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.auth && globalState.auth.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/loadAuth')
  };
}

export function login(name, password) {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAIL],
    promise: (client) => client.post('admin/users/signIn', {
      data: {
        user: {
          login: name,
          password: password
        }
      }
    })
  };
}

export function logout() {
  return {
    types: [LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAIL],
    promise: (client) => client.get('admin/auth/logout')
  };
}

export function changePassword(password) {
  return {
    types: [CHANGE_PASSWORD, CHANGE_PASSWORD_SUCCESS, CHANGE_PASSWORD_FAIL],
    promise: (client) => client.post('admin/users/changePassword', {
      data: {
        user: {
          password
        }
      }
    })
  };
}

export function getUserInfo() {
  return {
    types: [GET_USER_INFO, GET_USER_INFO_SUCCESS, GET_USER_INFO_FAIL],
    promise: (client) => client.post('admin/users/getUserInfo')
  };
}
