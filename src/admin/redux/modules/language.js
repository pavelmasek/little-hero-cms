const CS = 'CS';
const EN = 'EN';
const DE = 'DE';
const PL = 'PL';
const SET_LANGUAGE = 'SET_LANGUAGE';

const initialState = {
  language: 'cs',
  languages: ['cs', 'en', 'de', 'pl']
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CS:
      return {
        ...state,
        language: 'cs'
      };
    case EN:
      return {
        ...state,
        language: 'en'
      };
    case DE:
      return {
        ...state,
        language: 'de'
      };
    case PL:
      return {
        ...state,
        language: 'pl'
      };
    case SET_LANGUAGE:
      return {
        ...state,
        language: action.language
      };
    default:
      return state;
  }
}

export function setCS() {
  return {
    type: CS
  };
}

export function setEN() {
  return {
    type: EN
  };
}

export function setDE() {
  return {
    type: DE
  };
}

export function setPL() {
  return {
    type: PL
  };
}

export function setLanguage(language) {
  return {
    type: SET_LANGUAGE,
    language
  };
}
