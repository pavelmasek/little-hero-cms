const RUN_BUILD = 'RUN_BUILD';
const RUN_BUILD_LOAD = 'RUN_BUILD_LOAD';
const RUN_BUILD_FAIL = 'RUN_BUILD_FAIL';
const SET_BUILDED_TO_FALSE = 'SET_BUILDED_TO_FALSE';

const initialState = {
  builded: false,
  building: false,
  buildError: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case RUN_BUILD:
      return {
        ...state,
        building: true,
        buildError: false
      };
    case RUN_BUILD_LOAD:
      return {
        ...state,
        builded: true,
        building: false
      };
    case RUN_BUILD_FAIL:
      return {
        ...state,
        builded: false,
        building: false,
        buildError: true
      };
    case SET_BUILDED_TO_FALSE:
      return {
        ...state,
        builded: false
      };
    default:
      return state;
  }
}

export function runBuild() {
  return {
    types: [RUN_BUILD, RUN_BUILD_LOAD, RUN_BUILD_FAIL],
    promise: (client) => client.post('/admin/build/runBuild')
  };
}

export function setBuildedToFalse() {
  return {
    type: SET_BUILDED_TO_FALSE
  };
}
