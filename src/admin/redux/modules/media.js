const GET_MEDIA_FILES = 'GET_MEDIA_FILES';
const GET_MEDIA_FILES_SUCCESS = 'GET_MEDIA_FILES_SUCCESS';
const GET_MEDIA_FILES_FAIL = 'GET_MEDIA_FILES_FAIL';

const UPLOAD_MEDIA_FILE = 'UPLOAD_MEDIA_FILE';
const UPLOAD_MEDIA_FILE_SUCCESS = 'UPLOAD_MEDIA_FILE_SUCCESS';
const UPLOAD_MEDIA_FILE_FAIL = 'UPLOAD_MEDIA_FILE_FAIL';

const DELETE_MEDIA_FILE = 'DELETE_MEDIA_FILE';
const DELETE_MEDIA_FILE_SUCCESS = 'DELETE_MEDIA_FILE_SUCCESS';
const DELETE_MEDIA_FILE_FAIL = 'DELETE_MEDIA_FILE_FAIL';

const initialState = {
  gettingMediaFiles: false,
  mediaFiles: [],
  uploadingMediaFile: false,
  deletingMediaFile: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_MEDIA_FILES:
      return {
        gettingMediaFiles: true
      };
    case GET_MEDIA_FILES_SUCCESS:
      return {
        gettingMediaFiles: false,
        mediaFiles: action.result
      };
    case GET_MEDIA_FILES_FAIL:
      return {
        gettingMediaFiles: false
      };
    case UPLOAD_MEDIA_FILE:
      return {
        uploadingMediaFile: true
      };
    case UPLOAD_MEDIA_FILE_SUCCESS:
      return {
        uploadingMediaFile: false
      };
    case UPLOAD_MEDIA_FILE_FAIL:
      return {
        uploadingMediaFile: false
      };
    case DELETE_MEDIA_FILE:
      return {
        deletingMediaFile: true
      };
    case DELETE_MEDIA_FILE_SUCCESS:
      return {
        deletingMediaFile: false
      };
    case DELETE_MEDIA_FILE_FAIL:
      return {
        deletingMediaFile: false
      };
    default:
      return state;
  }
}

export function getAllMediaFiles() {
  return {
    types: [GET_MEDIA_FILES, GET_MEDIA_FILES_SUCCESS, GET_MEDIA_FILES_FAIL],
    promise: (client) => client.post('/admin/media/getAllMediaFiles', {})
  };
}

export function uploadMediaFile(file) {
  return {
    types: [UPLOAD_MEDIA_FILE, UPLOAD_MEDIA_FILE_SUCCESS, UPLOAD_MEDIA_FILE_FAIL],
    promise: (client) => client.post('/admin/media/uploadMediaFile', {
      data: {
        file
      }
    })
  };
}

export function deleteMediaFile(fileName) {
  return {
    types: [DELETE_MEDIA_FILE, DELETE_MEDIA_FILE_SUCCESS, DELETE_MEDIA_FILE_FAIL],
    promise: (client) => client.post('/admin/media/deleteMediaFile', {
      data: {
        fileName
      }
    })
  };
}
