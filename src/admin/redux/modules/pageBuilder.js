const SET_PAGE_BUILDER_SETTING = 'SET_PAGE_BUILDER_SETTING';
const UPDATE_PAGE_BUILDER_SETTING = 'UPDATE_PAGE_BUILDER_SETTING';
const SET_LANGUAGE_DATA = 'SET_LANGUAGE_DATA';
const UPDATE_LANGUAGE_DATA = 'UPDATE_LANGUAGE_DATA';
const FORCE_UPDATE_LANGUAGE_DATA = 'FORCE_UPDATE_LANGUAGE_DATA';
const UPDATE_SETTING_DATA = 'UPDATE_SETTING_DATA';
const REMOVE_DATA_FROM_ARRAY = 'REMOVE_DATA_FROM_ARRAY';

const UPDATE_IMAGE_DATA = 'UPDATE_IMAGE_DATA';

const SAVE_LANGUAGE_DATA = 'SAVE_LANGUAGE_DATA';
const SAVE_LANGUAGE_DATA_SUCCESS = 'SAVE_LANGUAGE_DATA_SUCCESS';
const SAVE_LANGUAGE_DATA_FAIL = 'SAVE_LANGUAGE_DATA_FAIL';

const SET_NEW_LANGUAGE_DATA = 'SET_NEW_LANGUAGE_DATA';
const SET_NEW_LANGUAGE_DATA_SUCCESS = 'SET_NEW_LANGUAGE_DATA_SUCCESS';
const SET_NEW_LANGUAGE_DATA_FAIL = 'SET_NEW_LANGUAGE_DATA_FAIL';

const LOAD_LANGUAGE_DATA = 'LOAD_LANGUAGE_DATA';
const LOAD_LANGUAGE_DATA_SUCCESS = 'LOAD_LANGUAGE_DATA_SUCCESS';
const LOAD_LANGUAGE_DATA_FAIL = 'LOAD_LANGUAGE_DATA_FAIL';

const GET_LANGUAGE_VERSIONS = 'GET_LANGUAGE_VERSIONS';
const GET_LANGUAGE_VERSIONS_SUCCESS = 'GET_LANGUAGE_VERSIONS_SUCCESS';
const GET_LANGUAGE_VERSIONS_FAIL = 'GET_LANGUAGE_VERSIONS_FAIL';

const GET_LANGUAGE_VERSION_DATA = 'GET_LANGUAGE_VERSION_DATA';
const GET_LANGUAGE_VERSION_DATA_SUCCESS = 'GET_LANGUAGE_VERSION_DATA_SUCCESS';
const GET_LANGUAGE_VERSION_DATA_FAIL = 'GET_LANGUAGE_VERSION_DATA_FAIL';

const CREATE_NEW_LANGUAGE_VERSION = 'CREATE_NEW_LANGUAGE_VERSION';
const CREATE_NEW_LANGUAGE_VERSION_SUCCESS = 'CREATE_NEW_LANGUAGE_VERSION_SUCCESS';
const CREATE_NEW_LANGUAGE_VERSION_FAIL = 'CREATE_NEW_LANGUAGE_VERSION_FAIL';

const DELETE_LANGUAGE_VERSION = 'DELETE_LANGUAGE_VERSION';
const DELETE_LANGUAGE_VERSION_SUCCESS = 'DELETE_LANGUAGE_VERSION_SUCCESS';
const DELETE_LANGUAGE_VERSION_FAIL = 'DELETE_LANGUAGE_VERSION_FAIL';

const RENAME_LANGUAGE_VERSION = 'RENAME_LANGUAGE_VERSION';
const RENAME_LANGUAGE_VERSION_SUCCESS = 'RENAME_LANGUAGE_VERSION_SUCCESS';
const RENAME_LANGUAGE_VERSION_FAIL = 'RENAME_LANGUAGE_VERSION_FAIL';

const initialState = {
  setting: [],
  languageData: {},
  languageVersions: [],
  gettingLanguageData: false,
  settingNewLanguage: false,
  savingLanguageData: false,
  creatingNewLanguageVersion: false,
  deletingLanguageVersion: false,
  renamingLanguageVersion: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_PAGE_BUILDER_SETTING:
      return {
        ...state,
        setting: action.setting
      };
    case UPDATE_PAGE_BUILDER_SETTING:
      return {
        ...state,
        setting: state.setting.map(component => {
          console.log('component ', component);
          console.log('component setting ', action.componentSetting);
          if (component.id === action.componentSetting.id) {
            return action.componentSetting;
          }
          return component;
        })
      };
    case SET_LANGUAGE_DATA:
      return {
        ...state,
        languageData: action.languageData
      };
    case UPDATE_LANGUAGE_DATA:
      const updateLangData = {};
      const concreteLanguageData = { ...state.languageData[action.langdataUpdateDataKey] };
      concreteLanguageData[action.langdataLanguage] = action.langdataUpdate;
      updateLangData[action.langdataUpdateDataKey] = {
        ...state.languageData[action.langdataUpdateDataKey],
        ...concreteLanguageData
      };
      return {
        ...state,
        languageData: {
          ...state.languageData,
          ...updateLangData
        }
      };
    case FORCE_UPDATE_LANGUAGE_DATA:
      const { langdataUpdate, langdataUpdateDataKey } = action;
      const forceUpdateLangData = {};
      forceUpdateLangData[langdataUpdateDataKey] = langdataUpdate;
      return {
        ...state,
        languageData: {
          ...state.languageData,
          ...forceUpdateLangData
        }
      };
    case REMOVE_DATA_FROM_ARRAY:
      const { objectKey, arrayDataKey } = action;
      const newLangData = state.languageData;
      const newArr = state.languageData[arrayDataKey].filter((obj, key) => {
        if (key === objectKey) {
          Object.keys(obj).map((item) => {
            delete newLangData[item];
          });
          return false;
        }
        return true;
      });
      newLangData[arrayDataKey] = newArr;
      return {
        ...state,
        languageData: {
          ...newLangData
        }
      };
    case UPDATE_SETTING_DATA:
      const newSettingData = {};
      const { settingDataUpdateDataKey, value } = action;
      newSettingData[settingDataUpdateDataKey] = value;
      return {
        ...state,
        languageData: {
          ...state.languageData,
          ...newSettingData
        }
      };
    case UPDATE_IMAGE_DATA:
      return {
        ...state,
        languageData: {
          ...state.languageData,
          ...action.imageData
        }
      };
    case SAVE_LANGUAGE_DATA:
      console.log('initialized saving language data');
      return {
        ...state,
        savingLanguageData: true
      };
    case SAVE_LANGUAGE_DATA_SUCCESS:
      console.log('success ', action.result);
      return {
        ...state,
        savingLanguageData: false
      };
    case SAVE_LANGUAGE_DATA_FAIL:
      console.log('fail ', action.error);
      return {
        ...state,
        savingLanguageData: false
      };
    case SET_NEW_LANGUAGE_DATA:
      console.log('initialized setting new language data');
      return {
        ...state,
        settingNewLanguage: true
      };
    case SET_NEW_LANGUAGE_DATA_SUCCESS:
      console.log('success ', action.result);
      return {
        ...state,
        settingNewLanguage: false
      };
    case SET_NEW_LANGUAGE_DATA_FAIL:
      console.log('fail ', action.error);
      return {
        ...state,
        settingNewLanguage: false
      };
    case LOAD_LANGUAGE_DATA:
      console.log('initialized loading language data');
      return {
        ...state
      };
    case LOAD_LANGUAGE_DATA_SUCCESS:
      console.log('successful loading language data');
      return {
        ...state,
        gettingLanguageData: false,
        languageData: action.result
      };
    case LOAD_LANGUAGE_DATA_FAIL:
      console.log('fail ', action.error);
      return {
        ...state,
        gettingLanguageData: false
      };
    case GET_LANGUAGE_VERSIONS:
      console.log('initialized loading language VERSIONS');
      return {
        ...state,
        gettingLanguageData: true
      };
    case GET_LANGUAGE_VERSIONS_SUCCESS:
      console.log('successful loading language VERSIONS');
      return {
        ...state,
        languageVersions: action.result,
        gettingLanguageData: false
      };
    case GET_LANGUAGE_VERSIONS_FAIL:
      console.log('fail ', action.error);
      return {
        ...state,
        gettingLanguageData: false
      };
    case GET_LANGUAGE_VERSION_DATA:
      console.log('initialized loading language VERSIONS');
      return state;
    case GET_LANGUAGE_VERSION_DATA_SUCCESS:
      console.log('successful loading language VERSIONS');
      return {
        ...state,
        languageData: action.result
      };
    case GET_LANGUAGE_VERSION_DATA_FAIL:
      console.log('fail ', action.error);
      return state;
    case CREATE_NEW_LANGUAGE_VERSION:
      console.log('initialized creating new language VERSIONS');
      return {
        ...state,
        creatingNewLanguageVersion: true
      };
    case CREATE_NEW_LANGUAGE_VERSION_SUCCESS:
      console.log('successful creating new language VERSIONS');
      return {
        ...state,
        creatingNewLanguageVersion: false
      };
    case CREATE_NEW_LANGUAGE_VERSION_FAIL:
      console.log('fail ', action.error);
      return {
        ...state,
        creatingNewLanguageVersion: false
      };
    case DELETE_LANGUAGE_VERSION:
      console.log('initialized deleting new language VERSIONS');
      return {
        ...state,
        deletingLanguageVersion: true
      };
    case DELETE_LANGUAGE_VERSION_SUCCESS:
      console.log('successful deleting new language VERSIONS');
      return {
        ...state,
        deletingLanguageVersion: false
      };
    case DELETE_LANGUAGE_VERSION_FAIL:
      console.log('fail ', action.error);
      return {
        ...state,
        deletingLanguageVersion: false
      };
    case RENAME_LANGUAGE_VERSION:
      console.log('initialized renaming new language VERSIONS');
      return {
        ...state,
        renamingLanguageVersion: true
      };
    case RENAME_LANGUAGE_VERSION_SUCCESS:
      console.log('successful renaming new language VERSIONS');
      return {
        ...state,
        renamingLanguageVersion: false
      };
    case RENAME_LANGUAGE_VERSION_FAIL:
      console.log('fail ', action.error);
      return {
        ...state,
        renamingLanguageVersion: false
      };
    default:
      return state;
  }
}

export function setPageBuilderSetting(setting) {
  return {
    type: SET_PAGE_BUILDER_SETTING,
    setting
  };
}

export function updatePageBuilderSetting(componentSetting) {
  return {
    type: UPDATE_PAGE_BUILDER_SETTING,
    componentSetting
  };
}

export function setLanguageData(languageData) {
  return {
    type: SET_LANGUAGE_DATA,
    languageData
  };
}
export function updateLanguageData(langdataUpdateDataKey, langdataLanguage, langdataUpdate) {
  return {
    type: UPDATE_LANGUAGE_DATA,
    langdataUpdateDataKey,
    langdataLanguage,
    langdataUpdate
  };
}

export function forceUpdateLanguageData(langdataUpdateDataKey, langdataUpdate) {
  return {
    type: FORCE_UPDATE_LANGUAGE_DATA,
    langdataUpdateDataKey,
    langdataUpdate
  };
}

export function removeDataFromArray(objectKey, arrayDataKey) {
  return {
    type: REMOVE_DATA_FROM_ARRAY,
    objectKey,
    arrayDataKey
  };
}

export function updateSettingData(settingDataUpdateDataKey, value) {
  return {
    type: UPDATE_SETTING_DATA,
    settingDataUpdateDataKey,
    value
  };
}

export function updateImageData(imageData) {
  return {
    type: UPDATE_IMAGE_DATA,
    imageData
  };
}

export function saveLanguageData(languageData, version) {
  return {
    types: [SAVE_LANGUAGE_DATA, SAVE_LANGUAGE_DATA_SUCCESS, SAVE_LANGUAGE_DATA_FAIL],
    promise: (client) => client.post('/admin/languageData/saveLanguageData', {
      data: {
        languageData,
        version
      }
    })
  };
}

export function setNewLanguageData(fileName) {
  return {
    types: [SET_NEW_LANGUAGE_DATA, SET_NEW_LANGUAGE_DATA_SUCCESS, SET_NEW_LANGUAGE_DATA_FAIL],
    promise: (client) => client.post('/admin/languageData/setNewLanguageData', {
      data: {
        fileName
      }
    })
  };
}

export function loadLanguageData() {
  return {
    types: [LOAD_LANGUAGE_DATA, LOAD_LANGUAGE_DATA_SUCCESS, LOAD_LANGUAGE_DATA_FAIL],
    promise: (client) => client.post('/admin/languageData/loadLanguageData', {})
  };
}

export function getLanguageVersions() {
  return {
    types: [GET_LANGUAGE_VERSIONS, GET_LANGUAGE_VERSIONS_SUCCESS, GET_LANGUAGE_VERSIONS_FAIL],
    promise: (client) => client.post('/admin/languageData/getLanguageVersions', {})
  };
}

export function getLanguageVersionData(fileName) {
  return {
    types: [GET_LANGUAGE_VERSION_DATA, GET_LANGUAGE_VERSION_DATA_SUCCESS, GET_LANGUAGE_VERSION_DATA_FAIL],
    promise: (client) => client.post('/admin/languageData/getLanguageVersionData', {
      data: {
        fileName
      }
    })
  };
}

export function createNewLanguageVersion(fileName) {
  return {
    types: [CREATE_NEW_LANGUAGE_VERSION, CREATE_NEW_LANGUAGE_VERSION_SUCCESS, CREATE_NEW_LANGUAGE_VERSION_FAIL],
    promise: (client) => client.post('/admin/languageData/createNewLanguageVersion', {
      data: {
        fileName
      }
    })
  };
}

export function deleteLanguageVersion(fileName) {
  return {
    types: [DELETE_LANGUAGE_VERSION, DELETE_LANGUAGE_VERSION_SUCCESS, DELETE_LANGUAGE_VERSION_FAIL],
    promise: (client) => client.post('/admin/languageData/deleteLanguageVersion', {
      data: {
        fileName
      }
    })
  };
}

export function renameLanguageVersion(oldFileName, newFileName) {
  return {
    types: [RENAME_LANGUAGE_VERSION, RENAME_LANGUAGE_VERSION_SUCCESS, RENAME_LANGUAGE_VERSION_FAIL],
    promise: (client) => client.post('/admin/languageData/renameLanguageVersion', {
      data: {
        oldFileName,
        newFileName
      }
    })
  };
}
