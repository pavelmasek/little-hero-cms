import { DEVICES, ORIENTATION } from 'admin/utils/constants';

const EDIT_ON = 'EDIT_ON';
const EDIT_OFF = 'EDIT_OFF';
const SET_PROPERTIES = 'SET_PROPERTIES';
const EDIT_PROPERTIES_PROPS = 'EDIT_PROPERTIES_PROPS';
const SWITCH_EDITING = 'SWITCH_EDITING';

const EDITOR_CONTEXT_ON = 'EDITOR_CONTEXT_ON';
const EDITOR_CONTEXT_OFF = 'EDITOR_CONTEXT_OFF';

const SET_DEVICE_TYPE = 'SET_DEVICE_TYPE';
const SET_ORIENTATION = 'SET_ORIENTATION';

const initialState = {
  editing: false,
  editorContext: false,
  device: DEVICES.desktop,
  orientation: ORIENTATION.landscape,
  properties: {}
};


export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case EDIT_ON:
      return {
        ...state,
        editing: true
      };
    case EDIT_OFF:
      return {
        ...state,
        editing: false
      };
    case EDITOR_CONTEXT_ON:
      return {
        ...state,
        editorContext: true
      };
    case EDITOR_CONTEXT_OFF:
      return {
        ...state,
        editorContext: false
      };
    case SWITCH_EDITING:
      return {
        ...state,
        editing: !state.editing
      };
    case SET_PROPERTIES:
      return {
        ...state,
        properties: action.properties
      };
    case EDIT_PROPERTIES_PROPS:
      return {
        ...state,
        properties: {
          ...state.properties,
          props: action.props
        }
      };
    case SET_DEVICE_TYPE:
      return {
        ...state,
        device: action.device
      };
    case SET_ORIENTATION:
      return {
        ...state,
        orientation: action.orientation
      };
    default:
      return state;
  }
}

export function editOn() {
  return {
    type: EDIT_ON
  };
}

export function editOff() {
  return {
    type: EDIT_OFF
  };
}

export function editorContextOn() {
  return {
    type: EDITOR_CONTEXT_ON
  };
}

export function editorContextOff() {
  return {
    type: EDITOR_CONTEXT_OFF
  };
}

export function switchEditing() {
  return {
    type: SWITCH_EDITING
  };
}

export function setProperties(properties) {
  return {
    type: SET_PROPERTIES,
    properties
  };
}

export function editPropertiesProps(props) {
  return {
    type: EDIT_PROPERTIES_PROPS,
    props
  };
}

export function setDevice(device) {
  return {
    type: SET_DEVICE_TYPE,
    device
  };
}

export function setOrientation(orientation) {
  return {
    type: SET_ORIENTATION,
    orientation
  };
}
