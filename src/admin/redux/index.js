import admin from './modules/admin';
import auth from './modules/auth';
import editor from './modules/editor';
import language from './modules/language';
import media from './modules/media';
import pageBuilder from './modules/pageBuilder';
import seo from './modules/seo';

export default {
  admin,
  auth,
  editor,
  language,
  media,
  pageBuilder,
  seo
};
