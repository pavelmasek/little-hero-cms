const translation = require('./translation.json');

export default function translate(key) {
  const { data, languages } = translation;
  if (typeof localStorage === 'undefined') {
    return (data[key] && data[key].en) || '';
  }
  const language = localStorage.getItem('littleHeroCmsLanguage');
  return (data[key] && data[key][languages.indexOf(language)]) || '';
}

export function setTranslation() {
  if (typeof localStorage === 'undefined') {
    return;
  }
  const settedLanguage = localStorage.getItem('littleHeroCmsLanguage');
  if (settedLanguage) {
    return;
  }
  const { languages } = translation;
  const browserLanguage = navigator.language;
  const defaultLanguage = languages[languages.indexOf(browserLanguage)] || 'en';
  localStorage.setItem('littleHeroCmsLanguage', defaultLanguage);
}

export function getLanguages() {
  return translation.languages;
}

export function getCurrentLanguage() {
  if (typeof localStorage === 'undefined') {
    return '';
  }
  return localStorage.getItem('littleHeroCmsLanguage');
}

export function setLanguageOfTranslation(language) {
  const { languages } = translation;
  if (languages[languages.indexOf(language)]) {
    localStorage.setItem('littleHeroCmsLanguage', language);
  }
}

