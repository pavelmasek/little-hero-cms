export default (path) => {
  return new Promise(resolve => {
    require.ensure([], () => {
      resolve({
        component: require(path)
      });
    });
  });
};
