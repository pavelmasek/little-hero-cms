export const DEVICES = {
  desktop: 'desktop',
  tablet: 'tablet',
  mobile: 'mobile'
};

export const ORIENTATION = {
  portrait: 'portrait',
  landscape: 'landscape'
};

export const DIMENSIONS = {
  mobile: {
    width: 320,
    height: 568
  },
  tablet: {
    width: 768,
    height: 1024
  }
};
