import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as reduxAsyncConnect} from 'redux-connect';

import cmsActions from 'admin/redux';
import publicActions from 'public/redux';

export default combineReducers({
  routing: routerReducer,
  reduxAsyncConnect,
  ...cmsActions,
  ...publicActions
});
