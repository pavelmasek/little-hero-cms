// require('babel-polyfill');
const ip = require('ip');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || ip.address() || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || ip.address() || 'localhost',
  apiPort: process.env.APIPORT,
  app: {
    title: 'ANSA Sanatorium, s.r.o. - Privátní chirurgická klinika',
    description: 'Privátní chirurgická klinika, chirurgie, traumatologie, plastická chirurgie, ortopedie - ANSA Sanatorium s.r.o.',
    head: {
      meta: [
        {name: 'description', content: 'Privátní chirurgická klinika, chirurgie, traumatologie, plastická chirurgie, ortopedie - ANSA Sanatorium s.r.o.'},
        {charset: 'utf-8'}
      ]
    }
  },

}, environment);
