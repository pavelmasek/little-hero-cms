import Express from 'express';
import React from 'react/lib/React';
import ReactDOM from 'react-dom/server';
import config from './config';
import favicon from 'serve-favicon';
import compression from 'compression';
import httpProxy from 'http-proxy';
import path from 'path';
import createStore from './redux/create';
import * as pageBuilder from 'admin/redux/modules/pageBuilder';
import ApiClient from './helpers/ApiClient';
import Html from './helpers/Html';
import PrettyError from 'pretty-error';
import http from 'http';

import { match } from 'react-router';
import { ReduxAsyncConnect, loadOnServer } from 'redux-connect';
import createHistory from 'react-router/lib/createMemoryHistory';
import {Provider} from 'react-redux';
import getAdminRoutes from 'routes/admin-routes.js';
import getPublicRoutes from 'routes/public-routes.js';
import getAuthRoutes from 'routes/auth-routes.js';
import jsonfile from 'jsonfile';
import aws from 'aws-sdk';
import jwt from 'jsonwebtoken';

import cookieParser from 'cookie-parser';

const s3 = new aws.S3({
  params: {
    Bucket: process.env.S3_BUCKET_NAME
  },
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  signatureVersion: 'v4'
});

const targetUrl = 'http://' + config.apiHost + ':' + config.apiPort;
const pretty = new PrettyError();
const app = new Express();
const server = new http.Server(app);

const proxy = httpProxy.createProxyServer({
  target: targetUrl
});

app.use(compression());
app.use(cookieParser())

app.use(favicon(path.join(__dirname, '..', 'static', 'favicon.ico')));

app.use(Express.static(path.join(__dirname, '..', 'static')));

// Proxy to API server
app.use('/api', (req, res) => {
  proxy.web(req, res, {target: targetUrl});
});

// added the error handling to avoid https://github.com/nodejitsu/node-http-proxy/issues/527
proxy.on('error', (error, req, res) => {
  let json;
  if (error.code !== 'ECONNRESET') {
    console.error('proxy error', error);
  }
  if (!res.headersSent) {
    res.writeHead(500, {'content-type': 'application/json'});
  }

  json = {error: 'proxy_error', reason: error.message};
  res.end(JSON.stringify(json));
});

app.use('/refresh-webpack-isomorphic-tools', (req, res) => {
  refreshLanguageData();
  console.log('/===========\\')
  console.log('|| refresh ||');
  console.log('\\===========/');
  res.json({build: 'successfull'});
});

app.use('/admin', (req, res) => {
  const jwtToken = req.cookies ? req.cookies.jwtToken : undefined;
  let token = req.header('Authorization') || jwtToken;
  if (!token)
    return res.redirect('/login');

  token = token.replace('Bearer ', '');

  jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
    if (err) {
      return res.redirect('/login').status(401).json({
        success: false,
        message: 'Token is not valid'
      });
    } else {
      req.user = user;
      requestResponseHandler(req, res)
    }
  });
});

app.use('/login', requestResponseHandler);

app.use(requestResponseHandler);

refreshLanguageData();

if (config.port) {
  server.listen(config.port, (err) => {
    if (err) {
      console.error(err);
    }
    console.info('----\n==> ✅  %s is running, talking to API server on %s.', config.app.title, config.apiPort);
    console.info('==> 💻  Open http://%s:%s in a browser to view the app.', config.host, config.port);
  });
} else {
  console.error('==>     ERROR: No PORT environment variable has been specified');
}

let getRoutes = (baseUrl, store) => {
  if (baseUrl.indexOf('/admin') > -1) {
    return getAdminRoutes(store);
  } else if (baseUrl.indexOf('/login') > -1) {
    return getAuthRoutes(store);
  }
  return getPublicRoutes(store);
}

let getTemplateType = (baseUrl) => {
  if (baseUrl.indexOf('/admin') > -1) {
    return 'admin';
  } else if (baseUrl.indexOf('/login') > -1) {
    return 'auth';
  }
  return 'main';
}

function requestResponseHandler(req, res) {
  if (__DEVELOPMENT__) {
    // Do not cache webpack stats: the script file would change since
    // hot module replacement is enabled in the development env
    webpackIsomorphicTools.refresh();
  }

  const client = new ApiClient(req);
  const history = createHistory(req.originalUrl);

  const store = createStore(history, client);

  const languageData = global.languageData;
  store.dispatch(pageBuilder.setLanguageData(languageData));

  function hydrateOnClient() {
    console.log('hydrate on client');
    res.send('<!doctype html>\n' +
      ReactDOM.renderToString(<Html templateType={getTemplateType(req.baseUrl)} assets={webpackIsomorphicTools.assets()} store={store}/>));
  }

  if (__DISABLE_SSR__) {
    hydrateOnClient();
    return;
  }

  match({ history, routes: getRoutes(req.originalUrl, store), location: req.originalUrl }, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      res.redirect(redirectLocation.pathname + redirectLocation.search);
    } else if (error) {
      console.error('ROUTER ERROR:', pretty.render(error));
      res.status(500);
      hydrateOnClient();
    } else if (renderProps) {
      loadOnServer({...renderProps, store, helpers: {client}}).then(() => {
        const component = (
          <Provider store={store} key="provider">
            <ReduxAsyncConnect {...renderProps} />
          </Provider>
        );

        res.status(200);

        global.navigator = {userAgent: req.headers['user-agent']};
        res.send('<!doctype html>\n' +
          ReactDOM.renderToString(<Html templateType={getTemplateType(req.baseUrl)} assets={webpackIsomorphicTools.assets()} component={component} store={store}/>));
      });
    } else {
      res.status(404).send('Not found');
    }
  });
}

function refreshLanguageData() {
  const S3_BUCKET_FOLDER = process.env.S3_BUCKET_FOLDER ? process.env.S3_BUCKET_FOLDER + '/' : '';
  console.log('S3 > ', process.env.S3_BUCKET_FOLDER);
  s3.getObject({ Key: `${S3_BUCKET_FOLDER}CommonLanguageData.json`}, (err, data) => {
    if (err)
      console.error(err);
    global.languageData = JSON.parse(data.Body.toString('utf-8'));
  });
}
