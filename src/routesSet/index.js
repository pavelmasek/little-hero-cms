import RouteData from './data.json';

export function getLanguageRouteData() {
  return RouteData;
}

export function getLanguageRoute(routeName) {
  return RouteData[routeName];
}
export function getRouteByData(languageRoute, language) {
  const keys = Object.keys(RouteData);
  for (let ii = 0, jj = keys.length; ii < jj; ii++) {
    if (RouteData[keys[ii]][language] === languageRoute) {
      return RouteData[keys[ii]];
    }
  }
}
