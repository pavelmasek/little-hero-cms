#Little Hero CMS
Little Hero CMS is content management system for administration simple website presentation.
Administration & website presetation itself are based on modern technologies such as React.js on Frontend part and Node.js & little bit of MongoDB on Backend part.
Little Hero uses AWS S3 bucket as file server and primary was targeted for deploying on Heroku.

##Example
[Public part](https://little-hero-cms-master-cloud.herokuapp.com)

[Administration part](https://little-hero-cms-master-cloud.herokuapp.com/admin)

**login:** demo

**pass:** demo

###Project is under active development, so it can be broken at any time