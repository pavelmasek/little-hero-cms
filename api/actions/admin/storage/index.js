import aws from 'aws-sdk';

const S3_BUCKET_FOLDER = process.env.S3_BUCKET_FOLDER ? process.env.S3_BUCKET_FOLDER + '/' : '';

const params = {
  Bucket: process.env.S3_BUCKET_NAME
}

const s3 = new aws.S3({
  params: params,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  signatureVersion: 'v4'
});

export function putObject(putParams, awsFileCallback) {
  s3.putObject({
    ...putParams,
    Key: `${S3_BUCKET_FOLDER}${putParams.Key}`
  }, awsFileCallback);
}


export function copyFile(copyFileParams, awsFileCallback) {
  s3.copyObject({
    ...copyFileParams,
    CopySource: `${params.Bucket}/${S3_BUCKET_FOLDER}${copyFileParams.CopySource}`,
    Key: `${S3_BUCKET_FOLDER}${copyFileParams.Key}`
  }, awsFileCallback);
}

export function deleteFile(key, awsFileCallback) {
  s3.deleteObject({Key: `${S3_BUCKET_FOLDER}${key}`}, awsFileCallback);
}

export function getListOfFiles(params, awsFileCallback) {
  s3.listObjects({
    ...params,
    Prefix: `${S3_BUCKET_FOLDER}${params.Prefix}`
  }, awsFileCallback);
}

export function getObject(params, awsFileCallback) {
  s3.getObject({
    ...params,
    Key: `${S3_BUCKET_FOLDER}${params.Key}`
  }, awsFileCallback)
}

export {
  s3,
  params,
  S3_BUCKET_FOLDER
};
