import mongoose from 'mongoose';
import Users from 'models/users.js';
import bcrypt from 'bcrypt';
import { generateToken, jwt } from 'utils/jwt';
import { authenticated } from 'utils/security';

export function getAllUsers(req, res) {
  return authenticated(req, res, new Promise((resolve) => resolve(Users.find())));
}

export function getUsersBy(req, res) {
  return authenticated(req, res, new Promise((resolve) => resolve(Users.find(req.params[0]))));
}

export function addUser(req) {
  return new Promise((resolve, reject) => {
    const { login, password } = req.body.user;
    var user = new Users();
    user.login = login.trim();
    user.password = bcrypt.hashSync(password.trim(), 10);
    user.save(function(err, user){
      if(err)
        reject(err);
      resolve({ user:"bitch" });
    })
  });;
}

export function signIn(req, res) {
  return new Promise((resolve, reject) => {
    const { login, password } = req.body.user;
    Users
      .findOne({login: login})
      .exec((err, user) => {
        if (err) throw err;
        if (!user) {
          return res.status(404).json({
            error: true,
          });
          message: 'Username or Password is Wrong'
        }
        bcrypt.compare(password, user.password, (err, valid) => {
          if (!valid) {
            reject({valid: 'false'})
          }
          const token = generateToken(user);
          resolve({
            user: user,
            token: token
          });
        });
      })
  });
}

export function changePassword(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    const token = req.headers['authorization'].replace('Bearer ', '');
    const decoded = jwt.decode(token);
    const { password } = req.body.user;
    Users.update({login: decoded.login}, {
      password: bcrypt.hashSync(password.trim(), 10)
    }, (err, affected, resp) => {
      if (err)
        reject(err);
      resolve(affected);
    });
  }));
}

export function getUserInfo(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    const token = req.headers['authorization'].replace('Bearer ', '');
    if (!token) {
      reject('No token presented in the header');
    }
    const decoded = jwt.decode(token);
    if (!decoded) {
      reject('No decoded data');
    }
    resolve(decoded);
  }));
}
