import { copyFile, deleteFile, getListOfFiles, putObject, params, S3_BUCKET_FOLDER } from '../storage';
import { authenticated } from 'utils/security';

const service = 's3.amazonaws.com';
/**
 * Gets all media files from file server
 * @param {Request} req 
 * @param {Response} res 
 */
export function getAllMediaFiles(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    getListOfFiles({ Prefix: 'images/' }, (err, data) => {
      if (err)
        reject('Getting all media files', err);
      const images = data.Contents.filter(item => {
        return item.Key.substr(-1) !== '/';
      })
      .map((item) => {
        return {
          ...item,
          Link: `https://${params.Bucket}.${service}/${item.Key}`,
          Key: item.Key.replace(`${S3_BUCKET_FOLDER}`, '')
        }
      });
      resolve(images);
    });
  }));
}
/**
 * Upload new media file on file server
 * @param {Request} req 
 * @param {Response} res 
 */
export function uploadMediaFile(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    const { data, fileName } = req.body.file;
    putObject({ Key: `images/${fileName}`, Body: new Buffer(data, "binary"), ACL: 'public-read'}, (err, data) => {
      if (err)
        reject(err);
      resolve(`Uploading media file ${fileName}`);
    });
  }));
}

/**
 * Delete specified file by the file name
 * @param {Request} req 
 * @param {Response} res 
 */
export function deleteMediaFile(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    const { fileName } = req.body;
    deleteFile(`images/${fileName}`, (err, data) => {
      if (err)
        reject(err, `Deleting file ${fileName}`);
      resolve(`File ${fileName} was deleted`);
    });
  }));
}

export function securityTest(req, res) {
  return authenticated(req, res, new Promise((resolve) => {
    resolve('authenticated call of the method');
  }));
}
