import { XMLHttpRequest } from 'xmlhttprequest';
import ip from 'ip';
import config from '../../../../src/config';

const serverAddress = `http://${ip.address()}:${config.port}`;

export function runBuild() {

  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', `${serverAddress}/refresh-webpack-isomorphic-tools`);
    xhr.onload = () => {
      console.log('response');
      resolve(JSON.parse(xhr.responseText));
    }
    xhr.onerror = (err) => {
      console.log('reject');
      reject(err);
    }
    xhr.send();
  });
}
