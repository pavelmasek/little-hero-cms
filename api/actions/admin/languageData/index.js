import jsonfile from 'jsonfile';
import aws from 'aws-sdk';
import moment from 'moment';
import { authenticated } from 'utils/security';

import { copyFile, deleteFile, getListOfFiles, getObject, putObject, s3, params, S3_BUCKET_FOLDER } from '../storage';

const fileName = 'CommonLanguageData';

export function saveLanguageData(req, res) {
  const version = req.body.version;
  const fileNameWithPostFix = `versions/${version ? version : moment().format('YYYYMMDDHHmmss')}.json`;
  console.log('Save language data', fileNameWithPostFix);
  putObject({ Key: fileNameWithPostFix, Body: JSON.stringify(req.body.languageData, null, 2)}, (err, data) => {
    if (err) {
      return console.log('put object error ', err);
    }
    console.log('Everything is OK!');
  });
  //jsonfile.writeFileSync(`./src/languageData/${fileNameWithPostFix}`, req.body.languageData, {spaces: 2});
  return Promise.resolve({
    status: 'OK'
  });
}

export function setNewLanguageData(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    console.log('fileName', req.body.fileName);
    copyFile({...params, CopySource: `${req.body.fileName}`, Key: 'CommonLanguageData.json'}, (err, data) => {
      if (err)
        reject(err);
      resolve({message: `File ${req.body.fileName} was copied to master...`});
    });
  }));
}

export function getLanguageVersions(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    getListOfFiles({ Prefix: 'versions/'}, (err, data) => {
      if (err)
        reject(err);
      const files = data.Contents.filter(item => {
        return item.Key.substr(-1) !== '/';
      }).map(item => {
        return {
          ...item,
          Key: item.Key.replace(`${S3_BUCKET_FOLDER}`, '')
        }
      });
      resolve(files);
    });
  }));
}

export function getLanguageVersionData(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    getObject({ Key: req.body.fileName}, (err, data) => {
      if (err)
        reject('loadLanguageData ', err);
      resolve(JSON.parse(data.Body.toString('utf-8')))
    });
  }));
}

export function loadLanguageData(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    getObject({ Key: req.data.fileName}, (err, data) => {
      if (err)
        reject('loadLanguageData ', err);
      resolve(JSON.parse(data.Body.toString('utf-8')))
    });
  }));
}

export function createNewLanguageVersion(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    const fileNameWithPostFix = `versions/${fileName}-${moment().format('YYYYMMDDHHmmss')}@${moment().format('YYYYMMDDHHmmss')}.json`;
    copyFile({
      CopySource: `${req.body.fileName}`,
      Key: fileNameWithPostFix,
      }, (err, data) => {
      if (err)
        reject(err);
      resolve({message: `File ${req.body.fileName} copied...`});
    });
  }));
}

export function deleteLanguageVersion(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    deleteFile(req.body.fileName, (err, data) => {
      if (err)
        reject(err);
      resolve({message: `File ${req.body.fileName} was removed...`});
    });
  }));
}

export function renameLanguageVersion(req, res) {
  return authenticated(req, res, new Promise((resolve, reject) => {
    const { oldFileName, newFileName } = req.body;
    copyFile({ CopySource: `${oldFileName}`, Key: newFileName.replace(/\s/g,'') }, (err, data) => {
      if (err) {
        reject(`copy file ${err}`);
        return;
      }
      deleteFile(oldFileName, (err, data) => {
        if (err) {
          reject(`delete file ${err}`);
          return;
        }
        resolve({ message: `File ${oldFileName} was renamed to ${newFileName}`});
      });
    });
  }));
}
