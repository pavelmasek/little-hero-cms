import sendgrid from 'sendgrid';
var sg = sendgrid.SendGrid(process.env.SENDGRID_API_KEY);
var formSetting = require('./formSetting.json');

export function sendContactForm(req) {
  return new Promise((resolve, reject) => {
    console.log(req.body);
    let mailSetting = req.body;
    mailSetting.content = getContactContent(mailSetting);
    sendMail(mailSetting);
    resolve({form: 'sended'});
  })
}

function getContactContent(mailSetting) {
  return [
    "Jméno a příjmení: " + mailSetting.nameAndSurname,
    "E-mail: " + mailSetting.mail,
    "Telefon: " + mailSetting.phoneNumber,
    "",
    "Obsah zprávy: ",
    mailSetting.messageContent,
  ].join('<br />');
}

function sendMail(mailSetting) {
  console.log(mailSetting);
  var request = sg.emptyRequest()
  request.body = {
    "personalizations": [
      {
        "to": formSetting.to,
        "subject": formSetting.subject
      }
    ],
    "from": {
      "email": mailSetting.mail,
      "name": mailSetting.nameAndSurname
    },
    "content": [
      {
        "type": "text/html",
        "value": [
          "<html>",
          "<p>",
          mailSetting.content,
          "</p>",
          "</html>"
        ].join('<br />')
      }
    ]
  };
  request.method = 'POST'
  request.path = '/v3/mail/send'
  sg.API(request, function (response) {
    console.log(response.statusCode)
    console.log(response.body)
    console.log(response.headers)
  });
}
