import jwt from 'jsonwebtoken';

export function generateToken(user) {
  //1. Dont use password and other sensitive fields
  //2. Use fields that are useful in other parts of the
  //app/collections/models
  var u = {
   password: user.password,
   login: user.login,
   _id: user._id.toString()
  };
  return jwt.sign(u, process.env.JWT_SECRET, {
     expiresIn: 60 * 60 * 24 * 7 // expires in 7 days
  });
}

export {
  jwt
}
