import { jwt } from './jwt';

export function authenticated(req, res, promise) {
  return new Promise((resolve, reject) => {
    const jwtToken = req.cookies ? req.cookies.jwtToken : undefined;
    let token = req.header('Authorization') || jwtToken;
    if (!token)
      reject({
        message: 'Unauthorized access'
      });
    token = token.replace('Bearer ', '');
    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
      if (err) {
        reject({
          message: 'Unauthorized access'
        });
      }
      resolve(promise);
    });
  });
}
